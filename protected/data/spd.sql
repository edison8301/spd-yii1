-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 07 Nov 2014 pada 07.46
-- Versi Server: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `spd`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `access`
--

CREATE TABLE IF NOT EXISTS `access` (
`id` int(11) NOT NULL,
  `nama_controller` varchar(255) NOT NULL,
  `nama_action` varchar(255) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data untuk tabel `access`
--

INSERT INTO `access` (`id`, `nama_controller`, `nama_action`) VALUES
(1, 'akomodasi', 'create'),
(2, 'akomodasi', 'update'),
(3, 'akomodasi', 'delete'),
(4, 'pegawai', 'create'),
(5, 'pegawai', 'update'),
(6, 'pegawai', 'delete'),
(7, 'user', 'create'),
(8, 'user', 'update'),
(9, 'kwitansi', 'input'),
(10, 'kwitansi', 'update'),
(11, 'lhp', 'input'),
(12, 'lhp', 'update'),
(13, 'lhp', 'delete'),
(14, 'visum', 'create'),
(15, 'visum', 'update'),
(16, 'visum', 'delete'),
(17, 'kwitansi', 'delete'),
(18, 'ssh', 'create'),
(19, 'ssh', 'update'),
(20, 'ssh', 'delete'),
(21, 'user', 'delete'),
(22, 'role', 'create'),
(23, 'role', 'update'),
(24, 'role', 'delete'),
(25, 'pejabat', 'create'),
(26, 'pejabat', 'update'),
(27, 'pejabat', 'delete'),
(28, 'role', 'access'),
(29, 'spd', 'inputSpt'),
(30, 'spd', 'updateSpt'),
(31, 'spd', 'cetakSpd'),
(32, 'rill', 'input'),
(33, 'rill', 'update'),
(34, 'rill', 'delete'),
(45, 'spd', 'inputSpd'),
(46, 'spd', 'updateSpd'),
(47, 'spd', 'cetakSpt'),
(48, 'lhp', 'cetak'),
(49, 'kwitansi', 'cetak');

-- --------------------------------------------------------

--
-- Struktur dari tabel `akomodasi`
--

CREATE TABLE IF NOT EXISTS `akomodasi` (
`id` int(11) NOT NULL,
  `jarak` varchar(255) DEFAULT NULL,
  `id_golongan` int(11) DEFAULT NULL,
  `biaya` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=13 ;

--
-- Dumping data untuk tabel `akomodasi`
--

INSERT INTO `akomodasi` (`id`, `jarak`, `id_golongan`, `biaya`) VALUES
(1, 'DKI Jakarta', 1, '350000'),
(2, 'DKI Jakarta', 2, '400000'),
(3, 'DKI Jakarta', 3, '500000'),
(4, 'DKI Jakarta', 4, '650000'),
(5, 'Pulau Jawa', 1, '300000'),
(6, 'Pulau Jawa', 2, '400000'),
(7, 'Pulau Jawa', 3, '500000'),
(8, 'Pulau Jawa', 4, '830000'),
(9, 'Luar Pulau Jawa', 1, '500000'),
(10, 'Luar Pulau Jawa', 2, '500000'),
(11, 'Luar Pulau Jawa', 4, '800000'),
(12, 'Luar Pulau Jawa', 4, '1100000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `golongan`
--

CREATE TABLE IF NOT EXISTS `golongan` (
`id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=17 ;

--
-- Dumping data untuk tabel `golongan`
--

INSERT INTO `golongan` (`id`, `nama`) VALUES
(1, 'I a'),
(2, 'I b'),
(3, 'I c'),
(4, 'I d'),
(5, 'II a'),
(6, 'II b'),
(7, 'II c'),
(8, 'II d'),
(9, 'III a'),
(10, 'III b'),
(11, 'III c'),
(12, 'III d'),
(13, 'IV a'),
(14, 'IV b'),
(15, 'IV c'),
(16, 'IV d');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kwitansi`
--

CREATE TABLE IF NOT EXISTS `kwitansi` (
`id` int(11) NOT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `penerima` int(11) DEFAULT NULL,
  `id_spd` int(11) DEFAULT NULL,
  `lama` int(11) DEFAULT NULL,
  `akomodasi` varchar(255) DEFAULT NULL,
  `refresentatif` varchar(255) DEFAULT NULL,
  `uang_harian` varchar(255) DEFAULT NULL,
  `bendahara` int(11) DEFAULT NULL,
  `pptk` int(11) DEFAULT NULL,
  `pa` int(11) DEFAULT NULL,
  `bbm` varchar(255) DEFAULT NULL,
  `tol` varchar(255) DEFAULT NULL,
  `tiket` varchar(255) DEFAULT NULL,
  `total` varchar(255) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=17 ;

--
-- Dumping data untuk tabel `kwitansi`
--

INSERT INTO `kwitansi` (`id`, `nomor`, `tanggal`, `penerima`, `id_spd`, `lama`, `akomodasi`, `refresentatif`, `uang_harian`, `bendahara`, `pptk`, `pa`, `bbm`, `tol`, `tiket`, `total`) VALUES
(13, 'test', NULL, 10, 46, 2, '350000', '24000', '24000', 3, 3, 6, '12000', '12000', '12000', '458000'),
(14, '2324234/23432', NULL, 1, 44, NULL, '350000', '100000', '600000', 4, 4, 4, '0', '0', '0', '1650000'),
(15, 'KW/01', NULL, 1, 46, 2, '350000', '100000', '150000', 3, 4, 5, '120000', '300000', '150000', '1320000'),
(16, 'KW/02', NULL, 1, 46, NULL, '350000', '100000', '150000', 3, 4, 3, '0', '0', '0', '600000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lhp`
--

CREATE TABLE IF NOT EXISTS `lhp` (
`id` int(11) NOT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `kepada` varchar(255) DEFAULT NULL,
  `dari` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `hal` varchar(255) DEFAULT NULL,
  `id_spd` int(11) DEFAULT NULL,
  `kesimpulan` text
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `lhp`
--

INSERT INTO `lhp` (`id`, `nomor`, `kepada`, `dari`, `tanggal`, `hal`, `id_spd`, `kesimpulan`) VALUES
(1, '12345', 'Kasubag Umum dan Kepegawian DPKD Kota Serang', 'Kasi Pelaporan DPKD Kota Serang', '2014-04-21', 'qwerty', 46, 'qwe');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
`id` int(11) NOT NULL,
  `nip` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `id_golongan` int(11) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `tgl_lahir` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=11 ;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id`, `nip`, `password`, `nama`, `id_golongan`, `jabatan`, `tgl_lahir`, `foto`) VALUES
(1, '123456789123456782', '123456789123456789', 'Reza Ilham', 2, 'Sekertaris', '1995-01-06', ''),
(4, '134324', NULL, 'Alfa', 1, 'Programmer', '2014-04-19', NULL),
(8, '123456789012345678', '123456789012345678', 'Alfa Edison', 1, 'Staf', '1967-04-06', NULL),
(9, '123456789123456787', '123', 'Irvan Resna', 1, 'Staf', '1965-05-05', NULL),
(10, '123456789123456785', '123', 'Aeng', 1, 'Staf', '2014-05-23', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pejabat`
--

CREATE TABLE IF NOT EXISTS `pejabat` (
`id` int(11) NOT NULL,
  `nip` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `pejabat`
--

INSERT INTO `pejabat` (`id`, `nip`, `nama`, `jabatan`, `password`, `foto`) VALUES
(3, '197712311997021002', 'W. Hari Pamungkas, S.STP, M.Si', 'Sekretaris DPKD Kota Serang', NULL, ''),
(4, '198705302011012001', 'Devi Fitriani, SE', 'Pelaksana', NULL, NULL),
(5, '198006302009022008', 'Tini Suhartini, S.Sos', 'Pelaksana', NULL, NULL),
(6, '197705061997112001', 'Hj. Raudah, S.STP, M.Si', 'Kasubag Program Evaluasi & Pelaporan', NULL, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengikut`
--

CREATE TABLE IF NOT EXISTS `pengikut` (
`id` int(11) NOT NULL,
  `id_spd` int(11) DEFAULT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `aktif` int(11) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=103 ;

--
-- Dumping data untuk tabel `pengikut`
--

INSERT INTO `pengikut` (`id`, `id_spd`, `id_pegawai`, `aktif`) VALUES
(61, 46, 4, 1),
(63, 46, NULL, 0),
(64, 46, NULL, 0),
(65, 46, NULL, 0),
(66, 46, NULL, 0),
(67, 46, NULL, 0),
(68, 46, NULL, 0),
(69, 46, NULL, 0),
(70, 46, NULL, 0),
(71, 46, NULL, 0),
(72, 46, NULL, 0),
(73, 46, NULL, 0),
(74, 46, NULL, 0),
(75, 46, NULL, 0),
(76, 46, NULL, 0),
(77, 46, NULL, 0),
(78, 46, NULL, 0),
(79, 46, NULL, 0),
(80, 46, NULL, 0),
(82, 46, NULL, 0),
(83, 47, NULL, 0),
(84, 47, NULL, 0),
(85, 47, NULL, 0),
(86, 47, NULL, 0),
(87, 47, NULL, 0),
(88, 47, NULL, 0),
(89, 47, 4, 1),
(91, 47, 8, 0),
(92, 48, NULL, 0),
(93, 48, NULL, 0),
(94, 48, NULL, 0),
(95, 48, NULL, 0),
(96, 48, NULL, 0),
(97, 48, NULL, 0),
(98, 48, 4, 0),
(100, 47, 9, 0),
(101, 48, 8, 0),
(102, 52, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `perjalanan`
--

CREATE TABLE IF NOT EXISTS `perjalanan` (
`id` int(11) NOT NULL,
  `id_spd` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `aktif` int(11) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=312 ;

--
-- Dumping data untuk tabel `perjalanan`
--

INSERT INTO `perjalanan` (`id`, `id_spd`, `id_pegawai`, `tanggal`, `aktif`) VALUES
(61, 46, 4, '2014-04-08', 0),
(62, 46, 5, '2014-04-08', 0),
(63, 46, 4, '2014-04-09', 0),
(64, 46, 5, '2014-04-09', 0),
(65, 46, 4, '2014-04-10', 0),
(66, 46, 5, '2014-04-10', 0),
(67, 46, 4, '2014-04-11', 0),
(68, 46, 5, '2014-04-11', 0),
(69, 46, 4, '2014-04-12', 0),
(70, 46, 5, '2014-04-12', 0),
(71, 46, 4, '2014-04-08', 0),
(72, 46, 5, '2014-04-08', 0),
(73, 46, 4, '2014-04-09', 0),
(74, 46, 5, '2014-04-09', 0),
(75, 46, 4, '2014-04-10', 0),
(76, 46, 5, '2014-04-10', 0),
(77, 46, 4, '2014-04-11', 0),
(78, 46, 5, '2014-04-11', 0),
(79, 46, 4, '2014-04-12', 0),
(80, 46, 5, '2014-04-12', 0),
(81, 46, 4, '2014-04-08', 0),
(82, 46, 5, '2014-04-08', 0),
(83, 46, 4, '2014-04-09', 0),
(84, 46, 5, '2014-04-09', 0),
(85, 46, 4, '2014-04-10', 0),
(86, 46, 5, '2014-04-10', 0),
(87, 46, 4, '2014-04-11', 0),
(88, 46, 5, '2014-04-11', 0),
(89, 46, 4, '2014-04-12', 0),
(90, 46, 5, '2014-04-12', 0),
(91, 46, 4, '2014-04-08', 0),
(92, 46, 5, '2014-04-08', 0),
(93, 46, 4, '2014-04-09', 0),
(94, 46, 5, '2014-04-09', 0),
(95, 46, 4, '2014-04-10', 0),
(96, 46, 5, '2014-04-10', 0),
(97, 46, 4, '2014-04-11', 0),
(98, 46, 5, '2014-04-11', 0),
(99, 46, 4, '2014-04-12', 0),
(100, 46, 5, '2014-04-12', 0),
(101, 46, 4, '2014-04-08', 0),
(102, 46, 5, '2014-04-08', 0),
(103, 46, 4, '2014-04-09', 0),
(104, 46, 5, '2014-04-09', 0),
(105, 46, 4, '2014-04-10', 0),
(106, 46, 5, '2014-04-10', 0),
(107, 46, 4, '2014-04-11', 0),
(108, 46, 5, '2014-04-11', 0),
(109, 46, 4, '2014-04-12', 0),
(110, 46, 5, '2014-04-12', 0),
(111, 46, 4, '2014-04-08', 0),
(112, 46, 5, '2014-04-08', 0),
(113, 46, 4, '2014-04-09', 0),
(114, 46, 5, '2014-04-09', 0),
(115, 46, 4, '2014-04-10', 0),
(116, 46, 5, '2014-04-10', 0),
(117, 46, 4, '2014-04-11', 0),
(118, 46, 5, '2014-04-11', 0),
(119, 46, 4, '2014-04-12', 0),
(120, 46, 5, '2014-04-12', 0),
(121, 46, 4, '2014-04-08', 0),
(122, 46, 5, '2014-04-08', 0),
(123, 46, 4, '2014-04-09', 0),
(124, 46, 5, '2014-04-09', 0),
(125, 46, 4, '2014-04-10', 0),
(126, 46, 5, '2014-04-10', 0),
(127, 46, 4, '2014-04-11', 0),
(128, 46, 5, '2014-04-11', 0),
(129, 46, 4, '2014-04-12', 0),
(130, 46, 5, '2014-04-12', 0),
(131, 46, 4, '2014-04-08', 0),
(132, 46, 5, '2014-04-08', 0),
(133, 46, 7, '2014-04-08', 0),
(134, 46, 4, '2014-04-09', 0),
(135, 46, 5, '2014-04-09', 0),
(136, 46, 7, '2014-04-09', 0),
(137, 46, 4, '2014-04-10', 0),
(138, 46, 5, '2014-04-10', 0),
(139, 46, 7, '2014-04-10', 0),
(140, 46, 4, '2014-04-11', 0),
(141, 46, 5, '2014-04-11', 0),
(142, 46, 7, '2014-04-11', 0),
(143, 46, 4, '2014-04-12', 0),
(144, 46, 5, '2014-04-12', 0),
(145, 46, 7, '2014-04-12', 0),
(146, 46, 4, '2014-04-08', 0),
(147, 46, 5, '2014-04-08', 0),
(148, 46, 7, '2014-04-08', 0),
(149, 46, 4, '2014-04-09', 0),
(150, 46, 5, '2014-04-09', 0),
(151, 46, 7, '2014-04-09', 0),
(152, 46, 4, '2014-04-10', 0),
(153, 46, 5, '2014-04-10', 0),
(154, 46, 7, '2014-04-10', 0),
(155, 46, 4, '2014-04-11', 0),
(156, 46, 5, '2014-04-11', 0),
(157, 46, 7, '2014-04-11', 0),
(158, 46, 4, '2014-04-12', 0),
(159, 46, 5, '2014-04-12', 0),
(160, 46, 7, '2014-04-12', 0),
(161, 46, 1, '2014-04-08', 1),
(162, 46, 4, '2014-04-08', 1),
(163, 46, 5, '2014-04-08', 1),
(164, 46, 7, '2014-04-08', 1),
(165, 46, 1, '2014-04-09', 1),
(166, 46, 4, '2014-04-09', 1),
(167, 46, 5, '2014-04-09', 1),
(168, 46, 7, '2014-04-09', 1),
(169, 46, 1, '2014-04-10', 1),
(170, 46, 4, '2014-04-10', 1),
(171, 46, 5, '2014-04-10', 1),
(172, 46, 7, '2014-04-10', 1),
(173, 46, 1, '2014-04-11', 1),
(174, 46, 4, '2014-04-11', 1),
(175, 46, 5, '2014-04-11', 1),
(176, 46, 7, '2014-04-11', 1),
(177, 46, 1, '2014-04-12', 1),
(178, 46, 4, '2014-04-12', 1),
(179, 46, 5, '2014-04-12', 1),
(180, 46, 7, '2014-04-12', 1),
(181, 47, 1, '2014-06-02', 1),
(182, 47, 1, '2014-06-03', 1),
(183, 47, 1, '2014-06-04', 1),
(184, 47, 1, '2014-06-05', 1),
(185, 47, 1, '2014-06-06', 0),
(186, 47, 4, '2014-06-02', 0),
(187, 47, 5, '2014-06-02', 0),
(188, 47, 8, '2014-06-02', 0),
(189, 47, 4, '2014-06-03', 0),
(190, 47, 5, '2014-06-03', 0),
(191, 47, 8, '2014-06-03', 0),
(192, 47, 4, '2014-06-04', 0),
(193, 47, 5, '2014-06-04', 0),
(194, 47, 8, '2014-06-04', 0),
(195, 47, 4, '2014-06-05', 0),
(196, 47, 5, '2014-06-05', 0),
(197, 47, 8, '2014-06-05', 0),
(198, 47, 4, '2014-06-06', 0),
(199, 47, 5, '2014-06-06', 0),
(200, 47, 8, '2014-06-06', 0),
(201, 48, 1, '2014-06-09', 0),
(202, 48, 1, '2014-06-10', 0),
(203, 48, 1, '2014-06-11', 0),
(204, 48, 4, '2014-06-09', 0),
(205, 48, 5, '2014-06-09', 0),
(206, 48, 4, '2014-06-10', 0),
(207, 48, 5, '2014-06-10', 0),
(208, 47, 4, '2014-06-02', 0),
(209, 47, 5, '2014-06-02', 0),
(210, 47, 4, '2014-06-03', 0),
(211, 47, 5, '2014-06-03', 0),
(212, 47, 4, '2014-06-04', 0),
(213, 47, 5, '2014-06-04', 0),
(214, 47, 4, '2014-06-05', 0),
(215, 47, 5, '2014-06-05', 0),
(216, 47, 4, '2014-06-06', 0),
(217, 47, 5, '2014-06-06', 0),
(218, 47, 4, '2014-06-02', 0),
(219, 47, 5, '2014-06-02', 0),
(220, 47, 4, '2014-06-03', 0),
(221, 47, 5, '2014-06-03', 0),
(222, 47, 4, '2014-06-04', 0),
(223, 47, 5, '2014-06-04', 0),
(224, 47, 4, '2014-06-05', 0),
(225, 47, 5, '2014-06-05', 0),
(226, 47, 4, '2014-06-06', 0),
(227, 47, 5, '2014-06-06', 0),
(228, 47, 4, '2014-06-02', 0),
(229, 47, 5, '2014-06-02', 0),
(230, 47, 4, '2014-06-03', 0),
(231, 47, 5, '2014-06-03', 0),
(232, 47, 4, '2014-06-04', 0),
(233, 47, 5, '2014-06-04', 0),
(234, 47, 4, '2014-06-05', 0),
(235, 47, 5, '2014-06-05', 0),
(236, 47, 4, '2014-06-06', 0),
(237, 47, 5, '2014-06-06', 0),
(238, 47, 4, '2014-06-02', 0),
(239, 47, 5, '2014-06-02', 0),
(240, 47, 9, '2014-06-02', 0),
(241, 47, 4, '2014-06-03', 0),
(242, 47, 5, '2014-06-03', 0),
(243, 47, 9, '2014-06-03', 0),
(244, 47, 4, '2014-06-04', 0),
(245, 47, 5, '2014-06-04', 0),
(246, 47, 9, '2014-06-04', 0),
(247, 47, 4, '2014-06-05', 0),
(248, 47, 5, '2014-06-05', 0),
(249, 47, 9, '2014-06-05', 0),
(250, 47, 4, '2014-06-06', 0),
(251, 47, 5, '2014-06-06', 0),
(252, 47, 9, '2014-06-06', 0),
(253, 47, 4, '2014-06-02', 0),
(254, 47, 5, '2014-06-02', 0),
(255, 47, 9, '2014-06-02', 0),
(256, 47, 4, '2014-06-03', 0),
(257, 47, 5, '2014-06-03', 0),
(258, 47, 9, '2014-06-03', 0),
(259, 47, 4, '2014-06-04', 0),
(260, 47, 5, '2014-06-04', 0),
(261, 47, 9, '2014-06-04', 0),
(262, 47, 4, '2014-06-05', 0),
(263, 47, 5, '2014-06-05', 0),
(264, 47, 9, '2014-06-05', 0),
(265, 47, 4, '2014-06-02', 0),
(266, 47, 5, '2014-06-02', 0),
(267, 47, 4, '2014-06-03', 0),
(268, 47, 5, '2014-06-03', 0),
(269, 47, 4, '2014-06-04', 0),
(270, 47, 5, '2014-06-04', 0),
(271, 47, 4, '2014-06-05', 0),
(272, 47, 5, '2014-06-05', 0),
(273, 48, 9, '2014-06-02', 1),
(274, 48, 9, '2014-06-03', 1),
(275, 48, 8, '2014-06-02', 0),
(276, 48, 8, '2014-06-03', 0),
(277, 48, 8, '2014-06-02', 0),
(278, 48, 8, '2014-06-03', 0),
(279, 47, 4, '2014-06-02', 0),
(280, 47, 4, '2014-06-03', 0),
(281, 47, 4, '2014-06-04', 0),
(282, 47, 4, '2014-06-05', 0),
(283, 48, 5, '2014-06-02', 0),
(284, 48, 8, '2014-06-02', 0),
(285, 48, 5, '2014-06-03', 0),
(286, 48, 8, '2014-06-03', 0),
(287, 48, 5, '2014-06-02', 0),
(288, 48, 8, '2014-06-02', 0),
(289, 48, 5, '2014-06-03', 0),
(290, 48, 8, '2014-06-03', 0),
(291, 47, 4, '2014-06-02', 1),
(292, 47, 4, '2014-06-03', 1),
(293, 47, 4, '2014-06-04', 1),
(294, 47, 4, '2014-06-05', 1),
(295, 49, 9, '2014-06-09', 1),
(296, 49, 9, '2014-06-10', 1),
(305, 51, 10, '2014-06-02', 1),
(306, 51, 10, '2014-06-03', 1),
(307, 52, 1, '2014-07-22', 1),
(308, 52, 1, '2014-07-23', 1),
(309, 52, 1, '2014-07-24', 1),
(310, 52, 1, '2014-07-25', 1),
(311, 52, 1, '2014-07-26', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `refresentatif`
--

CREATE TABLE IF NOT EXISTS `refresentatif` (
`id` int(11) NOT NULL,
  `jarak` varchar(255) DEFAULT NULL,
  `biaya` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `refresentatif`
--

INSERT INTO `refresentatif` (`id`, `jarak`, `biaya`) VALUES
(1, 'Jarak Tempuh 5 s/d 15 KM', '100000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `riil`
--

CREATE TABLE IF NOT EXISTS `riil` (
`id` int(11) NOT NULL,
  `id_spd` int(11) DEFAULT NULL,
  `bbm` varchar(255) DEFAULT NULL,
  `tol` varchar(255) DEFAULT NULL,
  `jumlah` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `ppk` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=COMPACT AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `riil`
--

INSERT INTO `riil` (`id`, `id_spd`, `bbm`, `tol`, `jumlah`, `tanggal`, `ppk`) VALUES
(1, 46, '300000', '100000', '400.000', '2014-04-17', 3),
(2, 46, '300000', '100000', '400.000', '2014-04-17', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `role`
--

CREATE TABLE IF NOT EXISTS `role` (
`id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `role`
--

INSERT INTO `role` (`id`, `nama`) VALUES
(1, 'admin'),
(2, 'operator'),
(3, 'pegawai'),
(4, 'tes ok');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_access`
--

CREATE TABLE IF NOT EXISTS `role_access` (
`id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `access_id` int(11) NOT NULL,
  `status` int(11) DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data untuk tabel `role_access`
--

INSERT INTO `role_access` (`id`, `role_id`, `access_id`, `status`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 1),
(3, 1, 3, 1),
(4, 1, 9, 1),
(5, 1, 17, 1),
(6, 1, 10, 1),
(7, 1, 11, 1),
(8, 1, 13, 1),
(9, 1, 12, 1),
(10, 1, 4, 1),
(11, 1, 6, 1),
(12, 1, 5, 1),
(13, 1, 18, 1),
(14, 1, 20, 1),
(15, 1, 19, 1),
(16, 1, 7, 1),
(17, 1, 21, 1),
(18, 1, 8, 1),
(19, 1, 14, 1),
(20, 1, 16, 1),
(21, 1, 15, 1),
(22, 1, 22, 1),
(23, 1, 24, 1),
(24, 1, 23, 1),
(25, 1, 25, 1),
(26, 1, 27, 1),
(27, 1, 26, 1),
(28, 1, 28, 1),
(29, 1, 29, 1),
(30, 1, 31, 1),
(31, 1, 30, 1),
(32, 1, 32, 1),
(33, 1, 34, 1),
(34, 1, 33, 1),
(35, 1, 41, 0),
(36, 1, 35, 0),
(37, 1, 36, 0),
(38, 1, 42, 0),
(39, 1, 40, 0),
(40, 1, 39, 0),
(41, 1, 38, 0),
(42, 1, 37, 0),
(43, 1, 43, 0),
(44, 1, 44, 0),
(45, 1, 45, 1),
(46, 1, 46, 1),
(47, 1, 47, 1),
(48, 1, 48, 1),
(49, 1, 49, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `spd`
--

CREATE TABLE IF NOT EXISTS `spd` (
`id` int(11) NOT NULL,
  `nomor_spt` varchar(255) DEFAULT NULL,
  `nomor_spd` varchar(255) DEFAULT NULL,
  `tanggal_spt` date DEFAULT NULL,
  `tanggal_spd` date DEFAULT NULL,
  `id_pejabat` int(11) DEFAULT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `maksud` varchar(255) DEFAULT NULL,
  `tujuan` varchar(255) DEFAULT NULL,
  `tgl_pergi` date DEFAULT NULL,
  `tgl_kembali` date DEFAULT NULL,
  `lama` varchar(255) DEFAULT NULL,
  `kendaraan` varchar(255) DEFAULT NULL,
  `tempat_berangkat` varchar(255) DEFAULT NULL,
  `instansi` varchar(255) DEFAULT NULL,
  `biaya` varchar(255) DEFAULT NULL,
  `koring` varchar(255) DEFAULT NULL,
  `pengikut` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=53 ;

--
-- Dumping data untuk tabel `spd`
--

INSERT INTO `spd` (`id`, `nomor_spt`, `nomor_spd`, `tanggal_spt`, `tanggal_spd`, `id_pejabat`, `id_pegawai`, `maksud`, `tujuan`, `tgl_pergi`, `tgl_kembali`, `lama`, `kendaraan`, `tempat_berangkat`, `instansi`, `biaya`, `koring`, `pengikut`) VALUES
(44, 'Cek 11', 'SPD/2014/11/22', '2014-04-03', '2014-04-03', 3, 1, 'Cek 11', 'Cek 11', '2014-04-03', '2014-04-08', '2', 'Mobil', 'Serang', 'DPKD', '2000000', '5.2.15.01', NULL),
(46, '221/2222/33333', 'SPD/0234', '2014-04-05', '2014-04-05', 3, 1, 'Merapatkan rencana kerja', 'Bandung', '2014-04-08', '2014-04-12', '2', 'Mobil', 'Serang', 'BPKD', '2000000', '5.2.15.01', NULL),
(47, 'D/034/23', NULL, '2014-06-02', NULL, 3, 1, 'Mengikut rapat koordinasi', 'Bandung', '2014-06-02', '2014-06-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 'D/034/25', NULL, '2014-05-30', NULL, 3, 9, 'Mengikuti rapat koordinasi', 'Bandung', '2014-06-02', '2014-06-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 'D/034/26', NULL, '2014-05-30', NULL, 5, 9, 'Rapat koordinasi', 'Bandung', '2014-06-09', '2014-06-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 'D/034/27', NULL, '2014-05-30', NULL, 3, 10, '', 'Bandung', '2014-06-02', '2014-06-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 'sadfasfasd', NULL, '2014-07-21', NULL, 3, 1, '', 'KARAWANG', '2014-07-22', '2014-07-26', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `uang_harian`
--

CREATE TABLE IF NOT EXISTS `uang_harian` (
`id` int(11) NOT NULL,
  `jarak` varchar(255) DEFAULT NULL,
  `id_golongan` int(11) DEFAULT NULL,
  `biaya` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=COMPACT AUTO_INCREMENT=28 ;

--
-- Dumping data untuk tabel `uang_harian`
--

INSERT INTO `uang_harian` (`id`, `jarak`, `id_golongan`, `biaya`) VALUES
(1, 'Jarak Tempuh 15 s/d 30 KM', 1, '150000'),
(2, 'Jarak Tempuh 15 s/d 30 KM', 2, '200000'),
(3, 'Jarak Tempuh 15 s/d 30 KM', 3, '250000'),
(4, 'Jarak Tempuh 15 s/d 30 KM', 4, '275000'),
(5, 'Jarak Tempuh Lebih dari 30 KM', 1, '150000'),
(6, 'Jarak Tempuh Lebih dari 30 KM', 2, '225000'),
(7, 'Jarak Tempuh Lebih dari 30 KM', 3, '275000'),
(8, 'Jarak Tempuh Lebih dari 30 KM', 4, '325000'),
(9, 'Jarak Tempuh DKI', 1, '350000'),
(10, 'Jarak Tempuh DKI', 2, '375000'),
(11, 'Jarak Tempuh DKI', 3, '425000'),
(12, 'Jarak Tempuh DKI', 4, '475000'),
(13, 'Jarak Tempuh Kurang dari 200 KM', 1, '400000'),
(14, 'Jarak Tempuh Kurang dari 200 KM', 2, '425000'),
(15, 'Jarak Tempuh Kurang dari 200 KM', 3, '475000'),
(16, 'Jarak Tempuh Kurang dari 200 KM', 4, '525000'),
(17, 'Jarak Tempuh Lebih dari 200 KM', 1, '500000'),
(18, 'Jarak Tempuh Lebih dari 200 KM', 2, '525000'),
(19, 'Jarak Tempuh Lebih dari 200 KM', 3, '575000'),
(20, 'Jarak Tempuh Lebih dari 200 KM', 4, '600000'),
(21, 'Jarak Dalam Daerah', 1, '60000'),
(22, 'Jarak Dalam Daerah', 2, '75000'),
(23, 'Jarak Dalam Daerah', 3, '90000'),
(24, 'Jarak Dalam Daerah', 4, '110000'),
(25, 'Jarak Tempuh 5 s/d 15 KM', 1, '100000'),
(26, 'Jarak Tempuh 5 s/d 15 KM', 2, '125000'),
(27, 'Jarak Tempuh 5 s/d 15 KM', 3, '150000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `role_id`, `last_login`, `status`) VALUES
(1, 'admin', '123', 1, NULL, 1),
(2, 'eja', 'eja', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `visum`
--

CREATE TABLE IF NOT EXISTS `visum` (
`id` int(11) NOT NULL,
  `nomor` varchar(255) NOT NULL,
  `nip` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `instansi` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `visum`
--

INSERT INTO `visum` (`id`, `nomor`, `nip`, `nama`, `jabatan`, `instansi`) VALUES
(1, '1', '19881203 201202 1 001', 'Drs. Solehudin Al Ghifari, S.Sos, M.Si', 'Kasubag Evaluasi Keuangan Daerah', 'Kementrian Keuangan Jakarta');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access`
--
ALTER TABLE `access`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `akomodasi`
--
ALTER TABLE `akomodasi`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_akomodasi_gol` (`id_golongan`);

--
-- Indexes for table `golongan`
--
ALTER TABLE `golongan`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kwitansi`
--
ALTER TABLE `kwitansi`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_kwitansi_spd` (`id_spd`), ADD KEY `FK_kwitansi_ako` (`akomodasi`), ADD KEY `FK_kwitansi_ref` (`refresentatif`), ADD KEY `FK_kwitansi_bendahara` (`bendahara`), ADD KEY `FK_kwitansi_pelaksana` (`pptk`), ADD KEY `FK_kwitansi_ssh` (`uang_harian`);

--
-- Indexes for table `lhp`
--
ALTER TABLE `lhp`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_lhp_spd` (`id_spd`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_pegawai_golongan` (`id_golongan`);

--
-- Indexes for table `pejabat`
--
ALTER TABLE `pejabat`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengikut`
--
ALTER TABLE `pengikut`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_pengikut_spd` (`id_spd`), ADD KEY `FK_pengikut_pegawai` (`id_pegawai`);

--
-- Indexes for table `perjalanan`
--
ALTER TABLE `perjalanan`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refresentatif`
--
ALTER TABLE `refresentatif`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `riil`
--
ALTER TABLE `riil`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_rill_spd` (`id_spd`), ADD KEY `FK_rill_pejabat` (`ppk`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_access`
--
ALTER TABLE `role_access`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spd`
--
ALTER TABLE `spd`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_spd_pejabat` (`id_pejabat`), ADD KEY `FK_spd_pegawai` (`id_pegawai`);

--
-- Indexes for table `uang_harian`
--
ALTER TABLE `uang_harian`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_ssh_gol` (`id_golongan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_user` (`role_id`);

--
-- Indexes for table `visum`
--
ALTER TABLE `visum`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access`
--
ALTER TABLE `access`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `akomodasi`
--
ALTER TABLE `akomodasi`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `golongan`
--
ALTER TABLE `golongan`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `kwitansi`
--
ALTER TABLE `kwitansi`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `lhp`
--
ALTER TABLE `lhp`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pejabat`
--
ALTER TABLE `pejabat`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pengikut`
--
ALTER TABLE `pengikut`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `perjalanan`
--
ALTER TABLE `perjalanan`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=312;
--
-- AUTO_INCREMENT for table `refresentatif`
--
ALTER TABLE `refresentatif`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `riil`
--
ALTER TABLE `riil`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `role_access`
--
ALTER TABLE `role_access`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `spd`
--
ALTER TABLE `spd`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `uang_harian`
--
ALTER TABLE `uang_harian`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `visum`
--
ALTER TABLE `visum`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `akomodasi`
--
ALTER TABLE `akomodasi`
ADD CONSTRAINT `FK_akomodasi_gol` FOREIGN KEY (`id_golongan`) REFERENCES `golongan` (`id`);

--
-- Ketidakleluasaan untuk tabel `lhp`
--
ALTER TABLE `lhp`
ADD CONSTRAINT `lhp_ibfk_1` FOREIGN KEY (`id_spd`) REFERENCES `spd` (`id`) ON DELETE SET NULL;

--
-- Ketidakleluasaan untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
ADD CONSTRAINT `FK_pegawai_golongan` FOREIGN KEY (`id_golongan`) REFERENCES `golongan` (`id`);

--
-- Ketidakleluasaan untuk tabel `pengikut`
--
ALTER TABLE `pengikut`
ADD CONSTRAINT `FK_pengikut_pegawai` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id`),
ADD CONSTRAINT `FK_pengikut_spd` FOREIGN KEY (`id_spd`) REFERENCES `spd` (`id`);

--
-- Ketidakleluasaan untuk tabel `riil`
--
ALTER TABLE `riil`
ADD CONSTRAINT `FK_rill_spd` FOREIGN KEY (`id_spd`) REFERENCES `spd` (`id`),
ADD CONSTRAINT `riil_ibfk_1` FOREIGN KEY (`ppk`) REFERENCES `pejabat` (`id`) ON DELETE SET NULL;

--
-- Ketidakleluasaan untuk tabel `spd`
--
ALTER TABLE `spd`
ADD CONSTRAINT `spd_ibfk_3` FOREIGN KEY (`id_pejabat`) REFERENCES `pejabat` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
ADD CONSTRAINT `spd_ibfk_4` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `uang_harian`
--
ALTER TABLE `uang_harian`
ADD CONSTRAINT `FK_ssh_gol` FOREIGN KEY (`id_golongan`) REFERENCES `golongan` (`id`);

--
-- Ketidakleluasaan untuk tabel `user`
--
ALTER TABLE `user`
ADD CONSTRAINT `FK_user` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
