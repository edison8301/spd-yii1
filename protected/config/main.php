<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Surat Perjalanan Dinas',

	// preloading 'log' component
	'preload'=>array('log','bootstrap'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'reza',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
			'generatorPaths' => array(
				'bootstrap.gii'
			),
		),
		
		'jbackup'=>array(
            'path'=>Yii::app()->basePath.'/../backups/', //Directory where backups are saved
            'layout' => '//layouts/admin/column2', //2-column layout to display the options
            'filter' => 'accessControl', //filter or filters to the controller
            'bootstrap' => true, //if you want the module use bootstrap components
            'download' => true, // if you want the option to download
            'restore' => false, // if you want the option to restore
            'database' => true, //whether to make backup of the database or not
            //directory to consider for backup, must be made array key => value array ($ alias => $ directory)
            'directoryBackup'=>array( 
				//array(''=>Yii::app()->basePath.'/../css/')
            ),
            //directory sebe not take into account when the backup
            'excludeDirectoryBackup'=>array(
                //array(''=>Yii::app()->basePath.'/../css/')
            ),
            //files sebe not take into account when the backup
            'excludeFileBackup'=>array(
                __DIR__.'/../../folder/folder1/cfile.png',
            ),
            //directory where the backup should be done default Yii::getPathOfAlias('webroot')
            'directoryRestoreBackup'=>__DIR__.'/../../' 
        ),
		
	),

	// application components
	'components'=>array(
		'bootstrap'=>array(
			'class'=>'ext.bootstrap.components.Bootstrap',
			'responsiveCss'=>true,
		),
		'ePdf' => array(
			'class'=>'ext.yii-pdf.EYiiPdf',
			'params'=>array(
				 'HTML2PDF' => array(
					'librarySourcePath' => 'application.vendors.html2pdf.*',
					'classFile'         => 'html2pdf.class.php',
				)
			)
		),
		'user'=>array(
			// enable cookie-based authentication
			'class'=>'WebUser',
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		*/
		/*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		*/
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=spd',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);