<?php

class PegawaiController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.	
	*/
	
	public $layout='//layouts/admin/column2';

	/**
	* @return array action filters
	*/
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'accessRole'
		);
	}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete','profil',
					'excel','exportExcel','hapusFoto'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionProfil()
	{
		$this->render('profil',array(
			'model'=>$this->loadModel(Yii::app()->user->getState('pegawai_id')),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	
	public function actionCreate()
	{
		$model=new Pegawai;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pegawai']))
		{
			$model->attributes=$_POST['Pegawai'];
			
			$model->nip = str_replace(' ','',$model->nip);
			
			$foto = CUploadedFile::getInstance($model,'foto');
			
			if($foto!==null)
				$model->foto = str_replace(' ','-',time().'_'.$foto->name);
			
			if($model->save())
			{
				if($foto!==null)
				{
					$path = Yii::app()->basePath.'/../uploads/pegawai/';
					$foto->saveAs($path.$model->foto);
				}
				
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('/pegawai/create'));
			}
		}

		$pegawai=new Pegawai('search');
		$pegawai->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Pegawai']))
			$pegawai->attributes=$_GET['Pegawai'];

		$this->render('create',array(
			'model'=>$model,
			'pegawai'=>$pegawai,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$oldFoto = $model->foto;
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pegawai']))
		{
			$model->attributes=$_POST['Pegawai'];
			
			$model->nip = str_replace(' ','',$model->nip);
			
			$foto = CUploadedFile::getInstance($model,'foto');
			
			if($foto!==null)
				$model->foto = str_replace(' ','-',time().'_'.$foto->name);
			else	
				$model->foto = $oldFoto;
			
			if($model->save())
			{
				if($foto!==null)
				{
					$path = Yii::app()->basePath.'/../uploads/pegawai/';
					$foto->saveAs($path.$model->foto);
					
					if(file_exists($path.$oldFoto) AND $oldFoto != '')
						unlink($path.$oldFoto);
				} 
				
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('/pegawai/create'));
			}
		}

		$pegawai=new Pegawai('search');
		$pegawai->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Pegawai']))
			$pegawai->attributes=$_GET['Pegawai'];

		Yii::app()->user->setFlash('info','Silahkan sunting data pada kolom yang disediakan');
		
		$this->render('update',array(
			'model'=>$model,
			'pegawai'=>$pegawai,
		));
	}
	
	public function actionHapusFoto($id)
	{
		$model = $this->loadModel($id);
		$oldFoto = $model->foto;
		$model->foto = '';
		
		if($model->save())
		{
			$path = Yii::app()->basePath.'/../uploads/pegawai/';
			if(file_exists($path.$oldFoto) and $oldFoto != '')
				unlink($path.$oldFoto);
				
			Yii::app()->user->setFlash('success','Foto berhasil dihapus');
			$this->redirect(array('pegawai/update','id'=>$id));
		}
	}
	
	public function actionExcel()
	{
		$this->render('excel');
	}
	
	public function actionExportExcel() {

	     // get a reference to the path of PHPExcel classes 
		$phpExcelPath = Yii::getPathOfAlias('ext.phpexcel.Classes');
	 
		// Turn off our amazing library autoload 
		spl_autoload_unregister(array('YiiBase','autoload'));        
	 
		//
		// making use of our reference, include the main class
		// when we do this, phpExcel has its own autoload registration
		// procedure (PHPExcel_Autoloader::Register();)
		include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
	    
		spl_autoload_register(array('YiiBase','autoload'));        
		
	    $objPHPExcel = new PHPExcel();
	    
	    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
	    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);

	    	$objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue('A1', 'No')
		    ->setCellValue('B1', 'NIP')
		    ->setCellValue('C1', 'Nama')
		    ->setCellValue('D1', 'Golongan')
		    ->setCellValue('E1', 'Jabatan')
		    ->setCellValue('F1', 'Tgl Lahir');
	    $i=2;$no=1;
			foreach(Pegawai::model()->findAll() as $data){
		    
		    $objPHPExcel->setActiveSheetIndex(0)
		    ->setCellValue('A'.$i.'', $no)
		    ->setCellValue('B'.$i.'', $data->getNip())
		    ->setCellValue('C'.$i.'', $data->nama)
		    ->setCellValue('D'.$i.'', $data->golongan->nama)
		    ->setCellValue('E'.$i.'', $data->jabatan)
		    ->setCellValue('F'.$i.'', Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_lahir));
			 $i++;$no++;}


	 
	    $objPHPExcel->getActiveSheet()->setTitle('Data Pegawai');
	 
	    $objPHPExcel->setActiveSheetIndex(0);
	     
	    ob_end_clean();
	    ob_start();
	    
	    header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="Export_Data_Pegawai.xls"');
	    header('Cache-Control: max-age=0');
	    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	    $objWriter->save('php://output');
  	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Pegawai');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new Pegawai;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pegawai']))
		{
			$model->attributes=$_POST['Pegawai'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('admin'));
			}
		}

		$pegawai=new Pegawai('search');
		$pegawai->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Pegawai']))
			$pegawai->attributes=$_GET['Pegawai'];

		$this->render('admin',array(
			'model'=>$model,
			'pegawai'=>$pegawai,
		));
	}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Pegawai::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='pegawai-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
