<?php

class SpdController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	
	public $layout='//layouts/admin/column2';

	/**
	* @return array action filters
	*/
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'accessRole',
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','viewSpt','viewSpd','view2'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('inputSpt','updateSpt','InputSpd','cetakSpd','cetakSpt',
					'createSpd','delete','laporan','Ekspor','rekap','filter','setting',
					'updateSpd','cetakPdfSptSetdaTanpaPengikut','cetakPdfSptSetda',
					'cetakPdfSptDpkd','cetakPdfSptDpkdTanpaPengikut','CetakPdfSpdSetda',
					'cetakPdfSpdDpkd','cetakPdfSpt','excelSpd','excelSpt','cetakPdfSpd',
					'exportExcelSpt','exportExcelSpd','cetakPdfSpdLembar2','cetakPdfSpdMpdf',
				),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','adminspd','delete','laporan'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/

	public $numberFormat=array('decimals'=>2, 'decimalSeparator'=>',', 'thousandSeparator'=>'');

	public function actionViewSpt($id)
	{
		$this->render('viewSpt',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionViewSpd($id)
	{
		$this->render('viewSpd',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionView2($id)
	{
		$this->render('view2',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionExcelSpd()
	{
		$this->render('excelSpd');
	}
	
	public function actionExportExcelSpd()
	{
		$this->layout = '//layouts/excel';
		$this->render('_excelSpd',array('border'=>1));
	}

	
	public function actionExcelSpt()
	{
		$this->render('excelSpt');
	}
	
	public function actionExportExcelSpt()
	{
		$this->layout = '//layouts/excel';
		$this->render('_excelSpt',array('border'=>1));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	
	public function actionInputSpt()
	{
		$model=new Spd;
		$model2=new Pengikut;
		$spd = new Spd;
		
		$model->scenario = 'inputSpt';
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Spd']))
		{
			$model->attributes=$_POST['Spd'];
			if($model->save())
			{
				$errorPengikut = 0;
				
				//START INSERT PENGIKUT
				if(isset($_POST['Pengikut']))
				{
					foreach($_POST['Pengikut'] as $key => $id_pegawai)
					{
						if($id_pegawai!=null)
						{
							//CEK TANGGAL PERJALANAN PENGIKUT	
							$tanggal = $model->tgl_pergi;
							$error = 0;
		
							while($tanggal <= $model->tgl_kembali)
							{
								$perjalanan = Perjalanan::model()->findByAttributes(array('id_pegawai'=>$id_pegawai,'tanggal'=>$tanggal,'aktif'=>1));
		
								if($perjalanan !== null AND $perjalanan->id_spd != $model->id)
								{
									$error = 1;
									$errorPengikut = 1;
								}
			
								$tanggal = date('Y-m-d',strtotime(date("Y-m-d", strtotime($tanggal)) . " +1 day"));
							}
							
							if($error == 1)
							{
								Yii::app()->user->setFlash('error','Pengikut '.Pegawai::model()->findByPk($id_pegawai)->nama.' sedang dalam perjalanan dinas antara tanggal '.$model->tgl_pergi.' dan '.$model->tgl_kembali);
							}
							
							if($error == 0)
							{
								$pengikut = Pengikut::model()->findByAttributes(array('id_spd'=>$model->id,'id_pegawai'=>$id_pegawai));
							
								if($pengikut===null)
								{
									$pengikut = new Pengikut;
									$pengikut->id_spd = $model->id;
									$pengikut->id_pegawai = $id_pegawai;
								} 
							
								$pengikut->aktif = 1;
								$pengikut->save();
							}
						}
					}
				}		
				//END INSERT PENGIKUT
      
				//START INSERT PERJALANAN
				$tanggal = $model->tgl_pergi;
				
				while($tanggal <= $model->tgl_kembali)
				{
					//INPUT PERJALANAN PEGAWAI
					$perjalanan = Perjalanan::model()->findByAttributes(array('id_spd'=>$model->id,'id_pegawai'=>$model->id_pegawai,'tanggal'=>$tanggal));
					
					if($perjalanan === null)
					{
						$perjalanan = new Perjalanan;
						$perjalanan->id_spd = $model->id;
						$perjalanan->id_pegawai = $model->id_pegawai;
						$perjalanan->tanggal = $tanggal;
					}
							
					$perjalanan->aktif = 1;
					$perjalanan->save();
					
					//INPUT PERJALANAN PENGIKUT
					foreach($model->getDataPengikut() as $pengikut)
					{
						$perjalanan = Perjalanan::model()->findByAttributes(array('id_spd'=>$model->id,'id_pegawai'=>$pengikut->id_pegawai,'tanggal'=>$tanggal));
						
						if($perjalanan === null)
						{
							$perjalanan = new Perjalanan;
							$perjalanan->id_spd = $model->id;
							$perjalanan->id_pegawai = $pengikut->id_pegawai;
							$perjalanan->tanggal = $tanggal;
						}
						
						$perjalanan->aktif = 1;
						$perjalanan->save();
					}
					
					$tanggal = date('Y-m-d',strtotime(date("Y-m-d", strtotime($tanggal)) . " +1 day"));
				}
				
				if($errorPengikut)
					$this->redirect(array('updateSpt','id'=>$model->id));
				
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('inputSpt'));
			}
				
						
		}
		
		
		$spd=new Spd('search');
		$spd->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Spd']))
			$spd->attributes=$_GET['Spd'];
			
		$pegawai = new Pegawai;
		$pejabat = new Pejabat;
		
		$this->render('inputSpt',array(
			'model'=>$model,
			'model2'=>$model2,
			'spd'=>$spd,
			'pegawai'=>$pegawai,
			'pejabat'=>$pejabat
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdateSpt($id)
	{
		$model=$this->loadModel($id);
		//$model = new Spd;
		$model2 = new Pengikut;
		$spd = new Spd;
		
		$model->scenario = 'updateSpt';
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Spd']))
		{
			$model->attributes=$_POST['Spd'];
			
			if($model->save())
			{
				$errorPengikut = 0;
				
				//START UPDATE DATA PENGIKUT
				if(isset($_POST['Pengikut']))
				{
					foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$model->id)) as $data)
					{	
						$data->aktif = 0;
						$data->save();
					}
					
					foreach($_POST['Pengikut'] as $key => $id_pegawai)
					{
						if($id_pegawai!=null)
						{
							
							//CEK TANGGAL PERJALANAN PENGIKUT	
							$tanggal = $model->tgl_pergi;
							$error = 0;
		
							while($tanggal <= $model->tgl_kembali)
							{
								$perjalanan = Perjalanan::model()->findByAttributes(array('id_pegawai'=>$id_pegawai,'tanggal'=>$tanggal,'aktif'=>1));
		
								if($perjalanan !== null AND $perjalanan->id_spd != $model->id)
								{
									$error = 1;
									$errorPengikut = 1;
								}
			
								$tanggal = date('Y-m-d',strtotime(date("Y-m-d", strtotime($tanggal)) . " +1 day"));
							}
							
							if($error == 1)
							{
								Yii::app()->user->setFlash('error','Pengikut '.Pegawai::model()->findByPk($id_pegawai)->nama.' sedang dalam perjalanan dinas antara tanggal '.$model->tgl_pergi.' dan '.$model->tgl_kembali);
							}
							
							if($error == 0)
							{
								$pengikut = Pengikut::model()->findByAttributes(array('id_spd'=>$model->id,'id_pegawai'=>$id_pegawai));
							
								if($pengikut===null)
								{
									$pengikut = new Pengikut;
									$pengikut->id_spd = $model->id;
									$pengikut->id_pegawai = $id_pegawai;
								} 
							
								$pengikut->aktif = 1;
								$pengikut->save();
							}
						}
					}
					
				}
				//END UPDATE DATA PENGIKUT
				
				//START UPDATE DATA PERJALANAN
				foreach(Perjalanan::model()->findAllByAttributes(array('id_spd'=>$model->id)) as $data)
				{
					$data->aktif = 0;
					$data->save();
				}
				
				$tanggal = $model->tgl_pergi;
				while($tanggal <= $model->tgl_kembali)
				{
					//INPUT PERJALANAN PEGAWAI
					$perjalanan = Perjalanan::model()->findByAttributes(array('id_spd'=>$model->id,'id_pegawai'=>$model->id_pegawai,'tanggal'=>$tanggal));
					if($perjalanan === null)
					{
						$perjalanan = new Perjalanan;
						$perjalanan->id_spd = $model->id;
						$perjalanan->id_pegawai = $model->id_pegawai;
						$perjalanan->tanggal = $tanggal;
					}
						
					$perjalanan->aktif = 1;
					$perjalanan->save();
					
					//INPUT PERJALANAN PENGIKUT
					foreach($model->getDataPengikut() as $pengikut)
					{
						$perjalanan = Perjalanan::model()->findByAttributes(array('id_spd'=>$model->id,'id_pegawai'=>$pengikut->id,'tanggal'=>$tanggal));
						if($perjalanan === null)
						{
							$perjalanan = new Perjalanan;
							$perjalanan->id_spd = $model->id;
							$perjalanan->id_pegawai = $pengikut->id_pegawai;
							$perjalanan->tanggal = $tanggal;
						}
						
						$perjalanan->aktif = 1;
						$perjalanan->save();
					}
					
					$tanggal = date('Y-m-d',strtotime(date("Y-m-d", strtotime($tanggal)) . " +1 day"));
				}
				//END UPDATE DATA PERJALANAN
				
				if($errorPengikut)
					$this->redirect(array('updateSpt','id'=>$model->id));
				
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('inputSpt'));
			}
		}
		
		Yii::app()->user->setFlash('info','Silahkan sunting data pada kolom yang disediakan');

		$this->render('updateSpt',array(
			'model'=>$model,
			'model2'=>$model2,
			'spd'=>$spd,
		));
	}

	public function actionUpdateSpd($id)
	{
		$model=$this->loadModel($id);
		
		$model->scenario = 'updateSpd';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Spd']))
		{
			$model->attributes=$_POST['Spd'];
			
			$model->biaya = str_replace('.','',$model->biaya);
			
			if($model->save())
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
			$this->redirect(array('inputSpd'));
		}

		$this->render('updateSpd',array(
			'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Spd');
		$this->render('index',array(
		'dataProvider'=>$dataProvider,
		));
	}

		/**
		* Manages all models.
		*/
		public function actionCetakSpt()
		{
			$spd=new Spd('search');
			$spd->unsetAttributes();  // clear any default values
			
			if(isset($_GET['Spd']))
				$spd->attributes=$_GET['Spd'];

			$this->render('cetakSpt',array(
				'spd'=>$spd,
			));
		}

	public function actionInputSpd()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'nomor = ""';
			
		$model=new Spd('search',array(
			'criteria'=>$criteria
		));
			
		$model->unsetAttributes();  // clear any default values
			
		if(isset($_GET['Spd']))
			$model->attributes=$_GET['Spd'];

		$this->render('inputSpd',array(
			'model'=>$model,
		));
	}

	public function actionCetakSpd()
	{
		$model=new Spd('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['Spd']))
			$model->attributes=$_GET['Spd'];

		$this->render('cetakSpd',array(
			'model'=>$model,
		));
	}

		public function actionLaporan()
		{
			$model=new Spd('search');
			$model->unsetAttributes();  // clear any default values

			if(isset($_GET['Spd']))
			$model->attributes=$_GET['Spd'];

			$this->render('laporan',array(
			'model'=>$model,
			));
		}
		
	public function actionCetakPdfSpdLembar2($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A3', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Surat Perjalanan Dinas');
		$pdf->SetSubject('Surat Perjalanan Dinas');
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage('P', 'A3');
		$pdf->SetFont('times', '', 11);
		
		$model = $this->loadModel($id);
		
		$html .= '<p style="text-align:right;line-height:100%"><b>Lampiran VI<br>Permenkeu RI No. 113/PMK 05/2012</b></p>';
			
		$html .= '
			   <table border="1" style="padding:5px">
			 	<tr>
			 		<td width="50%" >
			 		</td>
			 		<td width="50%" >
			 			<table>
				 			<tr>
				 				<td>I.Berangkat dari (Tempat Kedudukan)</td>
				 				<td>: '.$model->tempat_berangkat.'</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>: '.$model->tujuan.'</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>: '.Bantu::tanggal(($model->tgl_pergi)).'</td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"> '.$model->getRelationField("pejabat","jabatan").'</td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><p style="text-decoration:underline;"><b>'.$model->pejabat->nama.'</b></p></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2">NIP.'.$model->getRelationField("pejabat","nip").'</td>
				 			</tr>
			 			</table>
			 		</td>
			 	</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>II.Tiba di</td>
				 				<td>: '.$model->tujuan.'</td>
				 			</tr>
				 			<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>: '.Bantu::tanggal(($model->tgl_pergi)).'</td>
				 			</tr>
				 			<tr><td></td></tr>
				 			<tr><td></td></tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>: '.$model->tujuan.'</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>: '.$model->tempat_berangkat.'</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>: '.Bantu::tanggal(($model->tgl_kembali)).'</td>
				 			</tr>
				 			<tr><td></td></tr>
				 			<tr><td></td></tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>III.Tiba di</td>
				 				<td>:</td>
				 			</tr>
				 			
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>IV.Tiba di</td>
				 				<td>:</td>
				 			</tr>
				 			
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>V.Tiba di</td>
				 				<td>:</td>
				 			</tr>
				 			
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>VI.Tiba di</td>
				 				<td>: '.$model->tempat_berangkat.'</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>: '.Bantu::tanggal(($model->tgl_kembali)).'</td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2">An. Pejabat yang berwenang/<br>Pejabat lainnya yang di tunjuk</td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><p style="text-decoration:underline;"><b>'.$model->pejabat->nama.'</b></p></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2">NIP.'.$model->getRelationField("pejabat","nip").'</td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td><p>Telah diperiksa dengan keterangan bahwa perjalanan
				 						tersebut atas perintahnya dan semata-mata untuk
				 						kepentingan jabatan dan waktu yang sesingkat-singkatnya.</p>
				 				</td>
				 			</tr>
				 			<tr>
				 				<td align="center">An. Pejabat yang berwenang/<br>Pejabat lainnya yang di tunjuk</td>
				 			</tr>
				 			<tr>
				 				<td align="center"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center"><p style="text-decoration:underline;"><b>'.$model->getRelationField("pejabat","nama").'</b><br>NIP. '.$model->getRelationField("pejabat","nip").'</p></td>
				 			</tr>

			 			</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						Catatan lain lain:
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table>
							<tr>
								<td width="5%">
									<b>VII.</b>
								</td>
								<td width="95%">
									<b>PERHATIAN</b>
								<p>Pejabat yang berwenang menerbitkan SPPD, pegawai yang melakukan perjalanan dinas, para pejabat yang mengesahkan tanggal
								berangkat/tiba, serta bendaharawan bertanggung jawab berdasarkan peraturan-peraturan keuangan Negara apabila negara menderita rugi
								akibat kesalahan, kelalaian dan kealpaannya.</p>
								</td>
							</tr>
						</table>
						
					</td>
				</tr>
				';
	
		$html .='</table>';
		
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

		public function actionRekap()
		{
			
			if(isset($_GET['tgl_awal']) & isset($_GET['tgl_akhir'])) {
				$criteria = new CDbCriteria;
				$criteria->params = array(':tgl_awal'=>$_GET['tgl_awal'], ':tgl_akhir'=>$_GET['tgl_akhir']);
				$criteria->condition = 'tanggal >= :tgl_awal AND tanggal <= :tgl_akhir AND nomor IS NOT NULL';
			
				/*$spd = Spd::model()->findAll($criteria);
				if($spd==null){
					$data= 0;
				}else { $data = Spd::model()->findAll($criteria);}*/
				$model = Spd::model()->findAll($criteria);
			} else {
				$criteria = new CDbCriteria;
				$criteria->condition = 'nomor IS NOT NULL';
				//$spd = Spd::model()->findAll($criteria);
				$model = Spd::model()->findAll($criteria);
			}
			

			$this->render('rekap',array(
				'model'=>$model,
			));
		}

		

		public function actionSetting()
		{
			$model=new Spd('search');
			$model->unsetAttributes();  // clear any default values

			if(isset($_GET['Spd']))
			$model->attributes=$_GET['Spd'];

			$this->render('setting',array(
			'model'=>$model,
			));
		}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=Spd::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function actionEkspor()
	{
		$data = array(
		    1 => array ('Name', 'Surname'),
		    array('Schwarz', 'Oliver'),
		    array('Test', 'Peter')
		);
		Yii::import('application.extensions.phpexcel.JPhpExcel');
		$xls = new JPhpExcel('UTF-8', false, 'My Test Sheet');
		$xls->addArray($data);
		$xls->generateXML('my-test');

	}
	
	public function actionCetakPdfSpt($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'F4', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perintah Tugas');
		$pdf->SetTitle('Surat Perintah Tugas');
		$pdf->SetSubject('Surat Perintah Tugas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 12);
		
		$model = $this->loadModel($id);
		
		$html = '';
		
		if(isset($_GET['kop']) AND $_GET['kop']=='setda')
		{
			$html .= ' 
				<table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%">
							<p style="font-size:70px;line-height:100%;text-align:center">
							<b>PEMERINTAH KOTA SERANG</b><br>
							<b>SEKRETARIAT DAERAH</b><br>
							<i style="font-size:29px;">Jl. Jendral Sudirman Kawasan Perumahan Kota Serang Baru Telp.(0254)210538,FAX.(0254)202810</i></p>
						</td>
					</tr>
				</table>';
		}
		
		if(isset($_GET['kop']) AND $_GET['kop']=='dpkd')
		{
			$html .=' <table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%" >
							<div style="font-size:70px;line-height:93%;text-align:center" >
							<b>PEMERINTAH KOTA SERANG</b><br>
							<b style="font-size:50px;">DINAS PENGELOLAAN KEUANGAN DAERAH</b><br>
							( D P K D )<br>
							<i style="font-size:29px;">Jl. Jendral Sudirman Kawasan Perumahan Kota Serang Baru Telp.(0254)210538,FAX.(0254)202810</i></p>
						</td>
					</tr>
				</table>';
		}
		
		$html .= '
			   	
				
				<div style="text-align:center">
					<b><u>S U R A T &nbsp; T U G A S</u></b><br>
					Nomor: '.$model->nomor_spt.'
				</div>
				
			 	<div>Yang bertanda tangan dibawah ini:</div>
				
				<table>	
					<tr>
						<td width="25%" >Nama</td>
						<td width="75%" >: <b>'.$model->getRelationField("pejabat","nama").'</b></td>
					</tr>
					<tr>
						<td width="25%" >NIP</td>
						<td width="75%" >: '.$model->getRelationField("pejabat","nip").'</td>
					</tr>
					<tr>
						<td width="25%" >Jabatan</td>
						<td width="75%" >: '.$model->getRelationField("pejabat","jabatan").'</td>
					</tr>
				</table>
				
				<div style="text-align:center"><b>Memberikan Perintah Kepada:</b></div>
				
				<table>
					<tr>
						<td width="25%" >Nama</td>
						<td width="75%" >: <b>'.$model->getRelationField("pegawai","nama").'</b></td>
					</tr>
					<tr>
						<td width="25%" >NIP</td>
						<td width="75%" >: '.$model->getRelationField("pegawai","nip").'</td>
					</tr>
					<tr>
						<td width="25%" >Jabatan</td>
						<td width="75%" >: '.$model->getRelationField("pegawai","jabatan").'</td>
					</tr>
				</table>
				
				<div>Untuk melaksanakan tugas kegiatan Perjalanan Dinas:</div>
				<br>
				<table>
					<tr>
						<td width="30%" >Tujuan</td>
						<td width="70%" >: '.$model->tujuan.'</td>
					</tr>
					<tr>
						<td width="30%" >Tanggal Berangkat</td>
						<td width="70%" >: '.Bantu::tanggal(($model->tgl_pergi)).'</td>
					</tr>
					<tr>
						<td width="30%" >Tanggal kembali</td>
						<td width="70%" >: '.Bantu::tanggal(($model->tgl_kembali)).'</td>
					</tr>
				</table>';
				
		
		if(isset($_GET['tanpaPengikut']) AND $_GET['tanpaPengikut']==1)
		{
			$html .= '';
		
		} else {
			
			$html .= '
				<div><b>Pengikut:</b><br></div>
				
				<table border="1px" style="border:1px solid #000;" height="10%" cellpadding="3px" cellspacing="0px">
				<tr>
						<td width="33%" style="border-bottom:1px solid #000;border-right:1px solid #000;text-align:center"><b>NIP</b></td>
						<td width="33%" style="border-bottom:1px solid #000;border-right:1px solid #000;text-align:center"><b>Nama</b></td>
						<td width="34%" style="border-bottom:1px solid #000;border-right:1px solid #000;text-align:center"><b>Jabatan</b></td>
				</tr>';
		
			foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$id,'aktif'=>1)) as $pengikut)
			{
				$html .= 
					'<tr style="line-height:100%;margin:0px;">
						<td width="33%" style="border-right:1px solid #000;">'.$pengikut->getRelationField("pegawai","nip").'</td>
						<td width="33%" style="border-right:1px solid #000;">'.$pengikut->getRelationField("pegawai","nama").'</td>
						<td width="34%" style="border-right:1px solid #000;">'.$pengikut->getRelationField("pegawai","jabatan").'</td>
					</tr>';
			
			}
			
			$html .='</table>';
		}
		
		$html .= '<p style="line-height:10%;">&nbsp;</p>';
		
		$html .='
				<p>Kegiatan yang harus dilaksanakan:<br>&nbsp;&nbsp;<i>'.$model->maksud.'</i></p><br>
				<p>Demikian Surat Perintah ini di buat, agar dilaksanakan dengan sebaik-baiknya.</p><br>
				<table>
					<tr>
						<td width="50%">&nbsp;</td>
						<td width="50%" align="center">
							<p>Serang, '.Bantu::tanggal(($model->tanggal_spt)).'<br>
							Yang Memberi Perintah</p>
							<p>&nbsp;</p>
							<p style="line-height:110%;"><span style="text-decoration:underline;"><b>'.$model->getRelationField("pejabat","nama").'</b></span>
							<br>NIP. '.$model->getRelationField("pejabat","nip").'</p>
						</td>
					</tr>
				</table>';
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }
	
	public function actionCetakPdfSptSetdaTanpaPengikut($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A4', true, 'UTF-8');

		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perintah Tugas');
		$pdf->SetTitle('Surat Perintah Tugas');
		$pdf->SetSubject('Surat Perintah Tugas');
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 12);
		
		$model = $this->loadModel($id);
		
		$html = '<table style="border-bottom:1px;">';
		$html .= '<tr>';
		$html .= 	'<td width="15%"><img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px"></td>';
		$html .= 	'<td width="85%" style="text-align:center">';
		$html .= 		'<p style="font-size:70px;line-height:100%"><b>PEMERINTAH KOTA SERANG</b><br>';
		$html .= 		'<b>SEKRETARIAT DAERAH</b><br>';
		$html .= 		'<i style="font-size:29px;">Jl. Jendral Sudirman Kawasan Perumahan Kota Serang Baru Telp.(0254)210538,FAX.(0254)202810</i></p>';
		$html .= 	'</td>';
		$html .= '</tr>';
		$html .= '</table>';
		
		$html .= '<div style="text-align:center">';
		$html .= 	'<b><u>S U R A T &nbsp;&nbsp; T U G A S</u></b> <br>';
		$html .= 	'Nomor: '.$model->nomor_spt;
		$html .= '</div>';
		$html .= '<div style="margin-bottom:0px">Yang bertanda tangan dibawah ini:</div>';
		
		$html .= '<table>';
		$html .= '<tr>';
		$html .= 	'<td width="30%" >Nama</td>';
		$html .= 	'<td width="70%" >: <b>'.$model->getRelationField("pejabat","nama").'</b></td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= 	'<td width="30%" >NIP</td>';
		$html .= 	'<td width="70%" >: '.$model->getRelationField("pejabat","nip").'</td>';
		$html .= '</tr>';
		$html .= '<tr>';
		$html .= 	'<td width="30%" >Jabatan</td>';
		$html .=	'<td width="70%" >: '.$model->getRelationField("pejabat","jabatan").'</td>';
		$html .= '</tr>';
		$html .= '</table>';
		
		$html .= '
				
				<div style="margin:5px" align="center"><b>Memberikan Perintah Kepada:</b></div>
				<table style="margin:5px">
					<tr>
						<td width="30%" >Nama</td>
						<td width="70%" >: <b>'.$model->getRelationField("pegawai","nama").'</b></td>
					</tr>
					<tr>
						<td width="30%" >NIP</td>
						<td width="70%" >: '.$model->getRelationField("pegawai","nip").'</td>
					</tr>
					<tr>
						<td width="30%" >Jabatan</td>
						<td width="70%" >: '.$model->getRelationField("pegawai","jabatan").'</td>
					</tr>
				</table>
				
				<div style="margin:5px">Untuk melaksanakan tugas kegiatan Perjalanan Dinas:</div>
				<br>
				<table>
					<tr>
						<td width="30%" >Tujuan</td>
						<td width="70%" >: '.$model->tujuan.'</td>
					</tr>
					<tr>
						<td width="30%" >Tanggal Berangkat</td>
						<td width="70%" >: '.Bantu::tanggal(($model->tgl_pergi)).'</td>
					</tr>
					<tr>
						<td width="30%" >Tanggal kembali</td>
						<td width="70%" >: '.Bantu::tanggal(($model->tgl_kembali)).'</td>
					</tr>
				</table>';		

		
		
			
		$html .='
				<p>Kegiatan yang harus dilaksanakan:<br>&nbsp;&nbsp;&nbsp;<i>'.$model->maksud.'</i></p><br>
				<p>Demikian Surat Perintah ini di buat, agar dilaksanakan dengan sebaik-baiknya.</p><br>
				
				<table>
					<tr>
						<td width="50%">
						</td>
						<td width="50%" align="center">
							<p>Serang, '.Bantu::tanggal(($model->tanggal)).'<br>
							Yang Memberi Perintah</p>
							<p>&nbsp;</p>
							<p style="line-height:70%;"><span style="text-decoration:underline;"><b>'.$model->pejabat->nama.'</b></span></p>
							<p style="line-height:10%;">NIP. '.$model->pejabat->nip.'</p>
						</td>
					</tr>
				</table>';
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }
	
	public function actionCetakPdfSptSetda($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A3', true, 'UTF-8');
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perintah Tugas');
		$pdf->SetTitle('Surat Perintah Tugas');
		$pdf->SetSubject('Surat Perintah Tugas');
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 12);
		
		$model = $this->loadModel($id);
		
		$html=' 
				<table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%">
							<p style="font-size:70px;line-height:100%;text-align:center">
							<b>PEMERINTAH KOTA SERANG</b><br>
							<b>SEKRETARIAT DAERAH</b><br>
							<i style="font-size:29px;">Jl. Jendral Sudirman Kawasan Perumahan Kota Serang Baru Telp.(0254)210538,FAX.(0254)202810</i></p>
						</td>
					</tr>
				</table>';
		
		$html .= '
			   	
				
				<div style="text-align:center">
					<b><u>S U R A T &nbsp; T U G A S</u></b><br>
					Nomor: '.$model->nomor_spt.'
				</div>
				
			 	<div>Yang bertanda tangan dibawah ini:</div>
				
				<table>	
					<tr>
						<td width="25%" >Nama</td>
						<td width="75%" >: <b>'.$model->pejabat->nama.'</b></td>
					</tr>
					<tr>
						<td width="25%" >NIP</td>
						<td width="75%" >: '.$model->pejabat->nip.'</td>
					</tr>
					<tr>
						<td width="25%" >Jabatan</td>
						<td width="75%" >: '.$model->pejabat->jabatan.'</td>
					</tr>
				</table>
				
				<div style="text-align:center"><b>Memberikan Perintah Kepada:</b></div>
				
				<table>
					<tr>
						<td width="25%" >Nama</td>
						<td width="75%" >: <b>'.$model->pegawai->nama.'</b></td>
					</tr>
					<tr>
						<td width="25%" >NIP</td>
						<td width="75%" >: '.$model->pegawai->nip.'</td>
					</tr>
					<tr>
						<td width="25%" >Jabatan</td>
						<td width="75%" >: '.$model->pegawai->jabatan.'</td>
					</tr>
				</table>
				
				<div>Untuk melaksanakan tugas kegiatan Perjalanan Dinas:</div>
				<br>
				<table>
					<tr>
						<td width="30%" >Tujuan</td>
						<td width="70%" >: '.$model->tujuan.'</td>
					</tr>
					<tr>
						<td width="30%" >Tanggal Berangkat</td>
						<td width="70%" >: '.Bantu::tanggal(($model->tgl_pergi)).'</td>
					</tr>
					<tr>
						<td width="30%" >Tanggal kembali</td>
						<td width="70%" >: '.Bantu::tanggal(($model->tgl_kembali)).'</td>
					</tr>
				</table>';
				
					
		$html .= '
				<div><b>Pengikut:</b><br></div>
				
				<table border="1px" style="border:1px solid #000;" height="10%" cellpadding="3px" cellspacing="0px">
				<tr>
						<td width="33%" style="border-bottom:1px solid #000;border-right:1px solid #000;text-align:center"><b>NIP</b></td>
						<td width="33%" style="border-bottom:1px solid #000;border-right:1px solid #000;text-align:center"><b>Nama</b></td>
						<td width="34%" style="border-bottom:1px solid #000;border-right:1px solid #000;text-align:center"><b>Jabatan</b></td>
				</tr>
		';
		
		foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$id,'aktif'=>'1')) as $pengikut)
		{
			$html .= 
					'<tr style="line-height:100%;margin:0px;">
						<td width="33%" style="border-right:1px solid #000;">'.$pengikut->pegawai->nip.'</td>
						<td width="33%" style="border-right:1px solid #000;">'.$pengikut->pegawai->nama.'</td>
						<td width="34%" style="border-right:1px solid #000;">'.$pengikut->pegawai->jabatan.'</td>
					</tr>';
			
		}
			
		$html .='</table>';
		
		$html .= '<p style="line-height:10%;">&nbsp;</p>';
		
		$html .='
				<p>Kegiatan yang harus dilaksanakan:<br>&nbsp;&nbsp;<i>'.$model->maksud.'</i></p><br>
				<p>Demikian Surat Perintah ini di buat, agar dilaksanakan dengan sebaik-baiknya.</p><br>
				<table>
					<tr>
						<td width="50%">&nbsp;</td>
						<td width="50%" align="center">
							<p>Serang, '.Bantu::tanggal(($model->tanggal)).'<br>
							Yang Memberi Perintah</p>
							<p>&nbsp;</p>
							<p style="line-height:70%;"><span style="text-decoration:underline;"><b>'.$model->pejabat->nama.'</b></span></p>
							<p style="line-height:10%;">NIP. '.$model->pejabat->nip.'</p>
						</td>
					</tr>
				</table>';
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }
	
	 public function actionCetakPdfSptDpkd($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A3', true, 'UTF-8');

	    
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perintah Tugas');
		$pdf->SetTitle('Surat Perintah Tugas');
		$pdf->SetSubject('Surat Perintah Tugas');
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 12);
		
		$model = $this->loadModel($id);
		
		$html=' <table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%" >
							<div style="font-size:70px;line-height:93%;text-align:center" >
							<b>PEMERINTAH KOTA SERANG</b><br>
							<b style="font-size:50px;">DINAS PENGELOLAAN KEUANGAN DAERAH</b><br>
							( D P K D )<br>
							<i style="font-size:29px;">Jl. Jendral Sudirman Kawasan Perumahan Kota Serang Baru Telp.(0254)210538,FAX.(0254)202810</i></p>
						</td>
					</tr>
				</table>';
			
			
		$html .= '
			   	<div style="text-align:center">
					<b><u>S U R A T &nbsp; T U G A S</u></b><br>
					Nomor: '.$model->nomor_spt.'
				</div>

			 	<div>Yang bertanda tangan dibawah ini:</div>
				
				<table>	
					<tr>
						<td width="25%" >Nama</td>
						<td width="75%" >: <b>'.$model->pejabat->nama.'</b></td>
					</tr>
					<tr>
						<td width="25%" >NIP</td>
						<td width="75%" >: '.$model->pejabat->nip.'</td>
					</tr>
					<tr>
						<td width="25%" >Jabatan</td>
						<td width="75%" >: '.$model->pejabat->jabatan.'</td>
					</tr>
				</table>
				
				<div style="text-align:center"><b>Memberikan Perintah Kepada:</b></div>
				
				<table>
					<tr>
						<td width="25%" >Nama</td>
						<td width="75%" >: <b>'.$model->pegawai->nama.'</b></td>
					</tr>
					<tr>
						<td width="25%" >NIP</td>
						<td width="75%" >: '.$model->pegawai->nip.'</td>
					</tr>
					<tr>
						<td width="25%" >Jabatan</td>
						<td width="75%" >: '.$model->pegawai->jabatan.'</td>
					</tr>
				</table>
				
				<div>Untuk melaksanakan tugas kegiatan Perjalanan Dinas:</div>
				
				<table>
					<tr>
						<td width="25%" >Tujuan</td>
						<td width="75%" >: '.$model->tujuan.'</td>
					</tr>
					<tr>
						<td width="25%" >Tanggal Berangkat</td>
						<td width="75%" >: '.Bantu::tanggal(($model->tgl_pergi)).'</td>
					</tr>
					<tr>
						<td width="25%" >Tanggal kembali</td>
						<td width="75%" >: '.Bantu::tanggal(($model->tgl_kembali)).'</td>
					</tr>
				</table>';
				
					
		$html .='
				<div><b>Pengikut:</b></div>
				<br>
				<table border="0px" style="border:1px solid #000" cellpadding="2px">
					<tr>
						<td width="33%" style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;"><b>NIP</b></td>
						<td width="33%" style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;"><b>Nama</b></td>
						<td width="34%" style="text-align:center;border-bottom:1px solid #000;border-right:1px solid #000;"><b>Jabatan</b></td>
					</tr>
				';
				foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$id,'aktif'=>1)) as $pengikut)
					{
					$html .='
						<tr>
							<td width="33%" style="border-right:1px solid #000;">'.$pengikut->pegawai->nip.'</td>
							<td width="33%" style="border-right:1px solid #000;">'.$pengikut->pegawai->nama.'</td>
							<td width="34%" style="border-right:1px solid #000;">'.$pengikut->pegawai->jabatan.'</td>
						</tr>';
					}
				
		$html .='</table>

		';
		
	
		$html .='
				<div>Kegiatan yang harus dilaksanakan:<br>&nbsp;&nbsp;<i>'.$model->maksud.'</i></div>
				
				<div>Demikian Surat Perintah ini di buat, agar dilaksanakan dengan sebaik-baiknya.</div>
				
				<div>&nbsp;</div>
				
				<table>
					<tr>
						<td width="50%">&nbsp;</td>
						<td width="50%" align="center">
							<p>Serang, '.Bantu::tanggal(($model->tanggal)).'<br>
							Yang Memberi Perintah</p>
							<p>&nbsp;</p>
							<p style="line-height:70%;"><span style="text-decoration:underline;"><b>'.$model->pejabat->nama.'</b></span></p>
							<p style="line-height:10%;">NIP. '.$model->pejabat->nip.'</p>
						</td>
					</tr>
				</table>';
		
		
		
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }
	
	public function actionCetakPdfSptDpkdTanpaPengikut($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A4', true, 'UTF-8');

		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perintah Tugas');
		$pdf->SetTitle('Surat Perintah Tugas');
		$pdf->SetSubject('Surat Perintah Tugas');
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 12);
		
		$model = $this->loadModel($id);
		
		$html=' <table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%" >
							<div style="font-size:70px;line-height:93%;text-align:center" >
							<b>PEMERINTAH KOTA SERANG</b><br>
							<b style="font-size:50px;">DINAS PENGELOLAAN KEUANGAN DAERAH</b><br>
							( D P K D )<br>
							<i style="font-size:29px;">Jl. Jendral Sudirman Kawasan Perumahan Kota Serang Baru Telp.(0254)210538,FAX.(0254)202810</i></div>
						</td>
					</tr>
				</table>';
			
			
		$html .= '
			   	<div style="text-align:center">
					<b><u>S U R A T &nbsp;&nbsp; T U G A S</u></b><br>
					Nomor: '.$model->nomor_spt.'
				</div>


			 	<div>Yang bertanda tangan dibawah ini:</div>
				
				<table>	
					<tr>
						<td width="25%" >Nama</td>
						<td width="75%" >: <b>'.$model->pejabat->nama.'</b></td>
					</tr>
					<tr>
						<td width="25%" >NIP</td>
						<td width="75%" >: '.$model->pejabat->nip.'</td>
					</tr>
					<tr>
						<td width="25%" >Jabatan</td>
						<td width="75%" >: '.$model->pejabat->jabatan.'</td>
					</tr>
				</table>
				<div style="text-align:center"><b>Memberikan Perintah Kepada:</b></div>
				
				<table>
					<tr>
						<td width="25%" >Nama</td>
						<td width="75%" >: <b>'.$model->pegawai->nama.'</b></td>
					</tr>
					<tr>
						<td width="25%" >NIP</td>
						<td width="75%" >: '.$model->pegawai->nip.'</td>
					</tr>
					<tr>
						<td width="25%" >Jabatan</td>
						<td width="75%" >: '.$model->pegawai->jabatan.'</td>
					</tr>
				</table>
				
				<div>Untuk melaksanakan tugas kegiatan Perjalanan Dinas:</div>
				
				<table>
					<tr>
						<td width="30%" >Tujuan</td>
						<td width="70%" >: '.$model->tujuan.'</td>
					</tr>
					<tr>
						<td width="30%" >Tanggal Berangkat</td>
						<td width="70%" >: '.Bantu::tanggal(($model->tgl_pergi)).'</td>
					</tr>
					<tr>
						<td width="30%" >Tanggal kembali</td>
						<td width="70%" >: '.Bantu::tanggal(($model->tgl_kembali)).'<br></td>
					</tr>
				</table>';

		
		
		$html .='
				<div>Kegiatan yang harus dilaksanakan:<br>&nbsp;&nbsp;<i>'.$model->maksud.'</i></div>
				<div>Demikian Surat Perintah ini di buat, agar dilaksanakan dengan sebaik-baiknya.</div>
				<Br>
				<table>
					<tr>
						<td width="50%">
						</td>
						<td width="50%" align="center">
							<p>Serang, '.Bantu::tanggal(($model->tanggal)).'<br>
							Yang Memberi Perintah</p>
							<p>&nbsp;</p>
							<p style="line-height:70%;"><span style="text-decoration:underline;"><b>'.$model->pejabat->nama.'</b></span></p>
							<p style="line-height:10%;">NIP '.$model->pejabat->nip.'</p>
						</td>
					</tr>
				</table>';
		
		
		
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }
	
	public function actionCetakPdfSpd($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');

		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Data Pegawai');
		$pdf->SetSubject('Data Pegawai');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		$model = $this->loadModel($id);
		
		$html = '';
		
		if(isset($_GET['kop']) AND $_GET['kop']=='setda')
		{
		
			$html .=' <table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%">
							<div style="font-size:70px;line-height:101%;text-align:center">
							<b>PEMERINTAH KOTA SERANG</b><br>
							<b>SEKRETARIAT DAERAH</b><br>
							<i style="font-size:29px;">Jl. Jendral Sudirman Kawasan Perumahan Kota Serang Baru Telp.(0254)210538,FAX.(0254)202810</i></div>
						</td>
					</tr>
				</table>';
		}
		
		if(isset($_GET['kop']) AND $_GET['kop']=='dpkd')
		{
			$html .=' <table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%" >
							<div style="line-height:120%;text-align:center" >
							<b style="font-size:70px;">PEMERINTAH KOTA SERANG</b><br>
							<b style="font-size:50px;">DINAS PENGELOLAAN KEUANGAN DAERAH</b><br>
							<b style="font-size:50px">( D P K D )</b><br>
							<i style="font-size:29px;">Jl. Jendral Sudirman Kawasan Perumahan Kota Serang Baru Telp.(0254)210538,FAX.(0254)202810</i><br>
							&nbsp;</div>
						</td>
					</tr>
				</table>';
		}
		
		$pejabat_berwenang = $model->getRelationField("pejabat","jabatan");
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $pejabat_berwenang = "&nbsp;";
		
		$nama = $model->getRelationField("pegawai","nama").'<br>'.$model->getRelationNip("pegawai");
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $nama = "&nbsp;";
		
		$pangkat = $model->getRelationRelationField("pegawai","golongan","nama");
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $pangkat = "&nbsp;";
		
		$jabatan = $model->getRelationField("pegawai","jabatan");
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $jabatan = "&nbsp;";
		
		$biaya = '-';
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $biaya = "&nbsp;";
		
		$maksud = $model->maksud;
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $maksud = "&nbsp;";
		
		$kendaraan = $model->kendaraan;
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $kendaraan = "&nbsp;";
		
		$tempat_berangkat = $model->tempat_berangkat;
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $tempat_berangkat = "&nbsp;";
		
		$tempat_tujuan = $model->tujuan;
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $tempat_tujuan = "&nbsp;";
		
		$lama = $model->lama.' Hari';
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $lama = "&nbsp;";
		
		$tgl_pergi = Bantu::tanggal(($model->tgl_pergi));
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $tgl_pergi = "&nbsp;";
		
		$tgl_kembali = Bantu::tanggal(($model->tgl_kembali));
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $tgl_kembali = "&nbsp;";

		$nomor_spd = $model->nomor_spd;
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $model->nomor_spd = "&nbsp;";
		
		
		$html .= '
			   
			 	<div style="text-align:center">
					<b><u>SURAT PERJALANAN DINAS</u></b><br>
					Nomor : '.$nomor_spd.'
				</div>
				<br>
				<table border="1" style="padding:3px">	
				<tr>
					<td width="6%" align="center">1</td>
					<td width="47%" >Pejabat berwenang yang memberi perintah</td>
					<td width="47%" colspan="2">'.$pejabat_berwenang.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">2</td>
					<td width="47%" >Nama/NIP pegawai yang diperintahkan</td>
					<td width="47%" colspan="2">'.$nama.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">3</td>
					<td width="47%" >a. Pangkat dan Golongan Ruang Gaji</td>
					<td width="47%" colspan="2">'.$pangkat.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Jabatan / Instansi</td>
					<td width="47%" colspan="2">'.$jabatan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >c. Tingkat biaya perjalanan dinas</td>
					<td width="47%" colspan="2">'.$biaya.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">4</td>
					<td width="47%" >Maksud perjalanan dinas</td>
					<td width="47%" colspan="2">'.$maksud.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">5</td>
					<td width="47%" >Alat angkutan yang dipergunakan</td>
					<td width="47%" colspan="2">'.$kendaraan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">6</td>
					<td width="47%" >a. Tempat berangkat</td>
					<td width="47%" colspan="2">'.$tempat_berangkat.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Tempat tujuan</td>
					<td width="47%" colspan="2">'.$tempat_tujuan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">7</td>
					<td width="47%" >a. Lamanya perjalanan dinas</td>
					<td width="47%" colspan="2">'.$lama.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Tanggal berangkat</td>
					<td width="47%" colspan="2">'.$tgl_pergi.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >c. Tanggal harus kembali / tiba di tempat baru*)</td>
					<td width="47%" colspan="2">'.$tgl_kembali.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">8</td>
					<td width="47%" ><b>Pengikut &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama:</b></td>
					<td width="22%"><b>Tgl. Lahir</b></td>
					<td width="25%"><b>Jabatan</b></td>
				</tr>';
	
		$namaPengikut = '';
		$tglLahirPengikut = '';
		$jabatanPengikut = '';
		
		$i = 1;
		
		foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$id,'aktif'=>1)) as $pengikut)
		{
			$nama = $pengikut->getRelationField("pegawai","nama");
			if(isset($_GET['blanko']) AND $_GET['blanko']==1) $nama = "&nbsp;";
			
			$tglLahir = date('d/m/Y', strtotime($pengikut->getRelationField("pegawai","tgl_lahir")));
			if(isset($_GET['blanko']) AND $_GET['blanko']==1) $tglLahir = "&nbsp;";
			
			$jabatan = $pengikut->getRelationField("pegawai","jabatan");
			if(isset($_GET['blanko']) AND $_GET['blanko']==1) $jabatan = "&nbsp;";
			
			
			$namaPengikut 		.= $nama.'<br>';
			$tglLahirPengikut 	.= $tglLahir.'<br>';
			$jabatanPengikut 	.= $jabatan.'<br>';
			$i++;
		}
		
		for($j=$i;$j<=6;$j++)
		{
			$namaPengikut .= '<br>&nbsp;';
			$tglLahirPengikut .= '<br>&nbsp;';
			$jabatanPengikut .= '<br>&nbsp;';
		}
		
		$html .='
				<tr>
					<td width="6%" align="center"></td>
					<td>'.$namaPengikut.'</td>
					<td>'.$tglLahirPengikut.'</td>
					<td>'.$jabatanPengikut.'</td>
				</tr>';
		
		$instansi = $model->instansi;
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $instansi = "&nbsp;";
		
		$koring = $model->koring;
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $koring = "&nbsp;";
		
		$html .='	
				<tr>
					<td width="6%" align="center">9</td>
					<td width="47%" >
						Pembebanan Anggaran<br>
						a. Instansi<br>
						b. Mata Anggaran
					</td>
					<td width="47%" >
						<br><br>
						'.$instansi.'<br>
						'.$koring.'
					</td>
				</tr>
				<tr>
					<td width="6%" align="center">10</td>
					<td width="47%" >Keterangan Lain-lain</td>
					<td width="47%" ></td>
				</tr>';
		$html .='</table>';
		
		$dikeluarkan = $model->tempat_berangkat;
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $dikeluarkan = "&nbsp;";
		
		$tgl_spd = $model->tanggal_spt; 
		if($model->tanggal_spd!=null) $tgl_spd = $model->tanggal_spd;
		$tgl_spd = Bantu::tanggal($tgl_spd);

		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $tgl_spd = "&nbsp;";
		
		$pejabat = $model->getRelationField("pejabat","jabatan");
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $pejabat = "&nbsp;";
		
		$namaPejabat = $model->getRelationField("pejabat","nama");
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $namaPejabat = "&nbsp;";
		
		$nip = $model->getRelationNip("pejabat");
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $nip = "&nbsp;";
	
		
		$html .='
				<br>
				<table>
					<tr>
						<td width="60%">&nbsp;</td>
						<td width="35%" style="text-align:center">
							Di keluarkan di : '."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$dikeluarkan.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
							Pada Tanggal : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$tgl_spd.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
							<br>'.$pejabat.'<br>
							<br><br><br><br>
							<b><u>'.$namaPejabat.'</u></b><br>
							'.'NIP. '.$nip.'
						</td>
					</tr>
				</table>';
		
		
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }
	
	public function actionCetakPdfSpdSetda($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A4', true, 'UTF-8');

		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Data Pegawai');
		$pdf->SetSubject('Data Pegawai');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		$model = $this->loadModel($id);
		
		$html=' <table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%">
							<div style="font-size:70px;line-height:101%;text-align:center">
							<b>PEMERINTAH KOTA SERANG</b><br>
							<b>SEKRETARIAT DAERAH</b><br>
							<i style="font-size:29px;">Jl. Jendral Sudirman Kawasan Perumahan Kota Serang Baru Telp.(0254)210538,FAX.(0254)202810</i></div>
						</td>
					</tr>
				</table>';
			
		
		$html .= '
			   
			 	<div style="text-align:center">
					<b><u>SURAT PERJALANAN DINAS</u></b><br>
					Nomor : '.$model->nomor_spd.'
				</div>
				<br>
				<table border="1" style="padding:3px">	
				<tr>
					<td width="6%" align="center">1</td>
					<td width="47%" >Pejabat berwenang yang memberi perintah</td>
					<td width="47%" colspan="2">'.$model->pejabat->jabatan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">2</td>
					<td width="47%" >Nama/NIP pegawain yang diperintahkan</td>
					<td width="47%" colspan="2"><b>'.$model->pegawai->nama.'</b><br>'.$model->pegawai->nip.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">3</td>
					<td width="47%" >a. Pangkat dan Golongan Ruang Gaji</td>
					<td width="47%" colspan="2">'.$model->pegawai->golongan->nama.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Jabatan / Instansi</td>
					<td width="47%" colspan="2">'.$model->pegawai->jabatan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >c. Tingkat biaya perjalanan dinas</td>
					<td width="47%" colspan="2">-</td>
				</tr>
				<tr>
					<td width="6%" align="center">4</td>
					<td width="47%" >Maksud perjalanan dinas</td>
					<td width="47%" colspan="2">'.$model->maksud.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">5</td>
					<td width="47%" >Alat angkutan yang dipergunakan</td>
					<td width="47%" colspan="2">'.$model->kendaraan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">6</td>
					<td width="47%" >a. Tempat berangkat</td>
					<td width="47%" colspan="2">'.$model->tempat_berangkat.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Tempat tujuan</td>
					<td width="47%" colspan="2">'.$model->tujuan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">7</td>
					<td width="47%" >a. Lamanya perjalanan dinas</td>
					<td width="47%" colspan="2">'.$model->lama.' Hari</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Tanggal berangkat</td>
					<td width="47%" colspan="2">'.Bantu::tanggal(($model->tgl_pergi)).'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >c. Tanggal harus kembali / tiba di tempat baru*)</td>
					<td width="47%" colspan="2">'.Bantu::tanggal(($model->tgl_kembali)).'</td>
				</tr>
				<tr>
					<td width="6%" align="center">8</td>
					<td width="47%" ><b>Pengikut &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama:</b></td>
					<td width="22%"><b>Tgl. Lahir</b></td>
					<td width="25%"><b>Jabatan</b></td>
				</tr>';
	
		$namaPengikut = '';
		$tglLahirPengikut = '';
		$jabatanPengikut = '';
		
		foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$id,'aktif'=>1)) as $pengikut)
		{
			$namaPengikut .= $pengikut->pegawai->nama.'<br>';
			$tglLahirPengikut .= date('d/m/Y', strtotime($pengikut->pegawai->tgl_lahir)).'<br>';
			$jabatanPengikut .= $pengikut->pegawai->jabatan.'<br>';
		}
		
		$html .='
				<tr>
					<td width="6%" align="center"></td>
					<td>'.$namaPengikut.'</td>
					<td>'.$tglLahirPengikut.'</td>
					<td>'.$jabatanPengikut.'</td>
				</tr>';
					
		$html .='	
				<tr>
					<td width="6%" align="center">9</td>
					<td width="47%" >
						Pembebanan Anggaran<br>
						a. Instansi<br>
						b. Mata Anggaran
					</td>
					<td width="47%" >
						<br><br>
						'.$model->instansi.'<br>
						'.$model->koring.'
					</td>
				</tr>
				<tr>
					<td width="6%" align="center">10</td>
					<td width="47%" >Keterangan Lain-lain</td>
					<td width="47%" ></td>
				</tr>';
		

		$html .='</table>';
		
	
		$html .='
				<div>&nbsp;</div>
				<table>
					<tr>
						<td width="70%">&nbsp;</td>
						<td width="30%">
							<div>Di keluarkan di : '.$model->tempat_berangkat.'<br>
							Pada Tanggal : '.Bantu::tanggal($model->tanggal_spd).'</div>
							<div style="text-align:center">'.$model->pejabat->jabatan.'</div>
							<div>&nbsp;</div>
							<div style="text-align:center" style="line-height:70%;"><span style="text-decoration:underline;"><b>'.$model->pejabat->nama.'</b></span></p>
							<div align="text-align:center" style="line-height:10%;">'.$model->pejabat->nip.'</p>
						</td>
					</tr>
				</table>';

		
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }
	
	public function actionCetakPdfSpdDpkd($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A4', true, 'UTF-8');

		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'Legal', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Data Pegawai');
		$pdf->SetSubject('Data Pegawai');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		$model = $this->loadModel($id);
		
		$html=' <table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%" >
							<div style="font-size:70px;line-height:93%;text-align:center" >
							<b>PEMERINTAH KOTA SERANG</b><br>
							<b style="font-size:50px;">DINAS PENGELOLAAN KEUANGAN DAERAH</b><br>
							( D P K D )<br>
							<i style="font-size:29px;">Jl. Jendral Sudirman Kawasan Perumahan Kota Serang Baru Telp.(0254)210538,FAX.(0254)202810</i></div>
						</td>
					</tr>
				</table>';
			
		
		$html .= '
			   
			 	<div style="text-align:center">
					<b><u>SURAT PERJALANAN DINAS</u></b><br>
					Nomor : '.$model->nomor.'
				</div>
				<br>
				<table border="1" style="padding:3px">	
				<tr>
					<td width="6%" align="center">1</td>
					<td width="47%" >Pejabat berwenang yang memberi perintah</td>
					<td width="47%" colspan="2">'.$model->pejabat->jabatan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">2</td>
					<td width="47%" >Nama/NIP pegawain yang diperintahkan</td>
					<td width="47%" colspan="2"><b>'.$model->pegawai->nama.'</b><br>'.$model->pegawai->nip.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">3</td>
					<td width="47%" >a. Pangkat dan Golongan Ruang Gaji</td>
					<td width="47%" colspan="2">'.$model->pegawai->golongan->nama.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Jabatan / Instansi</td>
					<td width="47%" colspan="2">'.$model->pegawai->jabatan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >c. Tingkat biaya perjalanan dinas</td>
					<td width="47%" colspan="2">-</td>
				</tr>
				<tr>
					<td width="6%" align="center">4</td>
					<td width="47%" >Maksud perjalanan dinas</td>
					<td width="47%" colspan="2">'.$model->maksud.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">5</td>
					<td width="47%" >Alat angkutan yang dipergunakan</td>
					<td width="47%" colspan="2">'.$model->kendaraan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">6</td>
					<td width="47%" >a. Tempat berangkat</td>
					<td width="47%" colspan="2">'.$model->tempat_berangkat.'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Tempat tujuan</td>
					<td width="47%" colspan="2">'.$model->tujuan.'</td>
				</tr>
				<tr>
					<td width="6%" align="center">7</td>
					<td width="47%" >a. Lamanya perjalanan dinas</td>
					<td width="47%" colspan="2">'.$model->lama.' Hari</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >b. Tanggal berangkat</td>
					<td width="47%" colspan="2">'.Bantu::tanggal(($model->tgl_pergi)).'</td>
				</tr>
				<tr>
					<td width="6%" align="center"></td>
					<td width="47%" >c. Tanggal harus kembali / tiba di tempat baru*)</td>
					<td width="47%" colspan="2">'.Bantu::tanggal(($model->tgl_kembali)).'</td>
				</tr>
				<tr>
					<td width="6%" align="center">8</td>
					<td width="47%" ><b>Pengikut &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama:</b></td>
					<td width="22%"><b>Tgl. Lahir</b></td>
					<td width="25%"><b>Jabatan</b></td>
				</tr>';
	
		$namaPengikut = '';
		$tglLahirPengikut = '';
		$jabatanPengikut = '';
		
		foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$id,'aktif'=>1)) as $pengikut)
		{
			$namaPengikut .= $pengikut->pegawai->nama.'<br>';
			$tglLahirPengikut .= date('d/m/Y', strtotime($pengikut->pegawai->tgl_lahir)).'<br>';
			$jabatanPengikut .= $pengikut->pegawai->jabatan.'<br>';
		}
		
		$html .='
				<tr>
					<td width="6%" align="center"></td>
					<td>'.$namaPengikut.'</td>
					<td>'.$tglLahirPengikut.'</td>
					<td>'.$jabatanPengikut.'</td>
				</tr>';
					
		$html .='	
				<tr>
					<td width="6%" align="center">9</td>
					<td width="47%" >
						Pembebanan Anggaran<br>
						a. Instansi<br>
						b. Mata Anggaran
					</td>
					<td width="47%" >
						<br><br>
						'.$model->instansi.'<br>
						'.$model->koring.'
					</td>
				</tr>
				<tr>
					<td width="6%" align="center">10</td>
					<td width="47%" >Keterangan Lain-lain</td>
					<td width="47%" ></td>
				</tr>';
		

		$html .='</table>';
		
	
		$html .='
				<div>&nbsp;</div>
				<table>
					<tr>
						<td width="70%">&nbsp;</td>
						<td width="30%">
							<div>Di keluarkan di : '.$model->tempat_berangkat.'<br>
							Pada Tanggal : '.Bantu::tanggal($model->tanggal).'</div>
							<div style="text-align:center">'.$model->pejabat->jabatan.'</div>
							<div>&nbsp;</div>
							<div style="text-align:center" style="line-height:70%;"><span style="text-decoration:underline;"><b>'.$model->pejabat->nama.'</b></span></p>
							<div align="text-align:center" style="line-height:10%;">'.$model->pejabat->nip.'</p>
						</td>
					</tr>
				</table>';

		
		
		$pdf->writeHTML($html, true, false, false, false, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='spd-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}

public function actionCetakPdfSpdMpdf($id)
{
		$pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');

		$model = $this->loadModel($id);
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);
		// set 	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		if(isset($_GET['kop']) AND $_GET['kop']=='setda')
		{
			$marginTop = 35;
		}	
		if(isset($_GET['kop']) AND $_GET['kop']=='dpkd')
		{
			$marginTop = 42;
		}				
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;

		$pdf = new mPDF('UTF-8','F4',9,'times',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		if(isset($_GET['kop']) AND $_GET['kop']=='setda')
		{
			$header = $this->renderPartial('//layouts/kop_setda',array(),true);
		}
		
		if(isset($_GET['kop']) AND $_GET['kop']=='dpkd')
		{
			$header = $this->renderPartial('//layouts/kop_dpkd',array(),true);
		}		

		$html = $this->renderPartial('//spd/cetak_pdf_spd',array('model'=>$model),true);

		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$pdf->Output();			
}

}
