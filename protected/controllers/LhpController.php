<?php

class LhpController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/admin/column2';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'accessRole'
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('input','update','cetak','delete','cetakPdf'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','excel','exportExcel'),
				'users'=>array('@'),
			),		
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionInput()
	{
		$model=new Lhp;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Lhp']))
		{
			$model->attributes=$_POST['Lhp'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('input'));
			}
		}

		$lhp=new Lhp('search');
		$lhp->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Lhp']))
			$lhp->attributes=$_GET['Lhp'];

		$this->render('input',array(
			'model'=>$model,
			'lhp'=>$lhp
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Lhp']))
		{
			$model->attributes=$_POST['Lhp'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('input'));
			}
		}

		$lhp=new Lhp('search');
		$lhp->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Lhp']))
			$lhp->attributes=$_GET['Lhp'];
		
		Yii::app()->user->setFlash('info','Silahkan sunting data pada kolom yang disediakan');
		
		$this->render('update',array(
			'model'=>$model,
			'lhp'=>$lhp,
		));
	}
	
	public function actionExcel()
	{
		$this->render('excel');
	}
	
	public function actionExportExcel()
	{
		$this->layout = '//layouts/excel';
		$this->render('_excel',array('border'=>1));
	}
	
	public function actionCetakPdf($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A4', true, 'UTF-8');
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'A3', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Data Pegawai');
		$pdf->SetSubject('Data Pegawai');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage('P', 'Legal');
		$pdf->SetFont('times', '', 12);
		
		$model = $this->loadModel($id);
		
		$html = '';
		
		if(isset($_GET['kop']) AND $_GET['kop']=='setda') 
		{		
			$html .= ' 
				<table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%">
							<p style="font-size:70px;line-height:100%;text-align:center">
							<b>PEMERINTAH KOTA SERANG</b><br>
							<b>SEKRETARIAT DAERAH</b><br>
							<i style="font-size:29px;">Jl. Jendral Sudirman Kawasan Perumahan Kota Serang Baru Telp.(0254)210538,FAX.(0254)202810</i></p>
						</td>
					</tr>
				</table>';
		}
		
		if(isset($_GET['kop']) AND $_GET['kop']=='dpkd') 
		{	
			$html=' <table style="border-bottom:1px;">
					<tr>
						<td width="15%">
							<img src="'.Yii::app()->request->baseUrl.'/img/logo.jpg" width="65px">
						</td>
						<td width="85%" >
							<p style="font-size:70px;line-height:93%;text-align:center" >
							<b>PEMERINTAH KOTA SERANG</b><br>
							<b style="font-size:50px;">DINAS PENGELOLAAN KEUANGAN DAERAH</b><br>
							( D P K D )<br>
							<i style="font-size:29px;">Jl. Jendral Sudirman Kawasan Perumahan Kota Serang Baru Telp.(0254)210538,FAX.(0254)202810</i></p>
						</td>
					</tr>
				</table>';
		}
		
		$html .= '
				<p style="text-align:center;font-weight:bold;font-size:42px;line-height:75%">N O T A &nbsp;&nbsp; D I N A S</p>
				
				<table>
					<tr>
						<td width="15%">
							Kepada
						</td>
						<td width="85%">
							: Yth. '.$model->kepada.'
						</td>
					</tr>
					<tr>
						<td width="15%">
							Dari
						</td>
						<td width="85%">
							: '.$model->dari.'
						</td>
					</tr>
					<tr>
						<td width="15%">
							Tanggal
						</td>
						<td width="85%">
							: '.Bantu::tanggal(date('Y-m-d')).'
						</td>
					</tr>
					<tr>
						<td width="15%">
							Hal
						</td>
						<td width="85%">
							: '.$model->hal.'
						</td>
					</tr>
					<tr>
						<td width="100%" style="border-bottom:0px solid #000">&nbsp;</td>
					</tr>
				</table>';
		
		$html .= '<p style="line-height:10%">&nbsp;</p>';
		
		$html .= '

				<table width="100%" celpadding="1px">
					<tr>
						<td width="3%"><b>1.</b></td>
						<td width="97%" colspan="2"><b>Dasar Hukum</b></td>
					</tr>
					<tr>
						<td width="3%">&nbsp;</td>
						<td width="30%">Surat Perintah</td>
						<td width="67%"> : '.$model->kepada.'</td>
					</tr>
					<tr>
						<td width="3%">&nbsp;</td>
						<td width="30%">Nomor</td>
						<td width="67%"> : '.$model->nomor.'</td>
					</tr>
					<tr>
						<td width="3%">&nbsp;</td>
						<td width="30%">Tanggal</td>
						<td width="57%"> : '.Bantu::tanggal(($model->tanggal)).'</td>
					</tr>
					<tr>
						<td width="3%">&nbsp;</td>
						<td width="30%">Memerintahkan kepada</td>
						<td width="67%"> : </td>
					</tr>
					<tr>
						<td width="3%">&nbsp;</td>
						<td width="40%">a) '.$model->getRelationRelationField("spd","pegawai","nama").'</td>
						<td width="57%">: '.$model->getRelationRelationField("spd","pegawai","jabatan").'</td>
					</tr>';
		$i = 98; 
		foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$model->id_spd,'aktif'=>1)) as $pengikut)
		{
			$html .= '
				
					<tr>
						<td width="3%" align="center">&nbsp;</td>
						<td width="40%" >'.chr($i).') '.$pengikut->getRelationField("pegawai","nama").'</td>
						<td width="57%" >: '.$pengikut->getRelationField("pegawai","jabatan").'</td>
					</tr>';
			$i++;
		}				
		$html .= '
					<tr>
						<td width="100%" colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td width="3%"><b>2.</b></td>
						<td width="97%" colspan="2"><b>Maksud dan Tujuan</b></td>
					</tr>
					<tr>
						<td width="3%">&nbsp;</td>
						<td width="97%" colspan="2">'.$model->getRelationField("spd","maksud").'</td>
					</tr>
					<tr>
						<td width="100%" colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td width="3%"><b>3.</b></td>
						<td width="97%" colspan="2"><b>Kesimpulan / Rangkuman isi hasil perjalanan dinas</b></td>
					</tr>
					<tr>
						<td width="3%">&nbsp;</td>
						<td width="97%" colspan="2">'.$model->kesimpulan.'</td>
					</tr>
					<tr>
						<td width="100%" colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%" colspan="3">Demikian disampaikan atas perhatiannya diucapkan terimakasih</td>
					</tr>
				</table>';

		$html .= '
				<table>
					<tr>
						<td width="50%">&nbsp;</td>
						<td width="50%" align="center">
							<p>'.$model->getRelationRelationField("spd","pegawai","jabatan").'</p>
							<div>&nbsp;</div>
							<p style="line-height:100%"><b><u>'.$model->getRelationRelationField("spd","pegawai","nama").'</u></b>
							<br>NIP. '.$model->getRelationRelationNip("spd","pegawai").'</p>
						</td>
					</tr>
				</table>';
		
		$html .= '<div>&nbsp;</div>';

		if(!isset($_GET['nonPengikut']))
		{
			$html .='	
				<table>
					<tr>
						<td style="text-align:center"><b>Unsur Pengikut:</b></td>
					</tr>';
		
			foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$model->id_spd,'aktif'=>1)) as $pengikut)
			{
				$html .='
					<tr>
						<td width="40%">&nbsp;<br>'.$pengikut->pegawai->nama.'</td>
						<td width="40%" style="border-bottom:0px solid #000">&nbsp;<br></td>
						<td width="20%">&nbsp;</td>
					</tr>';
			}
			
			$html .='</table>';
		
		}
		
		$pdf->writeHTML($html, true, true, true, true, '');

		//Close and output PDF document
		$pdf->Output('.pdf', 'I');	
 
    }

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Lhp');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
	public function actionCetak()
	{
		$model=new Lhp('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Lhp']))
			$model->attributes=$_GET['Lhp'];

		$this->render('cetak',array(
			'model'=>$model,
		));
	}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Lhp::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='lhp-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
