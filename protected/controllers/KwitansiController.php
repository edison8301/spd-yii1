<?php

class KwitansiController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/admin/column2';

/**
* @return array action filters
*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'accessRole'
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('input','update','cetak','cetakPdf','excel','exportExcel','cetakPdfMpdf'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionInput()
	{
		$model=new Kwitansi;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Kwitansi']))
		{
			$model->attributes=$_POST['Kwitansi'];
			
			$model->uang_harian = str_replace('.','',$model->uang_harian);
			$model->akomodasi = str_replace('.','',$model->akomodasi);
			$model->refresentatif = str_replace('.','',$model->refresentatif);
			$model->bbm = str_replace('.','',$model->bbm);
			$model->tol = str_replace('.','',$model->tol);
			$model->tiket = str_replace('.','',$model->tiket);
			
			$model->total = 0;
			$model->total += floatval($model->uang_harian);
			$model->total += floatval($model->akomodasi);
			$model->total += floatval($model->refresentatif);
			$model->total += floatval($model->bbm);
			$model->total += floatval($model->tol);
			$model->total += floatval($model->tiket);
			
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('input'));
			}
		}

		$kwitansi = new Kwitansi('search');
		$kwitansi->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Kwitansi']))
			$kwitansi->attributes=$_GET['Kwitansi'];

			
		$akomodasi = new Akomodasi;
		$refresentatif = new Refresentatif;
		$uangHarian = new UangHarian;
		
		$this->render('input',array(
			'model'=>$model,
			'kwitansi'=>$kwitansi,
			'akomodasi'=>$akomodasi,
			'refresentatif'=>$refresentatif,
			'uangHarian'=>$uangHarian
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Kwitansi']))
		{
			$model->attributes=$_POST['Kwitansi'];
			
			$model->uang_harian = str_replace('.','',$model->uang_harian);
			$model->akomodasi = str_replace('.','',$model->akomodasi);
			$model->refresentatif = str_replace('.','',$model->refresentatif);
			$model->bbm = str_replace('.','',$model->bbm);
			$model->tol = str_replace('.','',$model->tol);
			$model->tiket = str_replace('.','',$model->tiket);
			
			$model->total = 0;
			$model->total += floatval($model->uang_harian*$model->spd->lama);
			$model->total += floatval($model->akomodasi);
			$model->total += floatval($model->refresentatif);
			$model->total += floatval($model->bbm);
			$model->total += floatval($model->tol);
			$model->total += floatval($model->tiket);
			
			if($model->save())
			{	
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('input'));
			}
		}

		$kwitansi=new Kwitansi('search');
		$kwitansi->unsetAttributes();  // clear any default values
			
		if(isset($_GET['Kwitansi']))
			$kwitansi->attributes=$_GET['Kwitansi'];

		Yii::app()->user->setFlash('info','Silahkan sunting data pada kolom yang disediakan');
		
		$akomodasi = new Akomodasi;
		$refresentatif = new Refresentatif;
		$uangHarian = new UangHarian;
		
		$this->render('update',array(
			'model'=>$model,
			'kwitansi'=>$kwitansi,
			'akomodasi'=>$akomodasi,
			'refresentatif'=>$refresentatif,
			'uangHarian'=>$uangHarian
		));
	}
	
	public function actionExcel()
	{
		$this->render('excel');
	}
	
	public function actionExportExcel()
	{
		$this->layout = '//layouts/excel';
		$this->render('_excel',array('border'=>1));
	}
	
	public function actionCetakPdf($id)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A3', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'F4', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->SetTitle('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->SetSubject('RINCIAN BIAYA PERJALANAN DINAS');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage( 'P', 'F4');
		$pdf->SetFont('times', '', 8);
		
		
		$model = $this->loadModel($id);
		
		$html='
				<div style="line-height:100%;text-align:right"><i>Lampiran VII<br>Permenkeu RI. no.113 /PMK 05/2012</i></div>
				<div style="text-align:center;line-height:50%"><b><u>RINCIAN BIAYA PERJALANAN DINAS</u></b></div>';
		
		$html .= '<br>';
		
		$tanggal_spd = $model->getRelationField("spd","tanggal_spt");
		
		if($model->getRelationField("spd","tanggal_spd")!=null)
		{
			$tanggal_spd = $model->getRelationField("spd","tanggal_spd");
		}

		$html .= '
			    <table width="100%">
					<tr>
			    		<td width="25%">Lampiran SPPD Nomor</td>
			    		<td width="75%">: '.$model->getRelationField("spd","nomor_spd").'</td>
			    	</tr>
			    	<tr>
			    		<td width="25%">Tanggal</td>
			    		<td width="75%">: '.Bantu::tanggal($tanggal_spd).'</td>
			    	</tr>
			    </table>';
		
		$html .= '	    
			    <table border="0px;" cellpadding="1px">
			    	<tr>
			    		<td width="7%" style="text-align:center;font-weight:bold;border-top:0px solid #000;border-bottom:0px solid #000;border-left:0px solid #000">NO</td>
			    		<td width="36%" style="text-align:center;font-weight:bold;border-top:0px solid #000;border-bottom:0px solid #000;border-left:0px solid #000">Perincian Biaya</td>
			    		<td width="26%" style="text-align:center;font-weight:bold;border-top:0px solid #000;border-bottom:0px solid #000;border-left:0px solid #000">Jumlah</td>
			    		<td width="31%" style="text-align:center;font-weight:bold;border-top:0px solid #000;border-bottom:0px solid #000;border-left:0px solid #000;border-right:0px solid #000">Keterangan</td>
			    	</tr>
			    	';
		
		$i = 1;
		
		$total = 0;
		
		if(isset($_GET['dalamDaerah']) AND $_GET['dalamDaerah']==1)
		{
		
			$html .='
			    	<tr>
			    		<td width="7%" style="text-align:center;border-left:0px solid #000">
			    			'.$i.'
			    		</td>
			    		<td width="36%" style="border-left:0px solid #000">
			    			Uang Harian
			    		</td>
			    		<td width="26%" style="border-left:0px solid #000;text-align:right">
			    			Rp '.number_format($model->uang_harian*$model->getRelationField("spd","lama"), 2,",",".").'
			    		</td>
			    		<td width="31%" style="border-left:0px solid #000;border-right:0px solid #000">'.$model->getRelationField("spd","maksud").'</td>
			    	</tr>';
			$i++; $total = $total + $model->uang_harian*$model->getRelationField("spd","lama");
			
		}
		
		if(isset($_GET['luarDaerah']) AND $_GET['luarDaerah']==1)
		{
			$html .='
			    	<tr>
			    		<td width="7%" style="text-align:center;border-left:0px solid #000;line-height:75%">
			    			'.$i.'.
			    		</td>
			    		<td width="36%" style="border-left:0px solid #000;line-height:75%">
			    			Biaya Transport
			    		</td>
			    		<td width="26%" style="border-left:0px solid #000;text-align:right;line-height:75%">&nbsp;
			    			
			    		</td>
			    		<td width="31%" style="border-left:0px solid #000;border-right:0px solid #000;line-height:75%">&nbsp;'.$model->spd->maksud.'</td>
			    	</tr>';
					
			$i++;
			
			$html .='
			    	<tr>
			    		<td width="7%" style="text-align:center;border-left:0px solid #000;line-height:75%">&nbsp;
			    			
			    		</td>
			    		<td width="36%" style="border-left:0px solid #000;line-height:75%"> 
							&nbsp;&nbsp;&nbsp;BBM
			    		</td>
			    		<td width="26%" style="border-left:0px solid #000;text-align:right;line-height:75%">
			    			Rp '.number_format($model->bbm, 2,",",".").'
			    		</td>
			    		<td width="31%" style="border-left:0px solid #000;border-right:0px solid #000;line-height:75%">&nbsp;
							
						</td>
			    	</tr>';
			
			$total = $total + $model->bbm;
					
			$html .='
			    	<tr>
			    		<td width="7%" style="text-align:center;border-left:0px solid #000;line-height:75%">&nbsp;
			    			
			    		</td>
			    		<td width="36%" style="border-left:0px solid #000;line-height:75%">
			    			&nbsp;&nbsp;&nbsp;TOL
			    		</td>
			    		<td width="26%" style="border-left:0px solid #000;text-align:right;line-height:75%">
			    			Rp '.number_format($model->tol, 2,",",".").'
			    		</td>
			    		<td width="31%" style="border-left:0px solid #000;border-right:0px solid #000;line-height:75%">&nbsp;
							
						</td>
			    	</tr>';
					
			$total = $total + $model->tol;
					
			$html .='
			    	<tr>
			    		<td width="7%" style="text-align:center;border-left:0px solid #000;line-height:75%">&nbsp;
			    			
			    		</td>
			    		<td width="36%" style="border-left:0px solid #000;line-height:75%">
			    			&nbsp;&nbsp;&nbsp;Akomodasi
			    		</td>
			    		<td width="26%" style="border-left:0px solid #000;text-align:right;line-height:75%">
			    			Rp '.number_format($model->akomodasi, 2,",",".").'
			    		</td>
			    		<td width="31%" style="border-left:0px solid #000;border-right:0px solid #000;line-height:75%">&nbsp;
							
						</td>
			    	</tr>';
					
			$total = $total + $model->akomodasi;
					
			$html .='
			    	<tr>
			    		<td width="7%" style="text-align:center;border-left:0px solid #000;line-height:75%">&nbsp;
			    			
			    		</td>
			    		<td width="36%" style="border-left:0px solid #000;line-height:75%">
			    			&nbsp;&nbsp;&nbsp;Tiket Pesawat
			    		</td>
			    		<td width="26%" style="border-left:0px solid #000;text-align:right;line-height:75%">
			    			Rp '.number_format($model->tiket, 2,",",".").'
			    		</td>
			    		<td width="31%" style="border-left:0px solid #000;border-right:0px solid #000;line-height:75%">&nbsp;
							
						</td>
			    	</tr>';
			
			$total = $total + $model->tiket;
			
			$html .='
			    	<tr>
			    		<td width="7%" style="text-align:center;border-left:0px solid #000">
			    			'.$i.'.
			    		</td>
			    		<td width="36%" style="border-left:0px solid #000">
			    			Uang Harian ( '.$model->getRelationField("spd","lama").' x Rp '.number_format($model->uang_harian, 2,",",".").' )
			    		</td>
			    		<td width="26%" style="border-left:0px solid #000;text-align:right">
			    			Rp '.number_format($model->uang_harian*$model->getRelationField("spd","lama"), 2,",",".").'
			    		</td>
			    		<td width="31%" style="border-left:0px solid #000;border-right:0px solid #000">&nbsp;
			    			
			    		</td>
			    	</tr>';
					
			$i++; $total = $total + $model->uang_harian*$model->getRelationField("spd","lama");
			
			
		
		}
		
		if(isset($_GET['refresentatif']) AND $_GET['refresentatif']==1)
		{
				$html .='
			    	<tr>
			    		<td width="7%" style="text-align:center;border-left:0px solid #000">
			    			'.$i.'.
			    		</td>
			    		<td width="36%" style="border-left:0px solid #000">
			    			Refresentatif
			    		</td>
			    		<td width="26%" style="border-left:0px solid #000;text-align:right">
			    			Rp '.number_format($model->refresentatif, 2,",",".").'
			    		</td>
			    		<td width="31%" style="border-left:0px solid #000;border-right:0px solid #000">&nbsp;
							
						</td>
			    	</tr>';
				
				$total = $total + $model->refresentatif;
				
		}
		
		$html .='
			    	<tr style="line-height:10%;">
			    		<td width="7%" style="text-align:center;border-left:0px solid #000">&nbsp;
			    			
			    		</td>
			    		<td width="36%" style="border-left:0px solid #000">&nbsp;
			    			
			    		</td>
			    		<td width="26%" style="border-left:0px solid #000">&nbsp;
			    			
			    		</td>
			    		<td width="31%" style="border-left:0px solid #000;border-right:0px solid #000">&nbsp;
							
						</td>
			    	</tr>';
		
		$html .='
			    	<tr>
			    		<td width="7%" style="text-align:center;border-left:0px solid #000;border-bottom:0px solid #000">&nbsp;
			    			
			    		</td>
			    		<td width="36%" style="border-left:0px solid #000;border-bottom:0px solid #000">
			    			<b>Jumlah</b>
			    		</td>
			    		<td width="26%" style="border-left:0px solid #000;border-bottom:0px solid #000;text-align:right">
			    			<b>Rp '.number_format($total, 2,",",".").'</b>
			    		</td>
			    		<td width="31%" style="border-left:0px solid #000;border-right:0px solid #000;border-bottom:0px solid #000">&nbsp;
							
						</td>
			    	</tr>';
					
		$html .= '
					<tr>
						<td colspan="4" style="border:0px solid #000;">&nbsp;&nbsp;&nbsp;<i>Terbilang: '.Bantu::getTerbilang($total,0).' rupiah</i></td>
					</tr>';
		
		$html .='</table>';
		
		$pejabat = "Pejabat Pelaksana Teknis Kegiatan";
		if(isset($_GET['nonPptk']) AND $_GET['nonPptk']==1) $pejabat = "&nbsp;";
		
		$namaPptk = $model->getRelationField("pptkRelation","nama");
		if(isset($_GET['nonPptk']) AND $_GET['nonPptk']==1) $namaPptk = "&nbsp;";
		
		$nip = $model->getRelationField("pptkRelation","nip");
		if(isset($_GET['nonPptk']) AND $_GET['nonPptk']==1) $nip = "&nbsp;";
		
		$html .='
				<table>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td style="text-align:center">Serang, '.Bantu::tanggal($model->getRelationField("spd","tgl_pergi")).'</td>
					</tr>
					<tr>
			    		<td style="text-align:center">
			    			Telah Dibayar Sejumlah<br>
							<b>Rp '.number_format($total, 2,",",".").'</b>
						</td>
						<td>&nbsp;
							
						</td>
						<td style="text-align:center">
							Telah menerima jml uang sebesar<br>
							<b>Rp '.number_format($total, 2,",",".").'</b>
						</td>
					</tr>
					<tr>
						<td style="text-align:center">Bendahara Pengeluaran</td>
						<td style="text-align:center">'.$pejabat.'</td>
						<td style="text-align:center">Yang Menerima</td>
					</tr>
					<tr>
						<td>&nbsp;<br>&nbsp;</td>
						<td>&nbsp;<br>&nbsp;</td>
						<td>&nbsp;<br>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;<br>&nbsp;</td>
						<td>&nbsp;<br>&nbsp;</td>
						<td>&nbsp;<br>&nbsp;</td>
					</tr>
					<tr>
						<td style="text-align:center"><b><u>'.$model->getRelationField("bendaharaRelation","nama").'</u></b><br>NIP. '.$model->getRelationField("bendaharaRelation","nip").'</td>
						<td style="text-align:center">'.'<b><u>'.$namaPptk.'</u></b><br>NIP. '.$nip.'</td>
						<td style="text-align:center"><b><u>'.$model->getRelationField("penerimaRelation","nama").'</u></b><br>NIP. '.$model->getRelationField("penerimaRelation","nip").'</td>
			    	</tr>
				</table>';
	
		$html .='
				<table>
					<tr>
						<td style="border-bottom:0px solid #000" width="100%">&nbsp;</td>
					</tr>
				</table>';
		$html .= '<div style="line-height:70%;text-align:center"><b>PERHITUNGAN SPDD RAMPUNG</b></div>';
		$html .= '<br>';

		$html .='
				<table width="70%">
			    	<tr>
			    			<td width="40%">
			    				Ditetapkan sejumlah
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp '.number_format($total, 2,",",".").'</b>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td width="40%">
			    				Yang telah dibayarkan
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp '.number_format($total, 2,",",".").'</b>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td width="40%">
			    				Sisa kurang/lebih
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp 0</b>
			    			</td>
			    		</tr>
			    	</table>';
			
		$html .='
			    <table>
					<tr>
						<td width="60%">&nbsp;</td>
						<td width="35%" align="center">
							<p>Pejabat yang berwenang/<br>pejabat lain yang ditunjuk</p>
							<p>&nbsp;</p>
							<p><b><u>'.$model->getRelationField("paRelation","nama").'</u></b><br>
							NIP. '.$model->getRelationNip("paRelation").'</p>
						</td>
					</tr>
				</table>';
				
		$garis ='
				<table>
					<tr style="line-height:0%">
						<td width="100%" style="border-bottom:0px dashed #000">&nbsp;</td>
					</tr>
				</table>'; 
		
		//$html .= $html;
			   
		$pdf->writeHTML($html, true, false, false, false, '');
		$pdf->writeHTML($garis, true, false, false, false, '');
		$pdf->writeHTML($html, true, false, false, false, '');
		//Close and output PDF document
		//$pdf->copyPage(1);
		$pdf->Output('.pdf', 'I');	
 
    }
	
	

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Kwitansi');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

	/**
	* Manages all models.
	*/
	public function actionCetak()
	{
		$model=new Kwitansi('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Kwitansi']))
			$model->attributes=$_GET['Kwitansi'];

		$this->render('cetak',array(
			'model'=>$model,
		));
	}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Kwitansi::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='kwitansi-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}

	public function actionCetakPdfMpdf($id)
    {
		$pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');

		$model = $this->loadModel($id);
		
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'F4', true, 'UTF-8', false);
		// set 	{
		
		include(Yii::app()->basePath."/vendors/mpdf/mpdf.php");

		$marginLeft = 10;
		$marginRight = 10;
		$marginTop = 10;			
		$marginBottom = 20;
		$marginHeader = 5;
		$marginFooter = 5;	

		$pdf = new mPDF('UTF-8','F4',9,'times',$marginLeft,$marginRight,$marginTop,$marginBottom,$marginHeader,$marginFooter);

		$html = $this->renderPartial('cetak_pdf',array('model'=>$model),true);

		$pdf->SetHTMLHeader($header,'',true);
		$pdf->SetHTMLFooter($footer,'',true);

		$pdf->WriteHTML($html);

		$pdf->Output();	
 
    }

}
