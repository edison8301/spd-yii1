<?php
$this->breadcrumbs=array(
	'Akomodasis'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Akomodasi','url'=>array('index')),
array('label'=>'Create Akomodasi','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('akomodasi-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Akomodasi</h1>

<?php
            $this->widget('bootstrap.widgets.TbButtonGroup', array(
            //'size'=>'small',
            'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'buttons'=>array(
            array('label'=>'Menu', 'items'=>array(
					array('label'=>'Tambah Akomodasi', 'icon'=>'plus', 'url'=>array('akomodasi/create')),
                )),
            ),
            ));
?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'akomodasi-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'jarak',
		array(
			'class'=>'CDataColumn',
			'name'=>'id_golongan',
			'header'=>'Golongan',
			'type'=>'raw',
			'value'=>'$data->golongan->nama',
			'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
		),
		'biaya',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
