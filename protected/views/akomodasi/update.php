<?php
$this->breadcrumbs=array(
	'Akomodasis'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Akomodasi','url'=>array('index')),
	array('label'=>'Create Akomodasi','url'=>array('create')),
	array('label'=>'View Akomodasi','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Akomodasi','url'=>array('admin')),
	);
	?>

<h1>Update Akomodasi</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'akomodasi-grid',
'type'=>'striped bordered',
'dataProvider'=>$akomodasi->search(),
'filter'=>$akomodasi,
'columns'=>array(
		'jarak',
		array(
			'class'=>'CDataColumn',
			'name'=>'id_golongan',
			'header'=>'Golongan',
			'type'=>'raw',
			'value'=>'$data->golongan->nama',
			'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
		),
		'biaya',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
'template'=>'{update} {delete}'
),
),
)); ?>
