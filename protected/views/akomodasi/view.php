<?php
$this->breadcrumbs=array(
	'Akomodasis'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Akomodasi','url'=>array('index')),
array('label'=>'Create Akomodasi','url'=>array('create')),
array('label'=>'Update Akomodasi','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Akomodasi','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Akomodasi','url'=>array('admin')),
);
?>

<h1>Detail Akomodasi</h1>

<?php
            $this->widget('bootstrap.widgets.TbButtonGroup', array(
            //'size'=>'small',
            'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'buttons'=>array(
            array('label'=>'Menu', 'items'=>array(
					array('label'=>'Tambah Akomodasi', 'icon'=>'plus', 'url'=>array('akomodasi/create')),
                    array('label'=>'Sunting Akomodasi', 'icon'=>'plus', 'url'=>array('akomodasi/update','id'=>$model->id)),
                )),
            ),
            ));
?>
<div> &nbsp; </div>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'jarak',
		'id_golongan',
		'biaya',
),
)); ?>
