<?php
$this->breadcrumbs=array(
	'Akomodasis'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Akomodasi','url'=>array('index')),
array('label'=>'Manage Akomodasi','url'=>array('admin')),
);
?>

<h1>Entry Akomodasi</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'akomodasi-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'jarak',
		array(
			'class'=>'CDataColumn',
			'name'=>'id_golongan',
			'header'=>'Golongan',
			'type'=>'raw',
			'value'=>'$data->golongan->nama',
			'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
		),
		array(
			'class'=>'CDataColumn',
			'name'=>'biaya',
			'header'=>'Biaya',
			'type'=>'raw',
			'value'=>'number_format($data->biaya, 0,",",".")',
		),
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
'template'=>'{update} {delete}'
),
),
)); ?>
