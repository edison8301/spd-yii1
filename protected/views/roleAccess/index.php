<?php
$this->breadcrumbs=array(
	'Role Accesses',
);

$this->menu=array(
array('label'=>'Create RoleAccess','url'=>array('create')),
array('label'=>'Manage RoleAccess','url'=>array('admin')),
);
?>

<h1>Role Accesses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
