<?php
$this->breadcrumbs=array(
	'Role Accesses'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List RoleAccess','url'=>array('index')),
array('label'=>'Create RoleAccess','url'=>array('create')),
array('label'=>'Update RoleAccess','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete RoleAccess','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage RoleAccess','url'=>array('admin')),
);
?>

<h1>View RoleAccess #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'role_id',
		'access_id',
		'status',
),
)); ?>
