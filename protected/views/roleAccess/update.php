<?php
$this->breadcrumbs=array(
	'Role Accesses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List RoleAccess','url'=>array('index')),
	array('label'=>'Create RoleAccess','url'=>array('create')),
	array('label'=>'View RoleAccess','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage RoleAccess','url'=>array('admin')),
	);
	?>

	<h1>Update RoleAccess <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>