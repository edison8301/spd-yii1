<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'rill-form',
	'enableAjaxValidation'=>false,
)); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$(".format-money").on("keyup", function(){
	    var _this = $(this);
	    var value = _this.val().replace(/\.| /g,"");
	    _this.val(accounting.formatMoney(value, "", 0, ".", ","))
	})
});
</script>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php print $form->hiddenField($model,'id_spd'); ?>
	<?php print CHtml::label('Nomor SPD',''); ?>
	<?php print CHtml::textField('nomor_spd',$model->getRelationField("spd","nomor_spd") != '' ? $model->getRelationField("spd","nomor_spd") : '',array('readonly'=>'readonly','class'=>'span2','placeholder'=>'Pilih SPD')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'[..]',
            'type'=>'danger', 
            'htmlOptions'=>array(
				'onclick'=>'$("#dialogSpd").dialog("open"); return false;',
				'style'=>'margin-bottom:10px',
            ),
    )); ?>
	
	<?php echo $form->textFieldRow($model,'bbm',array('class'=>'span10 format-money','maxlength'=>255,'prepend'=>'Rp')); ?>

	<?php echo $form->textFieldRow($model,'tol',array('class'=>'span10 format-money','maxlength'=>255,'prepend'=>'Rp')); ?>

	<?php echo $form->textFieldRow($model,'jumlah',array('class'=>'span10 format-money','maxlength'=>255,'prepend'=>'Rp')); ?>

	<div class="control-group ">
		<label class="control-label" for="Rill_tanggal">
			Tanggal
		</label>
		<div class="controls">
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'name' => 'Riil[tanggal]',
				'id'=>'Riil_tanggal',
				'language' => 'id',
				'model' => $model,
				'value'=>$model->tanggal,
				// additional javascript options for the date picker plugin
				'options'=>array(
				    'showAnim'=>'fold',
				    'showOn'=>'button',
				    'buttonImage'=>Yii::app()->baseUrl.'/img/calendar.png',
				    'dateFormat'=>'yy-mm-dd',
				    'changeMonth' => 'false',
				    'showButtonPanel' => 'false',
				    'changeYear'=>'false',
				    'constrainInput' => 'false',
				),
				'htmlOptions'=>array(
				    'style'=>'height:20px;width:150px; margin-bottom:0px',
				    'readonly'=>true,
				),
			));?>
		</div>
	</div>

	<?php print $form->hiddenField($model,'ppk'); ?>
	<?php print CHtml::label('Pilih PPK',''); ?>
	<?php print CHtml::textField('nama_ppk',$model->getRelationField("ppkRelation","nama") != '' ? $model->getRelationField("ppkRelation","nama") : '',array('readonly'=>'readonly','class'=>'span2','placeholder'=>'Pilih Pegawai')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'[..]',
            'type'=>'danger', 
            'htmlOptions'=>array(
				'onclick'=>'$("#dialogPpk").dialog("open"); return false;',
				'style'=>'margin-bottom:10px',
            ),
    )); ?>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>

<?php //Dialog SPD
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogSpd',
    'options'=>array(
        'title'=>'Pilih SPD',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	$spd = new Spd;
	$this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'spd-grid',
		'type'=> 'striped bordered',
		'dataProvider'=>$spd->dataSpd(),
		'filter'=>$spd,
		'columns'=>array(
			array(
				'class'=>'CDataColumn',
				'type'=>'raw',
				'header'=>'Cetak',
				'value'=>'CHtml::link("<center><i class=icon-print ></i></center>",array("spd/viewSpd","id"=>"$data->id"))',
			),
			'nomor_spd',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_spd',
				'header'=>'Tanggal SPD',
				'type'=>'raw',
				'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tanggal_spd)',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pejabat',
				'header'=>'Penandatangan',
				'type'=>'raw',
				'value'=>'$data->getRelationField("pejabat","nama")',
				'filter'=>CHtml::listData(Pejabat::model()->findAll(),'id','nama')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pegawai',
				'header'=>'Pegawai',
				'type'=>'raw',
				'value'=>'$data->getRelationField("pegawai","nama")',
				'filter'=>CHtml::listData(Pegawai::model()->findAll(),'id','nama')
			),
			'maksud',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#dialogSpd\").dialog(\"close\");
								$(\"#Riil_id_spd\").val(\"$data->id\");
							    $(\"#nomor_spd\").val(\"$data->nomor_spd\");
								
				"))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
		),
));

	
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer
?>

<?php //Dialog PPK
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogPpk',
    'options'=>array(
        'title'=>'Pilih PPK',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	$ppk = new Pejabat;
	$this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'ppk-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$ppk->search(),
		'filter'=>$ppk,
		'columns'=>array(
			'nip',
			'nama',
			'jabatan',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#dialogPpk\").dialog(\"close\");
							    $(\"#Riil_ppk\").val(\"$data->id\");
								$(\"#nama_ppk\").val(\"$data->nama\");",
					"class"=>"btn-primary"
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
					
				),
			),
		),
));
	
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer
?>