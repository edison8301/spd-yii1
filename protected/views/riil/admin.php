<?php
$this->breadcrumbs=array(
	'Rills'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Rill','url'=>array('index')),
array('label'=>'Create Rill','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('rill-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Rills</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'rill-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		array(
			'class'=>'CDataColumn',
			'name'=>'id_spd',
			'header'=>'No. SPD',
			'type'=>'raw',
			'value'=>'$data->spd->nomor',
			'filter'=>CHtml::listData(Spd::model()->findAll(),'id','nomor')
		),
		'bbm',
		'tol',
		'jumlah',
		'tanggal',
		/*
		'ppk',
		*/
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
