<?php
$this->breadcrumbs=array(
	'Rills'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Rill','url'=>array('index')),
	array('label'=>'Create Rill','url'=>array('create')),
	array('label'=>'View Rill','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Rill','url'=>array('admin')),
	);
	?>

<h1>Input Rill</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'rill-grid',
'type'=> 'striped bordered',
'dataProvider'=>$rill->search(),
'filter'=>$rill,
'columns'=>array(
		array(
			'class'=>'CDataColumn',
			'name'=>'id_spd',
			'header'=>'No. SPD',
			'type'=>'raw',
			'value'=>'$data->spd->nomor_spd',
			'filter'=>CHtml::listData(Spd::model()->findAll(),'id','nomor_spd')
		),
		'bbm',
		'tol',
		'jumlah',
		array(
			'class'=>'CDataColumn',
			'name'=>'tanggal',
			'header'=>'Tanggal',
			'type'=>'raw',
			'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tanggal)',
		),
		/*
		'ppk',
		*/
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>