<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<div class="dialog">
	<div class="block">
		<p class="block-heading">Login</p>
		<div class="block-body">
			<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
				'id'=>'login-form',
				'enableClientValidation'=>true,
				'clientOptions'=>array(
					'validateOnSubmit'=>true,
				),
			)); ?>
			<center>
				<img src="<?php echo Yii::app()->request->baseUrl; ?>/img/logo.jpg" width="35%">
			</center>
			
			
			<?php echo $form->labelEx($model,'username'); ?>
			<?php echo $form->textField($model,'username',array('class'=>'span12')); ?>
			<?php echo $form->error($model,'username'); ?>

			<?php echo $form->labelEx($model,'password'); ?>
			<?php echo $form->passwordField($model,'password',array('class'=>'span12')); ?>
			<?php echo $form->error($model,'password'); ?>
			<Br>
			<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>'Login','type'=>'primary','icon'=>'lock white')); ?>
		
		</div>
			
		</div>
<?php $this->endWidget(); ?>
</div><!-- form -->
