<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<?php
	$this->widget(
		'bootstrap.widgets.TbButton',
		array(
			'label' => 'Blanko Depan Kop SETDA',
			'icon'=>'download-alt white',
			'type' => 'primary',
			'url'=>array('export/BlankoSpdSetda'),
			'htmlOptions'=>array('target'=>'_blank'),
		)
	);
	echo "&nbsp;";
	$this->widget(
		'bootstrap.widgets.TbButton',
		array(
			'label' => 'Blanko Depan Kop DPKD',
			'icon'=>'download-alt white',
			'type' => 'primary',
			'url'=>array('export/BlankoSpdDpkd'),
			'htmlOptions'=>array('target'=>'_blank'),
		)
	);
	echo "&nbsp;";
	$this->widget(
		'bootstrap.widgets.TbButton',
		array(
			'label' => 'Blanko Belakang SPD',
			'icon'=>'download-alt white',
			'type' => 'primary',
			'url'=>array('export/BlankoSpd'),
			'htmlOptions'=>array('target'=>'_blank'),
		)
	);
?>


<?php /*
<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<div class="block span3" style="background: #FFF; margin-top:0px">
	<a class="block-heading" data-toggle="collapse" href="#tablewidget">
		<i class="icon-plus"></i>
		Input Data SPD
		</a>
	<div id="summaryData" class="block-body collapse in">
		<table class="items table table-striped table-condensed">
			<tbody>
				<tr>
					<td>
						<center>
						<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=spd/adminspd">
							<img src="<?php echo Yii::app()->request->baseUrl; ?>/img/document-icon.png" alt="document" width="100px" style="margin:10px;" />
						</a>
						</center>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="block span3" style="background: #FFF; margin-top:0px">
	<a class="block-heading" data-toggle="collapse" href="#tablewidget">
		<i class="icon-user"></i>
		Input Pejabat
		</a>
	<div id="summaryData" class="block-body collapse in">
		<table class="items table table-striped table-condensed">
			<tbody>
				<tr>
					<td>
						<center>
						<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=pejabat/create">
							<img src="<?php echo Yii::app()->request->baseUrl; ?>/img/user-icon.png" alt="document" width="100px" style="margin:10px;" />
						</a>
						</center>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="block span3" style="background: #FFF; margin-top:0px">
	<a class="block-heading" data-toggle="collapse" href="#tablewidget">
		<i class="icon-th-large"></i>
		Input Pegawai
		</a>
	<div id="summaryData" class="block-body collapse in">
		<table class="items table table-striped table-condensed">
			<tbody>
				<tr>
					<td>
						<center>
						<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=pegawai/create">
							<img src="<?php echo Yii::app()->request->baseUrl; ?>/img/group-icon.png" alt="document" width="100px" style="margin:10px;" />
						</a>
						</center>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
*/ ?>