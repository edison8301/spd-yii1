<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'pegawai-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nip',array('class'=>'span5','maxlength'=>255,'placeholder'=>'NIP pegawai 18 karakter','value'=>$model->nip == '' ? '' : $model->getNip())); ?>

	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>255,'placeholder'=>'Nama pegawai')); ?>
	
	<div class="control-group ">
		<label class="control-label required" for="Produk_kategori_produk_id">
			Tgl Lahir<span class="required">*</span>
		</label>
		<div class="controls">
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'name' => 'Pegawai[tgl_lahir]',
				'id'=>'Pegawai_tgl_lahir',
				'language' => 'id',
				'model' => $model,
				'value'=>$model->tgl_lahir,
				// additional javascript options for the date picker plugin
				'options'=>array(
				    'showAnim'=>'fold',
				    'showOn'=>'button',
				    'buttonImage'=>Yii::app()->baseUrl.'/img/calendar.png',
				    'dateFormat'=>'yy-mm-dd',
				    'changeMonth' => 'false',
				    'showButtonPanel' => 'false',
				    'changeYear'=>'false',
				    'constrainInput' => 'false',
				    'yearRange' => '1930:2015'
				),
				'htmlOptions'=>array(
				    'style'=>'height:20px;width:150px; margin-bottom:0px',
				    'readonly'=>true,
				),
			));?>
		</div>
	</div>
	
	<?php echo $form->dropDownListRow($model,'id_golongan',CHtml::listData(Golongan::model()->findAll(),'id','nama')); ?>

	<?php echo $form->textFieldRow($model,'jabatan',array('class'=>'span5','maxlength'=>255,'placeholder'=>'Jabatan pegawai')); ?>
	
	<?php echo $form->passwordFieldRow($model,'password',array('class'=>'span5','maxlength'=>255,'placeholder'=>'Password untuk pegawai login')); ?>

	<?php echo $form->labelEx($model,'foto'); ?>
	
	<?php 
		if($model->foto != '')
		{
			print CHtml::image(Yii::app()->request->baseUrl.'/uploads/pegawai/'.$model->foto,'',array('style'=>'width:150px;margin-right:10px;')); 
			$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'link',
				'type'=>'danger',
				'size'=>'mini',
				'icon'=>'remove white',
				'label'=>'',
				'url'=>array('/pegawai/hapusFoto','id'=>$model->id)
			));
			
		} else {
			print CHtml::image(Yii::app()->request->baseUrl.'/img/no-profile.jpg','',array('style'=>'width:150px'));
		}
	?>		

	<?php echo $form->fileField($model,'foto'); ?>
	
	<?php echo $form->error($model,'foto'); ?>
	
	<?php //echo $form->textFieldRow($model,'tgl_lahir',array('class'=>'span5','maxlength'=>255)); ?>

	<?php //echo $form->datepickerRow($model,'tgl_lahir',array('prepend'=>'<i class="icon-calendar"></i>','options'=>array('autoclose'=>true,'format' => 'yyyy-mm-dd'))); ?>

	


<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
