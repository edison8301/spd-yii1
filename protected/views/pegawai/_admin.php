<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'pegawai-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$pegawai->search(),
		'filter'=>$pegawai,
		'columns'=>array(
			
			array(
				'class'=>'CDataColumn',
				'name'=>'foto',
				'header'=>'Foto',
				'type'=>'raw',
				'value'=>'$data->getFoto()',
				'headerHtmlOptions'=>array('style'=>'width:10%')
			),
			
			array(
				'class'=>'CDataColumn',
				'name'=>'nip',
				'header'=>'NIP',
				'type'=>'raw',
				'value'=>'$data->getNip()',
			),
			
			'nama',
			
			array(
				'class'=>'CDataColumn',
				'name'=>'id_golongan',
				'header'=>'Golongan',
				'type'=>'raw',
				'value'=>'$data->getRelationField("golongan","nama")',
				'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
			),
			'jabatan',
			array(
				'class'=>'CDataColumn',
				'name'=>'tgl_lahir',
				'header'=>'Tanggal Lahir',
				'type'=>'raw',
				'value'=>'$data->getTglLahir()',
			),
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}{delete}'
			),
		),
)); ?>