<?php
$this->breadcrumbs=array(
	'Pegawai'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Pegawai','url'=>array('index')),
array('label'=>'Create Pegawai','url'=>array('create')),
array('label'=>'Update Pegawai','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Pegawai','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Pegawai','url'=>array('admin')),
);
?>

<h1>Detail Pegawai <?php echo $model->nama; ?></h1>

<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','type'=>'primary','icon'=>'pencil white','label'=>'Sunting Pegawai','url'=>array('pegawai/update','id'=>$model->id))); ?>&nbsp;
<?php //$this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','type'=>'primary','icon'=>'download-alt white','label'=>'Export Pegawai','url'=>array('pegawai/exportDataPegawai','id'=>$model->id))); ?>&nbsp;
<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','type'=>'primary','icon'=>'list white','label'=>'Master Pegawai','url'=>array('pegawai/admin'))); ?>


<div>&nbsp;</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'nip',
		'nama',
		array(
			'label'=>'Golongan',
			'value'=>$model->golongan->nama
		),
		'jabatan',
		array(
			'label'=>'Tanggal Lahir',
			'value'=>Yii::app()->dateFormatter->format("dd-MM-yyyy",$model->tgl_lahir)
		),
),
)); 

?>
