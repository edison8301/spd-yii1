<?php
$this->breadcrumbs=array(
	'Pegawai'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Pegawai','url'=>array('index')),
array('label'=>'Manage Pegawai','url'=>array('admin')),
);
?>


<h1>Entry Pegawai</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<h2>Data Pegawai</h2>

<?php print $this->renderPartial('_admin',array('pegawai'=>$pegawai)); ?>

