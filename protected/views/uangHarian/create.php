<?php
$this->breadcrumbs=array(
	'Sshes'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Ssh','url'=>array('index')),
array('label'=>'Manage Ssh','url'=>array('admin')),
);
?>

<h1>Entry Uang Harian</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<?php print $this->renderPartial('_admin',array('uangHarian'=>$uangHarian)); ?>
