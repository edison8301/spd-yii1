<?php
$this->breadcrumbs=array(
	'Sshes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Ssh','url'=>array('index')),
	array('label'=>'Create Ssh','url'=>array('create')),
	array('label'=>'View Ssh','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Ssh','url'=>array('admin')),
	);
	?>

<h1>Update Uang Harian</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<?php print $this->renderPartial('_admin',array('uangHarian'=>$uangHarian)); ?>

