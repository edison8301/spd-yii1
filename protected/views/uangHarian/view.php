<?php
$this->breadcrumbs=array(
	'Sshes'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Ssh','url'=>array('index')),
array('label'=>'Create Ssh','url'=>array('create')),
array('label'=>'Update Ssh','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Ssh','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Ssh','url'=>array('admin')),
);
?>

<h1>Detail SSH</h1>

<?php
            $this->widget('bootstrap.widgets.TbButtonGroup', array(
            //'size'=>'small',
            'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'buttons'=>array(
            array('label'=>'Menu', 'items'=>array(
					array('label'=>'Tambah SSH', 'icon'=>'plus', 'url'=>array('ssh/create')),
                    array('label'=>'Sunting SSH', 'icon'=>'plus', 'url'=>array('ssh/update','id'=>$model->id)),
                )),
            ),
            ));
?>
<div> &nbsp; </div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'jarak',
		array(
			'label'=>'Golongan',
			'value'=>$model->golongan->nama
		),
		'biaya',
),
)); ?>
