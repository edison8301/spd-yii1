<?php
$this->breadcrumbs=array(
	'Sshes',
);

$this->menu=array(
array('label'=>'Create Ssh','url'=>array('create')),
array('label'=>'Manage Ssh','url'=>array('admin')),
);
?>

<h1>Sshes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
