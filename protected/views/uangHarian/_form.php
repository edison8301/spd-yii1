<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'ssh-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'jarak',array('class'=>'span4','maxlength'=>255)); ?>

	<?php echo $form->dropDownListRow($model,'id_golongan',CHtml::listData(Golongan::model()->findAll(),'id','nama')); ?>

	<?php echo $form->textFieldRow($model,'biaya',array('class'=>'span3','maxlength'=>255)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
