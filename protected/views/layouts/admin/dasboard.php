<?php $this->beginContent('//layouts/admin/main'); ?>
    
<?php
Yii::app()->clientScript->registerScript(
   'myHideEffect',
   '$(".alert").animate({opacity: 1.0}, 3000).fadeOut("slow");',
   CClientScript::POS_READY
);
?>
    <div class="content">

    	 <div id="tabContainer">
	    <div id="tabs">
	      <ul>
	        <li id="tabHeader_1" 
	        <?php 
				$path = Yii::app()->controller->id."/".Yii::app()->controller->action->id;
				if($path == "site/index" || $path == "site/admin" || $path == "site/excel" || $path == "perjalanan/index")
					{print "class='tabActiveHeader'";}
			?>>
	        	<i class="icon-folder-open icon-white"></i> Berkas
	        </li>

	        <li id="tabHeader_2" 
	        <?php
				if($path == "pegawai/admin" || $path == "pejabat/admin" || $path == "user/admin")
					{print "class='tabActiveHeader'";}
			?>>
	        	<i class="icon-file icon-white"></i> Master Data
	        </li>

	        <li id="tabHeader_3" 
	        <?php 
				if($path == "spd/create" || $path == "spd/adminspd" || $path == "kwitansi/create" || $path == "lhp/create" || $path == "rill/create")
					{print "class='tabActiveHeader'";}
			?>>
	        	<i class="icon-pencil icon-white"></i> Input Data
	        </li>

	        <li id="tabHeader_4" 
	        <?php 
				if($path == "spd/admin" || $path == "spd/spd" || $path == "kwitansi/admin" || $path == "lhp/admin" || $path == "rill/create")
					{print "class='tabActiveHeader'";}
			?>>
	        	<i class="icon-print icon-white"></i> Cetak Data
	        </li>

	        <li id="tabHeader_5" 
	        <?php 
	        	if($path == "spd/laporan" || $path == "site/blanko" || $path == "spd/setting" || $path == "spd/rekap")
	        		{print "class='tabActiveHeader'";} 
	        ?>>
	        	<i class="icon-list-alt icon-white"></i> Laporan
	        </li>

	        <li id="tabHeader_6" 
	        <?php 
	        	if($path == "ssh/create" || $path == "akomodasi/create" || $path == "visum/create")
	        		{print "class='tabActiveHeader'";} 
	        ?>>
	        	<i class="icon-barcode icon-white"></i> Utility
	        </li>
	      </ul>
	    </div>
	    <div id="tabscontent">
	      <div class="tabpage" id="tabpage_1">
		      	<ul>
		      		<li>
		      			<a href=""><i class="icon-user icon-black"></i> Otorisasi Menu</a>
		      		</li>
		      		<li>
		      			<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=site/excel"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/excel-black.png" alt="document" width="15px"/> Ekspor Ke Excel</a>
		      		</li>
		      		<li>
		      			<a href="<?php print $this->createUrl('/jbackup'); ?>"><i class="icon-hdd icon-black"></i> Backup Data</a>
		      		</li>
		      		<li>
		      			<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=perjalanan/"><i class="icon-calendar icon-black"></i> Kalender Perjalanan</a>
		      		</li>
		      		<li>
		      			<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=site/logout"><i class="icon-off icon-black"></i> Keluar Aplikasi</a>
		      		</li>
		      	</ul>
	      </div>
	      <div class="tabpage" id="tabpage_2">
	      		<ul>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=pegawai/admin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/group-black.png" alt="document" width="20px"/> Master Pegawai</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=pejabat/admin"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/user-group-black.png" alt="document" width="20px"/> Master TTD Doc</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=user/admin"><i class="icon-user icon-black"></i> User</a>
	      			</li>
	      		</ul>
	      </div>
	      <div class="tabpage" id="tabpage_3">
	      		<ul>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=spd/create"><i class="icon-plus icon-black"></i> Input SP</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=spd/adminspd"><i class="icon-plus icon-black"></i> Input SPD</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=kwitansi/create"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/kwitansi.png" alt="document" width="20px"/> Kwitansi</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=lhp/create"><i class="icon-file icon-black"></i> Input LHP</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=rill/create"><i class="icon-book icon-black"></i> Pernyataan Riil</a>
	      			</li>
	      		</ul>
	      </div>
	      <div class="tabpage" id="tabpage_4">
	      		<ul>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=spd/admin"><i class="icon-print icon-black"></i> Cetak SP</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=spd/spd"><i class="icon-print icon-black"></i> Cetak SPD</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=kwitansi/admin"><i class="icon-print icon-black"></i> Cetak Kwitansi</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=lhp/admin"><i class="icon-print icon-black"></i> Cetak LHP</a>
	      			</li>
	      		</ul>
	      </div>
	      <div class="tabpage" id="tabpage_5">
	      		<ul>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=spd/laporan"><i class="icon-list icon-black"></i> Rekap Laporan</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=site/blanko"><i class="icon-book icon-black"></i> Blanko SPD</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=spd/setting"><i class="icon-wrench icon-black"></i> Setting SPD</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=spd/rekap"><i class="icon-list icon-black"></i> Rekap Per SPD</a>
	      			</li>
	      		</ul>
	      </div>
	      <div class="tabpage" id="tabpage_6">
	      		<ul>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=ssh/create"><i class="icon-calendar icon-black"></i> Uang Harian</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=akomodasi/create"><i class="icon-inbox icon-black"></i> Akomodasi</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=visum/create"><i class="icon-pencil icon-black"></i> Visum</a>
	      			</li>
	      		</ul>
	      </div>
	    </div>
  	</div>
  	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/tabs_old.js"></script>
        

        
        
   

        <div class="container-fluid" style="padding-left: 0px; padding-right: 0px; background:#647DFB;">
            
                    
	
	
		<?php
			foreach(Yii::app()->user->getFlashes() as $key => $message) {
				echo '<div class="alert alert-' . $key . '">';
				echo '<button type="button" class="close" data-dismiss="alert">×</button>';
				print $message;
				print "</div>\n";
				
				
			}
		?>
		
		<?php print $content; ?>

            
        </div>
    </div>
    
<?php $this->endContent(); ?>

   


