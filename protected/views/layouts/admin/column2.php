<?php $this->beginContent('//layouts/admin/main'); ?>


<?php
/*
Yii::app()->clientScript->registerScript(
   'myHideEffect',
   '$(".alert").animate({opacity: 1.0}, 3000).fadeOut("slow");',
   CClientScript::POS_READY
);
*/
?>
    <div class="content">
		<?php
			$controller = Yii::app()->controller->id;
			$action = Yii::app()->controller->action->id;
		?>
    	<div id="tabContainer">
	    <div id="tabs">
	      <ul>
			<?php
				$tabActive = 0;
				if($controller=='site' AND $action == 'index') $tabActive = 1;
				if($controller=='site' AND $action == 'admin') $tabActive = 1;
				if($controller=='perjalanan' AND $action == 'index') $tabActive = 1;
				if($controller=='perjalanan' AND $action == 'rekap') $tabActive = 1;
				if($controller=='default' AND $action == 'index') $tabActive = 1;
			?>
			
	        <li id="tabHeader_1" class='<?php if($tabActive) print "tabActiveHeader"; ?>'>
	        	<i class="icon-folder-open icon-white"></i> Berkas
	        </li>
			
			<?php
				$tabActive = 0;
				if($controller=='pegawai' AND $action == 'create') $tabActive = 1;
				if($controller=='pegawai' AND $action == 'update') $tabActive = 1;
				if($controller=='pejabat' AND $action == 'create') $tabActive = 1;
				if($controller=='pejabat' AND $action == 'update') $tabActive = 1;
				if($controller=='user' AND $action == 'create') $tabActive = 1;
				if($controller=='user' AND $action == 'update') $tabActive = 1;
				if($controller=='role' AND $action == 'create') $tabActive = 1;
				if($controller=='role' AND $action == 'update') $tabActive = 1;
				if($controller=='role' AND $action == 'access') $tabActive = 1;				
			?>
	        <li id="tabHeader_2" class='<?php if($tabActive) print "tabActiveHeader"; ?>'>
	        	<i class="icon-file icon-white"></i> Master Data
	        </li>
			
			<?php
				$tabActive = 0;
				if($controller=='spd' AND $action == 'inputSpt') $tabActive = 1;
				if($controller=='spd' AND $action == 'updateSpt') $tabActive = 1;
				if($controller=='spd' AND $action == 'inputSpd') $tabActive = 1;
				if($controller=='spd' AND $action == 'updateSpd') $tabActive = 1;
				if($controller=='kwitansi' AND $action == 'input') $tabActive = 1; 
				if($controller=='kwitansi' AND $action == 'update') $tabActive = 1; 
				if($controller=='lhp' AND $action == 'input') $tabActive = 1; 
				if($controller=='lhp' AND $action == 'update') $tabActive = 1; 
				if($controller=='riil' AND $action == 'input') $tabActive = 1; 
				if($controller=='riil' AND $action == 'update') $tabActive = 1; 
			?>
	        <li id="tabHeader_3" class="<?php if($tabActive) print "tabActiveHeader"; ?>">
	        	<i class="icon-pencil icon-white"></i> Input Data
	        </li>
			
			<?php
				$tabActive = 0;
				if($controller=='spd' AND $action == 'cetakSpt') $tabActive = 1;
				if($controller=='spd' AND $action == 'cetakSpd') $tabActive = 1;
				if($controller=='spd' AND $action == 'viewSpd') $tabActive = 1;
				if($controller=='spd' AND $action == 'viewSpt') $tabActive = 1;
				if($controller=='kwitansi' AND $action == 'cetak') $tabActive = 1; 
				if($controller=='kwitansi' AND $action == 'view') $tabActive = 1; 
				if($controller=='lhp' AND $action == 'cetak') $tabActive = 1; 
				if($controller=='lhp' AND $action == 'view') $tabActive = 1; 
			?>
	        <li id="tabHeader_4" class="<?php if($tabActive) print "tabActiveHeader"; ?>">
	        	<i class="icon-print icon-white"></i> Cetak Data
	        </li>
			
			<?php
				$tabActive = 0;
				if($controller=='pegawai' AND $action == 'excel') $tabActive = 1;
				if($controller=='spd' AND $action == 'excelSpt') $tabActive = 1;
				if($controller=='spd' AND $action == 'excelSpd') $tabActive = 1;
				if($controller=='kwitansi' AND $action == 'excel') $tabActive = 1;
				if($controller=='lhp' AND $action == 'excel') $tabActive = 1;
			?>
	        <li id="tabHeader_5" class="<?php if($tabActive) print "tabActiveHeader"; ?>">
	        	<img src="<?php echo Yii::app()->request->baseUrl; ?>/img/excel-putih.png" alt="document" width="15px"/> Laporan/Rekap
	        </li>
			
			<?php
				$tabActive = 0;
				if($controller=='uangHarian' AND $action == 'create') $tabActive = 1;
				if($controller=='uangHarian' AND $action == 'update') $tabActive = 1;
				if($controller=='visum' AND $action == 'create') $tabActive = 1;
				if($controller=='visum' AND $action == 'update') $tabActive = 1;
				if($controller=='akomodasi' AND $action == 'create') $tabActive = 1;
				if($controller=='akomodasi' AND $action == 'update') $tabActive = 1;
			?>
	        <li id="tabHeader_7" class="<?php if($tabActive) print "tabActiveHeader"; ?>">
	        	<i class="icon-barcode icon-white"></i> Utility
	        </li>
			
			
	      </ul>
	    </div>
	    <div id="tabscontent">
	      <div class="hidden tabpage" id="tabpage_1">
		      	<ul>
					<li>
		      			<a href="<?php print $this->createUrl('/perjalanan'); ?>"><i class="icon-calendar icon-black"></i> Kalender Perjalanan</a>
		      		</li>
					<li>
		      			<a href="<?php print $this->createUrl('/perjalanan/rekap'); ?>"><i class="icon-align-left icon-black"></i> Rekap Perjalanan</a>
		      		</li>
		      		<li>
		      			<a href="<?php print $this->createUrl('/jbackup'); ?>"><i class="icon-hdd icon-black"></i> Backup Data</a>
		      		</li>
		      		
		      		<li>
		      			<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=site/logout"><i class="icon-off icon-black"></i> Keluar Aplikasi</a>
		      		</li>
		      	</ul>
	      </div>
	      <div class="hidden tabpage" id="tabpage_2">
	      		<ul>
	      			<li>
	      				<a href="<?php echo $this->createUrl('/pegawai/create'); ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/group-black.png" alt="document" width="20px"/> Master Pegawai</a>
	      			</li>
	      			<li>
	      				<a href="<?php print $this->createUrl('/pejabat/create'); ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/user-group-black.png" alt="document" width="20px"/> Master TTD Doc</a>
	      			</li>
	      			<li>
	      				<a href="<?php print $this->createUrl('/user/create'); ?>"><i class="icon-user icon-black"></i> User</a>
	      			</li>
					<li>
	      				<a href="<?php print $this->createUrl('/role/create'); ?>"><i class="icon-user icon-black"></i> User Role</a>
	      			</li>
	      		</ul>
	      </div>
	      <div class="hidden tabpage" id="tabpage_3">
	      		<ul>
	      			<li>
	      				<a href="<?php echo $this->createUrl('/spd/inputSpt'); ?>"><i class="icon-plus icon-black"></i> Input SPT</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo $this->createUrl('/spd/inputSpd'); ?>"><i class="icon-plus icon-black"></i> Input SPD</a>
	      			</li>
	      			<li>
	      				<a href="<?php print $this->createUrl('/kwitansi/input'); ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/kwitansi.png" alt="document" width="20px"/> Kwitansi</a>
	      			</li>
	      			<li>
	      				<a href="<?php print $this->createUrl('/lhp/input'); ?>"><i class="icon-file icon-black"></i> Input LHP</a>
	      			</li>
	      			<li>
	      				<a href="<?php print $this->createUrl('/riil/input'); ?>"><i class="icon-book icon-black"></i> Pernyataan Riil</a>
	      			</li>
	      		</ul>
	      </div>
	      <div class="hidden tabpage" id="tabpage_4">
	      		<ul>
	      			<li>
	      				<a href="<?php echo $this->createUrl('/spd/cetakSpt'); ?>"><i class="icon-print icon-black"></i> Cetak SPT</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo $this->createUrl('/spd/cetakSpd'); ?>"><i class="icon-print icon-black"></i> Cetak SPD</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo $this->createUrl('/kwitansi/cetak'); ?>"><i class="icon-print icon-black"></i> Cetak Kwitansi</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo $this->createUrl('/lhp/cetak'); ?>"><i class="icon-print icon-black"></i> Cetak LHP</a>
	      			</li>
	      		</ul>
	      </div>
		   <div class="hidden tabpage" id="tabpage_5">
	      		<ul>
	      			<li>
	      				<a href="<?php echo $this->createUrl('/pegawai/excel'); ?>"><i class="icon-download-alt icon-black"></i>Rekap Pegawai</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo $this->createUrl('/spd/excelSpt'); ?>"><i class="icon-download-alt icon-black"></i>Rekap SPT</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo $this->createUrl('/spd/excelSpd'); ?>"><i class="icon-download-alt icon-black"></i>Rekap SPD</a>
	      			</li>
					<li>
	      				<a href="<?php echo $this->createUrl('/kwitansi/excel'); ?>"><i class="icon-download-alt icon-black"></i>Rekap Kwitansi</a>
	      			</li>
					<li>
	      				<a href="<?php echo $this->createUrl('/lhp/excel'); ?>"><i class="icon-download-alt icon-black"></i>Rekap LHP</a>
	      			</li>
	      		</ul>
	      </div>
		  
	      <div class="hidden tabpage" id="tabpage_6">
	      		<ul>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=spd/laporan"><i class="icon-list icon-black"></i> Rekap Laporan</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=site/blanko"><i class="icon-book icon-black"></i> Blanko SPD</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=spd/setting"><i class="icon-wrench icon-black"></i> Setting SPD</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=spd/rekap"><i class="icon-list icon-black"></i> Rekap Per SPD</a>
	      			</li>
	      		</ul>
	      </div>
		 
	      <div class="hidden tabpage" id="tabpage_7">
	      		<ul>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=uangHarian/create"><i class="icon-calendar icon-black"></i> Uang Harian</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=akomodasi/create"><i class="icon-inbox icon-black"></i> Akomodasi</a>
	      			</li>
	      			<li>
	      				<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php?r=visum/create"><i class="icon-pencil icon-black"></i> Visum</a>
	      			</li>
	      		</ul>
	      </div>
		  <script>
			$(window).load(function() {
				$(".tabpage").removeClass("hidden");
			});
		  </script>
	    </div>
  	</div>
  	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/tabs_old.js"></script>
        
		<div>&nbsp;</div>
		<?php /*
        <div class="header">
            <h1 class="page-title"><?php print $this->pageTitle; ?></h1>
        </div>
        
        <ul class="breadcrumb" style="margin-top:0px !important">
            <li><a href="index.php">Home</a> <span class="divider">/</span></li>
            <li class="active">Dashboard</li>
        </ul>
		*/ ?>
        <div class="container-fluid">
            <div class="row-fluid">
    

	<?php foreach(Yii::app()->user->getFlashes() as $key => $message) { ?>
	<div class="row-fluid">
		<div class="span12 alert alert-<?php print $key; ?>">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<?php print  $message; ?>
		</div>
	</div>
	<?php } ?>	
	<div class="row-fluid">	
		<div class="span12">
			<?php print $content; ?>
		</div>
	</div>


                    
                    <footer>
                        <hr>
						<?php /*
                        <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes -->
                        <p class="pull-right">A <a href="http://www.portnine.com/bootstrap-themes" target="_blank">Free Bootstrap Theme</a> by <a href="http://www.portnine.com" target="_blank">Portnine</a></p>
                        */ ?>

                        <p>Copyright &copy; 2014 <a href="" target="_blank">Surat Perjalanan Dinas</a></p>
                    </footer>
                    
            </div>
        </div>
    </div>
    
<?php $this->endContent(); ?>