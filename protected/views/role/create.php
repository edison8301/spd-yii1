<?php
$this->breadcrumbs=array(
	'Roles'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Role','url'=>array('index')),
array('label'=>'Manage Role','url'=>array('admin')),
);
?>

<h1>Entry Role</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<h2>Data Role</h2>

<?php $this->renderPartial('_admin',array('role'=>$role)); ?>


