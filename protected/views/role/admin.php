<?php
$this->breadcrumbs=array(
	'Roles'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Role','url'=>array('index')),
array('label'=>'Create Role','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('role-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Entry Data Role</h1>

<?php $this->renderPartial('_form',array('model'=>$model)); ?>

<?php $this->renderPartial('_admin',array('role'=>$role)); ?>


