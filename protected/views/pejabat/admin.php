<?php
$this->breadcrumbs=array(
	'Pejabat'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Pejabat','url'=>array('index')),
array('label'=>'Create Pejabat','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('pejabat-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<?php
            /*$this->widget('bootstrap.widgets.TbButtonGroup', array(
            //'size'=>'small',
            'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'buttons'=>array(
            array('label'=>'Menu', 'items'=>array(
					array('label'=>'Tambah Pejabat', 'icon'=>'plus', 'url'=>array('pejabat/create')),
					array('label'=>'Unduh Ke PDF','linkOptions'=>array('target'=>'_blank'), 'icon'=>'download-alt', 'url'=>array('export/exportPejabat')),
                )),
            ),
            ));*/
?>
<h1>Entry Data Penanda Tangan Dokumen</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<?php echo $this->renderPartial('_admin', array('pejabat'=>$pejabat)); ?>
