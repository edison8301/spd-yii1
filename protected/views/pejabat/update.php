<?php
$this->breadcrumbs=array(
	'Pejabat'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Pejabat','url'=>array('index')),
	array('label'=>'Create Pejabat','url'=>array('create')),
	array('label'=>'View Pejabat','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Pejabat','url'=>array('admin')),
	);
	?>

<h1>Sunting Penandatangan Dokumen</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>

<h2>Data Penandatangan Dokumen</h2>

<?php echo $this->renderPartial('_admin',array('pejabat'=>$pejabat)); ?>
