<?php
$this->breadcrumbs=array(
	'Pejabats'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Pejabat','url'=>array('index')),
array('label'=>'Create Pejabat','url'=>array('create')),
array('label'=>'Update Pejabat','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Pejabat','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Pejabat','url'=>array('admin')),
);
?>

<h1>Detail Pejabat <?php echo $model->nama; ?></h1>

<?php
            $this->widget('bootstrap.widgets.TbButtonGroup', array(
            //'size'=>'small',
            'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'buttons'=>array(
            array('label'=>'Menu', 'items'=>array(
					array('label'=>'Tambah Pejabat', 'icon'=>'plus', 'url'=>array('pejabat/create')),
                    array('label'=>'Sunting Pejabat', 'icon'=>'plus', 'url'=>array('pejabat/update','id'=>$model->id)),
					array('label'=>'Unduh Ke PDF','linkOptions'=>array('target'=>'_blank'), 'icon'=>'download-alt', 'url'=>array('export/ExportDataPejabat','id'=>$model->id)),
                )),
            ),
            ));
?>
<div> &nbsp; </div>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'nip',
		'nama',
		'jabatan',
),
)); ?>
