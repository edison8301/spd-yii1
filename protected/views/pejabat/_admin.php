<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'pejabat-grid',
		'type'=> 'striped bordered',
		'dataProvider'=>$pejabat->search(),
		'filter'=>$pejabat,
		'columns'=>array(
			array(
				'class'=>'CDataColumn',
				'name'=>'foto',
				'header'=>'Foto',
				'type'=>'raw',
				'value'=>'$data->getFoto()',
				'headerHtmlOptions'=>array('style'=>'width:10%')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'nip',
				'header'=>'NIP',
				'type'=>'raw',
				'value'=>'$data->getNip()',
			),
			'nama',
			'jabatan',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}{delete}'
			),
		),
)); ?>