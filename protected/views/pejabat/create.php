<?php
$this->breadcrumbs=array(
	'Pejabat'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Pejabat','url'=>array('index')),
array('label'=>'Manage Pejabat','url'=>array('admin')),
);
?>

<h1>Entry Penandatangan Dokumen</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<h2>Data Penandatangan Dokumen</h2>

<?php echo $this->renderPartial('_admin', array('pejabat'=>$pejabat)); ?>