<table border="<?php print $border; ?>" class="table">
<tr>
	<th>NO KWITANSI</th>
	<th>NO LAMP SPD</th>
	<th>TGL KWITANSI</th>
	<th>KORING PD</th>
	<th>JENIS PD</th>
	<th>PEGAWAI</th>
	<th>NIP PEGAWAI</th>
	<th>JAB PEGAWAI</th>
	
	<th>MAKSUD</th>
	<th>TUJUAN</th>
	<th>TGL PERGI</th>
	<th>TGL KEMBALI</th>
	
	<th>UANG HARIAN</th>
	<th>AKOMODASI</th>
	<th>BBM</th>
	<th>TOL</th>
	<th>TIKET</th>
	<th>TOTAL</th>
	<th>TERBILANG</th>
	<th>BENDAHARA</th>
	<th>NIP BENDAHARA</th>
	<th>PPTK</th>
	<th>NIP PPTK</th>
	<th>PEJABAT PA</th>
	<th>NIP PEJABAT PA</th>
	<th>LAMA</th>
	<th>SAT</th>
	<th>REF</th>
</tr>
<?php if(isset($_GET['tampil'])) { ?>
<?php
		$criteria = new CDbCriteria;
		$criteria->order = 'tanggal ASC';
		
		if(!empty($_GET['tanggal_awal']) AND !empty($_GET['tanggal_akhir']))
		{
			$criteria->condition = 'tanggal >= :tanggal_awal AND tanggal <= :tanggal_akhir';
			$criteria->params = array(':tanggal_awal'=>$_GET['tanggal_awal'],':tanggal_akhir'=>$_GET['tanggal_akhir']);
		}
?>

<?php $i=1; foreach(Kwitansi::model()->findAll($criteria) as $data) { ?>
<tr>
	<td><?php print $data->nomor; ?></td>
	<td><?php print $data->getRelationField("spd","nomor_spd"); ?></td>
	<td><?php print $data->tanggal; ?></td>
	<td><?php print $data->getRelationField("spd","koring"); ?></td>
	<td><?php print $data->getRelationField("spd","koring"); ?></td>
	<td><?php print $data->getRelationField("penerimaRelation","nama"); ?></td>
	<td><?php print $data->getRelationField("penerimaRelation","nip"); ?></td>
	<td><?php print $data->getRelationField("penerimaRelation","jabatan"); ?></td>
	
	<td><?php print $data->getRelationField("spd","maksud"); ?></td>
	<td><?php print $data->getRelationField("spd","tujuan"); ?></td>
	<td><?php print $data->getRelationField("spd","tgl_pergi"); ?></td>
	<td><?php print $data->getRelationField("spd","tgl_kembali"); ?></td>
	
	<td><?php print $data->getRelationField("spd","lama")*$data->uang_harian; ?></td>
	<td><?php print $data->akomodasi; ?></td>
	<td><?php print $data->bbm; ?></td>
	<td><?php print $data->tol; ?></td>
	<td><?php print $data->tiket; ?></td>
	<td>&nbps;</td>
	<td>&nbsp;</td>
	<td><?php print $data->getRelationField("bendaharaRelation","nama"); ?></td>
	<td><?php print $data->getRelationField("bendaharaRelation","nip"); ?></td>
	<td><?php print $data->getRelationField("pptkRelation","nama"); ?></td>
	<td><?php print $data->getRelationField("pptkRelation","nip"); ?></td>
	<td><?php print $data->getRelationField("paRelation","nama"); ?></td>
	<td><?php print $data->getRelationField("paRelation","nip"); ?></td>
	<td><?php print $data->getRelationField("spd","lama"); ?></td>
	<td>&nbsp;</td>
	<td><?php print $data->refresentatif; ?></td>
</tr>
<?php $i++; } ?>
<?php } ?>
</table>