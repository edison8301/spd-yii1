<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor')); ?>:</b>
	<?php echo CHtml::encode($data->nomor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_spd')); ?>:</b>
	<?php echo CHtml::encode($data->id_spd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_akomodasi')); ?>:</b>
	<?php echo CHtml::encode($data->id_akomodasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_refresentatif')); ?>:</b>
	<?php echo CHtml::encode($data->id_refresentatif); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_ssh')); ?>:</b>
	<?php echo CHtml::encode($data->id_ssh); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bendahara')); ?>:</b>
	<?php echo CHtml::encode($data->bendahara); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('ppk')); ?>:</b>
	<?php echo CHtml::encode($data->ppk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bbm')); ?>:</b>
	<?php echo CHtml::encode($data->bbm); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tol')); ?>:</b>
	<?php echo CHtml::encode($data->tol); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tiket')); ?>:</b>
	<?php echo CHtml::encode($data->tiket); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total')); ?>:</b>
	<?php echo CHtml::encode($data->total); ?>
	<br />

	*/ ?>

</div>