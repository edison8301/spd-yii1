
<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'kwitansi-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$kwitansi->search(),
		'filter'=>$kwitansi,
		'columns'=>array(
			array(
				'class'=>'CDataColumn',
				'type'=>'raw',
				'header'=>'Cetak',
				'value'=>'CHtml::link("<center><i class=icon-print ></i></center>",array("kwitansi/view","id"=>"$data->id"))',
			),
			'nomor',
			array(
				'class'=>'CDataColumn',
				'name'=>'id_spd',
				'header'=>'No. SPD',
				'type'=>'raw',
				'value'=>'$data->getRelationField("spd","nomor_spd")',
				'filter'=>CHtml::listData(Spd::model()->findAll(),'id','nomor_spd')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'penerima',
				'header'=>'Penerima',
				'type'=>'raw',
				'value'=>'$data->getRelationField("penerimaRelation","nama")',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'uang_harian',
				'header'=>'Uang Harian',
				'type'=>'raw',
				'value'=>'number_format($data->uang_harian, 0,",",".")',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_spd',
				'header'=>'Lama',
				'type'=>'raw',
				'value'=>'$data->getRelationField("spd","lama")',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'uang_harian',
				'header'=>'Total Uang Harian',
				'type'=>'raw',
				'value'=>'number_format($data->uang_harian*$data->getRelationField("spd","lama"), 0,",",".")',
			),
			
			array(
				'class'=>'CDataColumn',
				'name'=>'akomodasi',
				'header'=>'Akomodasi',
				'type'=>'raw',
				'value'=>'number_format($data->akomodasi, 0,",",".")',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'refresentatif',
				'header'=>'Refresentatif',
				'type'=>'raw',
				'value'=>'number_format($data->refresentatif, 0,",",".")',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'tol',
				'header'=>'Tol',
				'type'=>'raw',
				'value'=>'number_format($data->tol, 0,",",".")',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'tiket',
				'header'=>'Tiket',
				'type'=>'raw',
				'value'=>'number_format($data->tiket, 0,",",".")',
			),
			
			array(
				'class'=>'CDataColumn',
				'name'=>'total',
				'header'=>'Total',
				'type'=>'raw',
				'value'=>'number_format($data->total, 0,",",".")',
			),
			
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}{delete}',
				'htmlOptions'=>array('style'=>'width:50px')
			)
			
		),
)); ?>
	
