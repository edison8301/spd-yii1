<?php
$this->breadcrumbs=array(
	'Kwitansis'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Kwitansi','url'=>array('index')),
array('label'=>'Create Kwitansi','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('kwitansi-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Cetak Tanda Bukti Pembayaran</h1>

<div>Silahkan klik simbol printer <i class="icon-print"></i> untuk memilih data yang akan dicetak</div>

<?php print $this->renderPartial('_admin',array('kwitansi'=>$model)); ?>