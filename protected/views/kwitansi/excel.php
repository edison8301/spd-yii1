
<h1>Export Excel SPT</h1>

<?php print CHtml::beginForm(array('kwitansi/excel'),'GET'); ?>

<?php print CHtml::hiddenField('tampil',1); ?>

<?php print CHtml::label('Tanggal Awal',''); ?>
<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'name' => 'tanggal_awal',
					'id'=>'tanggal_awal',
					'language' => 'id',
					'value'=>isset($_GET['tanggal_awal']) ? $_GET['tanggal_awal'] : "",
					// additional javascript options for the date picker plugin
					'options'=>array(
					    'showAnim'=>'fold',
					    'showOn'=>'button',
					    'buttonImage'=>Yii::app()->baseUrl.'/img/calendar.png',
					    'dateFormat'=>'yy-mm-dd',
					    'changeMonth' => 'false',
					    'showButtonPanel' => 'false',
					    'changeYear'=>'false',
					    'constrainInput' => 'false',
					),
					'htmlOptions'=>array(
					    'style'=>'height:20px;width:150px; margin-bottom:0px',
					    'readonly'=>true,
					),
				));?>
				
<?php print CHtml::label('Tanggal Akhir',''); ?>
<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'name' => 'tanggal_akhir',
					'id'=>'tanggal_akhir',
					'language' => 'id',
					'value'=>isset($_GET['tanggal_akhir']) ? $_GET['tanggal_akhir'] : "",
					// additional javascript options for the date picker plugin
					'options'=>array(
					    'showAnim'=>'fold',
					    'showOn'=>'button',
					    'buttonImage'=>Yii::app()->baseUrl.'/img/calendar.png',
					    'dateFormat'=>'yy-mm-dd',
					    'changeMonth' => 'false',
					    'showButtonPanel' => 'false',
					    'changeYear'=>'false',
					    'constrainInput' => 'false',
					),
					'htmlOptions'=>array(
					    'style'=>'height:20px;width:150px; margin-bottom:0px',
					    'readonly'=>true,
					),
				));?>
				
<div class="form-actions">
	

<?php $this->widget('bootstrap.widgets.TbButton',array(
		'buttonType'=>'submit',
		'label'=>'Tampilkan Data Export',
		'type'=>'primary',
		'icon'=>'search white',
)); ?>

<?php if(isset($_GET['tampil']) AND $_GET['tampil']==1) { ?>
<?php $this->widget('bootstrap.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Export Data',
		'type'=>'primary',
		'icon'=>'download-alt white',
		'url'=>array('kwitansi/exportExcel','tampil'=>1,'tanggal_awal'=>$_GET['tanggal_awal'],'tanggal_akhir'=>$_GET['tanggal_akhir'])
)); ?>
<?php } ?>

</div>

<div>&nbsp;</div>

<div style="overflow:auto">
	<?php $this->renderPartial('_excel',array('border'=>'0px')); ?>
</div>