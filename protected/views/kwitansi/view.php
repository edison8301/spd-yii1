<?php
$this->breadcrumbs=array(
	'Kwitansis'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Kwitansi','url'=>array('index')),
array('label'=>'Create Kwitansi','url'=>array('create')),
array('label'=>'Update Kwitansi','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Kwitansi','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Kwitansi','url'=>array('admin')),
);
?>

<h1>Cetak Kwitansi</h1>

<div>Silahkan pilih jenis kwitansi yang akan dicetak pada sisi kiri laman</div>

<?php print Bantu::getTerbilang("921000",0); ?>

<div>&nbsp;</div>

<div class="row-fluid">
	<div class="span3">
		<?php $this->widget('bootstrap.widgets.TbButton',array(
				'label' => 'Kwitansi Dalam Daerah',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('kwitansi/cetakPdf','id'=>$model->id,'dalamDaerah'=>1),
				'htmlOptions'=>array('target'=>'_blank','style'=>'margin-bottom:2px')
		)); ?>

		<?php $this->widget('bootstrap.widgets.TbButton',array(
				'label' => 'Kwitansi Luar Daerah',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('kwitansi/cetakPdf','id'=>$model->id,'luarDaerah'=>1),
				'htmlOptions'=>array('target'=>'_blank')
		)); ?>

		<div>&nbsp;</div>

		<?php $this->widget('bootstrap.widgets.TbButton',array(
				'label' => 'Kwitansi Dalam Daerah Non PPTK',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('kwitansi/cetakPdf','id'=>$model->id,'dalamDaerah'=>1,'refresentatif'=>1,'nonPptk'=>1),
				'htmlOptions'=>array('target'=>'_blank','style'=>'margin-bottom:2px')
		)); ?>
			
		<?php $this->widget('bootstrap.widgets.TbButton',array(
				'label' => 'Kwitansi Luar Daerah Non PPTK',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('kwitansi/cetakPdf','id'=>$model->id,'luarDaerah'=>1,'nonPptk'=>1),
				'htmlOptions'=>array('target'=>'_blank')
		)); ?>
		
		<div>&nbsp;</div>

		<?php $this->widget('bootstrap.widgets.TbButton',array(
				'label' => 'Kwitansi Dalam Daerah Refresentatif',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('kwitansi/cetakPdf','id'=>$model->id,'dalamDaerah'=>1,'refresentatif'=>1),
				'htmlOptions'=>array('target'=>'_blank','style'=>'margin-bottom:2px')
		)); ?>
			
		<?php $this->widget('bootstrap.widgets.TbButton',array(
				'label' => 'Kwitansi Luar Daerah Refresentatif',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('kwitansi/cetakPdf','id'=>$model->id,'luarDaerah'=>1,'refresentatif'=>1),
				'htmlOptions'=>array('target'=>'_blank')
		)); ?>
		
		<div>&nbsp;</div>
		
		<?php /*
		<?php $this->widget('bootstrap.widgets.TbButton',array(
				'label' => 'Register No Bukti',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('export/ExportKwitansi','id'=>$model->id),
				'htmlOptions'=>array('target'=>'_blank','style'=>'margin-bottom:2px')
		)); ?>
		
		<?php $this->widget('bootstrap.widgets.TbButton',array(
				'label' => 'Rekap Detail Bukti',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('export/ExportKwitansiDetail','id'=>$model->id),
				'htmlOptions'=>array('target'=>'_blank')
		)); ?>
		*/ ?>
	</div>
	<div class="span9">	
			<?php $this->widget('bootstrap.widgets.TbDetailView',array(
					'data'=>$model,
					'type'=>'striped bordered',
					'attributes'=>array(
						'nomor',
						array(
							'label'=>'Nomor SPD',
							'type'=>'raw',
							'value'=>CHtml::encode($model->getRelationField("spd","nomor_spd"))
						),
						array(
							'label'=>'Akomodasi',
							'type'=>'raw',
							'value'=>'Rp '.number_format($model->akomodasi, 2,",",".")
						),
						array(
							'label'=>'Refresentatif',
							'type'=>'raw',
							'value'=>'Rp '.number_format($model->refresentatif, 2,",",".")
						),
						array(
							'label'=>'Uang Harian',
							'type'=>'raw',
							'value'=>'Rp '.number_format($model->uang_harian, 2,",",".")
						),
						array(
							'label'=>'Bendahara',
							'type'=>'raw',
							'value'=>$model->getRelationField("bendaharaRelation","nama")
						),
						array(
							'label'=>'PPTK',
							'type'=>'raw',
							'value'=>$model->getRelationField("pptkRelation","nama")
						),
						array(
							'label'=>'PA/KPA',
							'type'=>'raw',
							'value'=>$model->getRelationField("paRelation","nama")
						),
						array(
							'label'=>'BBM',
							'type'=>'raw',
							'value'=>'Rp '.number_format($model->bbm, 2,",",".")
						),
						array(
							'label'=>'Tol',
							'type'=>'raw',
							'value'=>'Rp '.number_format($model->tol, 2,",",".")
						),
						array(
							'label'=>'Tiket',
							'type'=>'raw',
							'value'=>'Rp '.number_format($model->tiket, 2,",",".")
						),
						array(
							'label'=>'Total',
							'type'=>'raw',
							'value'=>'Rp <b>'.number_format($model->total, 2,",",".").'</b>'
						),
					),
			)); ?>
			
			<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','type'=>'primary','label'=>'Sunting Kwitansi','icon'=>'pencil white','url'=>array('kwitansi/update','id'=>$model->id))); ?>

	</div>
</div>


<div>&nbsp;</div>

