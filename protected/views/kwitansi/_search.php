<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'nomor',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'id_spd',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'id_akomodasi',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'id_refresentatif',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'id_ssh',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'bendahara',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'ppk',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'bbm',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'tol',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'tiket',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'total',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
