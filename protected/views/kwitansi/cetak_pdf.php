	<div style="line-height:100%;text-align:right"><i>Lampiran VII<br>Permenkeu RI. no.113 /PMK 05/2012</i></div>
	<div style="text-align:center;line-height:50%"><b><u>RINCIAN BIAYA PERJALANAN DINAS</u></b></div>
		
		<br>
		<?php 
		$tanggal_spd = $model->getRelationField("spd","tanggal_spt");
		
		if($model->getRelationField("spd","tanggal_spd")!=null)
		{
			$tanggal_spd = $model->getRelationField("spd","tanggal_spd");
		}

		?>
			    <table width="100%">
					<tr>
			    		<td width="25%">Lampiran SPPD Nomor</td>
			    		<td width="75%">: <?= $model->getRelationField("spd","nomor_spd") ?></td>
			    	</tr>
			    	<tr>
			    		<td width="25%">Tanggal</td>
			    		<td width="75%">: <?= Bantu::tanggal($tanggal_spd) ?></td>
			    	</tr>
			    </table>

			    <table width="100%" cellpadding="1px">
			    	<thead>
			    	<tr>
			    		<td width="7%" style="text-align:center;font-weight:bold;border-top:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000">NO</td>
			    		<td width="36%" style="text-align:center;font-weight:bold;border-top:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000">Perincian Biaya</td>
			    		<td width="26%" style="text-align:center;font-weight:bold;border-top:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000">Jumlah</td>
			    		<td width="31%" style="text-align:center;font-weight:bold;border-top:1px solid #000;border-bottom:1px solid #000;border-left:1px solid #000;border-right:1px solid #000">Keterangan</td>
			    	</tr>

		<?php
		
		$i = 1;
		
		$total = 0;
		
		if(isset($_GET['dalamDaerah']) AND $_GET['dalamDaerah']==1)
		{
		
		?>
			    	<tr>
			    		<td width="7%" style="text-align:center;border-left:1px solid #000">
			    			<?= $i ?>
			    		</td>
			    		<td width="36%" style="border-left:1px solid #000">
			    			Uang Harian
			    		</td>
			    		<td width="26%" style="border-left:1px solid #000;text-align:right">
			    			Rp <?= number_format($model->uang_harian*$model->getRelationField("spd","lama"), 2,",",".") ?>
			    		</td>
			    		<td width="31%" style="border-left:1px solid #000;border-right:1px solid #000"><?= $model->getRelationField("spd","maksud") ?></td>
			    	</tr> <?php 
			$i++; $total = $total + $model->uang_harian*$model->getRelationField("spd","lama");
			
		}
		
		if(isset($_GET['luarDaerah']) AND $_GET['luarDaerah']==1)
		{
			?>
			    	<tr>
			    		<td width="7%" style="text-align:center;border-left:1px solid #000;line-height:75%">
			    			<?= $i ?>
			    		</td>
			    		<td width="36%" style="border-left:1px solid #000;line-height:75%">
			    			Biaya Transport
			    		</td>
			    		<td width="26%" style="border-left:1px solid #000;text-align:right;line-height:75%">&nbsp;
			    			
			    		</td>
			    		<td width="31%" style="border-left:1px solid #000;border-right:1px solid #000;line-height:75%">&nbsp;<?= $model->spd->maksud ?></td>
			    	</tr> <?php
					
			$i++;
			
		?>
			    	<tr>
			    		<td width="7%" style="text-align:center;border-left:0px solid #000;line-height:75%">&nbsp;
			    			
			    		</td>
			    		<td width="36%" style="border-left:0px solid #000;line-height:75%"> 
							&nbsp;&nbsp;&nbsp;BBM
			    		</td>
			    		<td width="26%" style="border-left:0px solid #000;text-align:right;line-height:75%">
			    			Rp <?= number_format($model->bbm, 2,",",".") ?>
			    		</td>
			    		<td width="31%" style="border-left:0px solid #000;border-right:0px solid #000;line-height:75%">&nbsp;
							
						</td>
			    	</tr> <?php
			
			$total = $total + $model->bbm;
					
			?>
			    	<tr>
			    		<td width="7%" style="text-align:center;border-left:0px solid #000;line-height:75%">&nbsp;
			    			
			    		</td>
			    		<td width="36%" style="border-left:0px solid #000;line-height:75%">
			    			&nbsp;&nbsp;&nbsp;TOL
			    		</td>
			    		<td width="26%" style="border-left:0px solid #000;text-align:right;line-height:75%">
			    			Rp <?= number_format($model->tol, 2,",",".") ?>
			    		</td>
			    		<td width="31%" style="border-left:0px solid #000;border-right:0px solid #000;line-height:75%">&nbsp;
							
						</td>
			    	</tr> <?php
					
			$total = $total + $model->tol;
					
			$html ?>
			    	<tr>
			    		<td width="7%" style="text-align:center;border-left:0px solid #000;line-height:75%">&nbsp;
			    			
			    		</td>
			    		<td width="36%" style="border-left:0px solid #000;line-height:75%">
			    			&nbsp;&nbsp;&nbsp;Akomodasi
			    		</td>
			    		<td width="26%" style="border-left:0px solid #000;text-align:right;line-height:75%">
			    			Rp <?= number_format($model->akomodasi, 2,",",".") ?>
			    		</td>
			    		<td width="31%" style="border-left:0px solid #000;border-right:0px solid #000;line-height:75%">&nbsp;
							
						</td>
			    	</tr> <?php
					
			$total = $total + $model->akomodasi;
					
			?>
			    	<tr>
			    		<td width="7%" style="text-align:center;border-left:0px solid #000;line-height:75%">&nbsp;
			    			
			    		</td>
			    		<td width="36%" style="border-left:0px solid #000;line-height:75%">
			    			&nbsp;&nbsp;&nbsp;Tiket Pesawat
			    		</td>
			    		<td width="26%" style="border-left:0px solid #000;text-align:right;line-height:75%">
			    			Rp <?= number_format($model->tiket, 2,",",".") ?>
			    		</td>
			    		<td width="31%" style="border-left:0px solid #000;border-right:0px solid #000;line-height:75%">&nbsp;
							
						</td>
			    	</tr> <?php
			
			$total = $total + $model->tiket;
			
			?>
			    	<tr>
			    		<td width="7%" style="text-align:center;border-left:0px solid #000">
			    			'.$i.'.
			    		</td>
			    		<td width="36%" style="border-left:0px solid #000">
			    			Uang Harian ( <?= $model->getRelationField("spd","lama") ?> x Rp <?= number_format($model->uang_harian, 2,",",".") ?> )
			    		</td>
			    		<td width="26%" style="border-left:0px solid #000;text-align:right">
			    			Rp <?= number_format($model->uang_harian*$model->getRelationField("spd","lama"), 2,",",".") ?>
			    		</td>
			    		<td width="31%" style="border-left:0px solid #000;border-right:0px solid #000">&nbsp;
			    			
			    		</td>
			    	</tr> <?php
					
			$i++; $total = $total + $model->uang_harian*$model->getRelationField("spd","lama");
			
			
		
		}
		
		if(isset($_GET['refresentatif']) AND $_GET['refresentatif']==1)
		{
				?>
			    	<tr>
			    		<td width="7%" style="text-align:center;border-left:0px solid #000">
			    			<?= $i ?>.
			    		</td>
			    		<td width="36%" style="border-left:0px solid #000">
			    			Refresentatif
			    		</td>
			    		<td width="26%" style="border-left:0px solid #000;text-align:right">
			    			Rp '<?= pnumber_format($model->refresentatif, 2,",",".") ?>
			    		</td>
			    		<td width="31%" style="border-left:0px solid #000;border-right:0px solid #000">&nbsp;
							
						</td>
			    	</tr> <?php
				
				$total = $total + $model->refresentatif;
				
		}
		
		?>
			    	<tr style="line-height:10%;">
			    		<td width="7%" style="text-align:center;border-left:1px solid #000">&nbsp;
			    			
			    		</td>
			    		<td width="36%" style="border-left:1px solid #000">&nbsp;
			    			
			    		</td>
			    		<td width="26%" style="border-left:1px solid #000">&nbsp;
			    			
			    		</td>
			    		<td width="31%" style="border-left:1px solid #000;border-right:1px solid #000">&nbsp;
							
						</td>
			    	</tr> 
			    	<tr>
			    		<td width="7%" style="text-align:center;border-left:1px solid #000;border-bottom:1px solid #000">&nbsp;
			    			
			    		</td>
			    		<td width="36%" style="border-left:1px solid #000;border-bottom:1px solid #000">
			    			<b>Jumlah</b>
			    		</td>
			    		<td width="26%" style="border-left:1px solid #000;border-bottom:1px solid #000;text-align:right">
			    			<b>Rp <?= number_format($total, 2,",",".") ?></b>
			    		</td>
			    		<td width="31%" style="border-left:1px solid #000;border-right:1px solid #000;border-bottom:1px solid #000">&nbsp;
							
						</td>
			    	</tr> 
					<tr>
						<td colspan="4" style="border:1px solid #000;">&nbsp;&nbsp;&nbsp;<i>Terbilang: <?= Bantu::getTerbilang($total,0) ?> rupiah</i></td>
					</tr>
		
		</table> <?php
		
		$pejabat = "Pejabat Pelaksana Teknis Kegiatan";
		if(isset($_GET['nonPptk']) AND $_GET['nonPptk']==1) $pejabat = "&nbsp;";
		
		$namaPptk = $model->getRelationField("pptkRelation","nama");
		if(isset($_GET['nonPptk']) AND $_GET['nonPptk']==1) $namaPptk = "&nbsp;";
		
		$nip = $model->getRelationField("pptkRelation","nip");
		if(isset($_GET['nonPptk']) AND $_GET['nonPptk']==1) $nip = "&nbsp;";
		
		?>
				<table width="100%">
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td style="text-align:center">Serang, <?= Bantu::tanggal($model->getRelationField("spd","tgl_pergi")) ?></td>
					</tr>
					<tr>
			    		<td style="text-align:center">
			    			Telah Dibayar Sejumlah<br>
							<b>Rp <?= number_format($total, 2,",",".") ?></b>
						</td>
						<td>&nbsp;
							
						</td>
						<td style="text-align:center">
							Telah menerima jml uang sebesar<br>
							<b>Rp <?= number_format($total, 2,",",".") ?></b>
						</td>
					</tr>
					<tr>
						<td style="text-align:center">Bendahara Pengeluaran</td>
						<td style="text-align:center"><?= $pejabat ?></td>
						<td style="text-align:center">Yang Menerima</td>
					</tr>
					<tr>
						<td>&nbsp;<br>&nbsp;</td>
						<td>&nbsp;<br>&nbsp;</td>
						<td>&nbsp;<br>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;<br>&nbsp;</td>
						<td>&nbsp;<br>&nbsp;</td>
						<td>&nbsp;<br>&nbsp;</td>
					</tr>
					<tr>
						<td style="text-align:center"><b><u><?= $model->getRelationField("bendaharaRelation","nama") ?></u></b><br>NIP. <?= $model->getRelationField("bendaharaRelation","nip") ?></td>
						<td style="text-align:center"><b><u><?= $namaPptk ?></u></b><br>NIP. <?= $nip  ?></td>
						<td style="text-align:center"><b><u><?= $model->getRelationField("penerimaRelation","nama") ?></u></b><br>NIP. <?= $model->getRelationField("penerimaRelation","nip") ?></td>
			    	</tr>
				</table>
				<HR>
				<table>
					<tr>
						<td style="border-bottom:0px solid #000" width="100%">&nbsp;</td>
					</tr>
				</table>
		<div style="line-height:70%;text-align:center"><b>PERHITUNGAN SPDD RAMPUNG</b></div>
		<br>

	
				<table  width="100%">
			    	<tr>
			    			<td width="40%">
			    				Ditetapkan sejumlah
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp <?= number_format($total, 2,",",".") ?></b>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td width="40%">
			    				Yang telah dibayarkan
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp <?= number_format($total, 2,",",".") ?></b>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td width="40%">
			    				Sisa kurang/lebih
			    			</td>
			    			<td width="20%">
			    				:
			    			</td>
			    			<td width="40%">
			    				<b>Rp 0</b>
			    			</td>
			    		</tr>
			    	</table>
			
	
			    <table>
					<tr>
						<td width="65%">&nbsp;</td>
						<td width="15%" align="center">
							<p>Pejabat yang berwenang/<br>pejabat lain yang ditunjuk</p>
							<p>&nbsp;</p>
							<p><b><u><?= $model->getRelationField("paRelation","nama") ?></u></b><br>
							NIP. <?= $model->getRelationNip("paRelation") ?></p>
						</td>
					</tr>
				</table>