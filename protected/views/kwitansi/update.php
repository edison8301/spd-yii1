<?php
$this->breadcrumbs=array(
	'Kwitansis'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Kwitansi','url'=>array('index')),
	array('label'=>'Create Kwitansi','url'=>array('create')),
	array('label'=>'View Kwitansi','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Kwitansi','url'=>array('admin')),
	);
	?>

<h1>Tanda Bukti Pembayaran</h1>

<div class="row-fluid">
	<div class="span3">
		<?php echo $this->renderPartial('_form', array(
				'model'=>$model,
				'akomodasi'=>$akomodasi,
				'refresentatif'=>$refresentatif,
				'uangHarian'=>$uangHarian
		)); ?>
	</div>
	<div class="span8">
		<div style="overflow:auto">
		<?php echo $this->renderPartial('_admin', array('kwitansi'=>$kwitansi)); ?>
		</div>
	</div>
</div>


