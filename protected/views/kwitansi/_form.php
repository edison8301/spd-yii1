<?php

Yii::app()->clientScript->registerScript("hitungTotalUangHarian","

		function hitungTotalUangHarian()
		{
			
		}

");

?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'kwitansi-form',
	'enableAjaxValidation'=>false,
)); ?>
<script type="text/javascript">
	$(document).ready(function(){
		
		
		
		$(".format-money").on("keyup", function(){
			var _this = $(this);
			var value = _this.val().replace(/\.| /g,"");
			_this.val(accounting.formatMoney(value, "", 0, ".", ","))
		});	
		
		$(".format-money").trigger("keyup");
		
		$("#Kwitansi_uang_harian").on("keyup",function() {
			var lama = $('#lama').val();
			var uang_harian = $('#Kwitansi_uang_harian').val().replace('.',"");
			var total_uang_harian = lama*uang_harian;
			$('#total_uang_harian').val(total_uang_harian);
			$('#total_uang_harian').trigger("keyup");
		});
	});
</script>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>


<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nomor',array('class'=>'span9','maxlength'=>255,'placeholder'=>'Nomor Kwitansi')); ?>
	
	<?php print $form->hiddenField($model,'penerima'); ?>
	
	<?php print CHtml::label('Pilih Penerima',''); ?>
	<?php print CHtml::textField('nama_penerima',$model->getRelationField("penerimaRelation","nama")!='' ? $model->getRelationField("penerimaRelation","nama") : '',array('readonly'=>'readonly','class'=>'span8','placeholder'=>'Pilih Penerima')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'[..]',
            'type'=>'danger', 
            'htmlOptions'=>array(
				'onclick'=>'$("#dialogPenerima").dialog("open"); return false;',
				'style'=>'margin-bottom:10px',
            ),
    )); ?>
	
	<?php print $form->hiddenField($model,'id_spd'); ?>
	
	
	
	<?php print CHtml::label('Nomor SPD',''); ?>
	<?php print CHtml::textField('nomor_spd',$model->getRelationField("spd","nomor_spd") != '' ? $model->getRelationField("spd","nomor_spd") : '',array('readonly'=>'readonly','class'=>'span8','placeholder'=>'Pilih SPD')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'[..]',
            'type'=>'danger', 
            'htmlOptions'=>array(
				'onclick'=>'$("#dialogSpd").dialog("open"); return false;',
				'style'=>'margin-bottom:10px',
            ),
    )); ?>
	
	<?php print CHtml::label('Koring - Jenis Perjalanan Dinas',''); ?>
	<?php print CHtml::textField('koring_jenis_pd',$model->getRelationField("spd","koring") != '' ? $model->getRelationField("spd","koring") : '',array('readonly'=>'readonly')); ?>
	
	<?php print CHtml::label('Lama (Hari)',''); ?>
	<?php print CHtml::textField('lama',$model->getRelationField("spd","lama") != '' ? $model->getRelationField("spd","lama") : '',array('readonly'=>'readonly')); ?>
	
	<?php echo $form->textFieldRow($model,'uang_harian',array('class'=>'span10 format-money','maxlength'=>11,'prepend'=>'Rp','placeholder'=>'Uang Harian')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'[..]',
            'type'=>'danger', 
            'htmlOptions'=>array(
				'onClick'=>'$("#dialogUangHarian").dialog("open"); return false;',
				'value'=>'add',
				'style'=>'margin-bottom:10px',
            ),
    )); ?>
	
	<?php print CHtml::label('Total Uang Harian',''); ?>
	<?php print CHtml::textField('total_uang_harian',$model->getRelationField("spd","lama") != '' ? $model->getRelationField("spd","lama")*$model->uang_harian : '',array('readonly'=>'readonly','class'=>'format-money')); ?>
	
	<?php echo $form->textFieldRow($model,'akomodasi',array('class'=>'span10 format-money','maxlength'=>11,'prepend'=>'Rp','placeholder'=>'Biaya Akomodasi')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'[..]',
            'type'=>'danger', 
            'htmlOptions'=>array(
				'onclick'=>'$("#akomodasiDialog").dialog("open"); return false;',
				'style'=>'margin-bottom:10px',
            ),
    )); ?>

	<?php echo $form->textFieldRow($model,'refresentatif',array('class'=>'span10 format-money','maxlength'=>11,'prepend'=>'Rp','placeholder'=>'Biaya Refresentatif')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'[..]',
            'type'=>'danger', 
            'htmlOptions'=>array(
				'onClick'=>'$("#refresentatifDialog").dialog("open"); return false;',
				'style'=>'margin-bottom:10px',
            ),
    )); ?>
	
	

	
	<?php echo $form->textFieldRow($model,'bbm',array('class'=>'span12 format-money','maxlength'=>255,'prepend'=>'Rp','placeholder'=>'Biaya BBM')); ?>

	<?php echo $form->textFieldRow($model,'tol',array('class'=>'span12 format-money','maxlength'=>255,'prepend'=>'Rp','placeholder'=>'Biaya TOL')); ?>

	<?php echo $form->textFieldRow($model,'tiket',array('class'=>'span12 format-money','maxlength'=>255,'prepend'=>'Rp','placeholder'=>'Biaya Tiket Pesawat')); ?>
	
	<?php print $form->hiddenField($model,'bendahara'); ?>
	<?php print CHtml::label('Pilih Bendahara',''); ?>
	<?php print CHtml::textField('nama_bendahara',$model->getRelationField("bendaharaRelation","lama") != '' ? $model->getRelationField("bendaharaRelation","nama") : '',array('readonly'=>'readonly','class'=>'span8','placeholder'=>'Pilih Pegawai')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'[..]',
            'type'=>'danger', 
            'htmlOptions'=>array(
				'onclick'=>'$("#dialogBendahara").dialog("open"); return false;',
				'style'=>'margin-bottom:10px',
            ),
    )); ?>
	
	
	<?php print $form->hiddenField($model,'pptk'); ?>
	<?php print CHtml::label('Pilih PPTK',''); ?>
	<?php print CHtml::textField('nama_pptk',$model->getRelationField("pptkRelation","nama") != '' ? $model->getRelationField("pptkRelation","nama") : '',array('readonly'=>'readonly','class'=>'span8','placeholder'=>'Pilih Pegawai')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'[..]',
            'type'=>'danger', 
            'htmlOptions'=>array(
				'onclick'=>'$("#dialogPptk").dialog("open"); return false;',
				'style'=>'margin-bottom:10px',
            ),
    )); ?>
	
	
	<?php print $form->hiddenField($model,'pa'); ?>
	<?php print CHtml::label('Pilih PA/KPA',''); ?>
	<?php print CHtml::textField('nama_pa',$model->getRelationField("paRelation","nama") != '' ? $model->getRelationField("paRelation","nama") : '',array('readonly'=>'readonly','class'=>'span8','placeholder'=>'Pilih Pegawai')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'[..]',
            'type'=>'danger', 
            'htmlOptions'=>array(
				'onclick'=>'$("#dialogPa").dialog("open"); return false;',
				'style'=>'margin-bottom:10px',
            ),
    )); ?>
	
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); ?>
		<?php if(!$model->isNewRecord) { ?>
		<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','type'=>'primary','label'=>'Cetak','icon'=>'print white','url'=>array('kwitansi/view','id'=>$model->id))); ?>
		<?php } ?>
</div>
<?php $this->endWidget(); ?>

<?php //Dialog SPD
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogSpd',
    'options'=>array(
        'title'=>'Pilih SPD',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	$spd = new Spd;
	$this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'spd-grid',
		'type'=> 'striped bordered',
		'dataProvider'=>$spd->dataSpd(),
		'filter'=>$spd,
		'columns'=>array(
			array(
				'class'=>'CDataColumn',
				'type'=>'raw',
				'header'=>'Cetak',
				'value'=>'CHtml::link("<center><i class=icon-print ></i></center>",array("spd/viewSpd","id"=>"$data->id"))',
			),
			'nomor_spd',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_spd',
				'header'=>'Tanggal SPD',
				'type'=>'raw',
				'value'=>'Bantu::tgl($data->tanggal_spd)',
				//'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tanggal_spd)',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pejabat',
				'header'=>'Penandatangan',
				'type'=>'raw',
				'value'=>'$data->getRelationField("pejabat","nama")',
				'filter'=>CHtml::listData(Pejabat::model()->findAll(),'id','nama')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pegawai',
				'header'=>'Pegawai',
				'type'=>'raw',
				'value'=>'$data->getRelationField("pegawai","nama")',
				'filter'=>CHtml::listData(Pegawai::model()->findAll(),'id','nama')
			),
			'maksud',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#dialogSpd\").dialog(\"close\");
								$(\"#Kwitansi_lama\").val(\"$data->lama\");
								$(\"#Kwitansi_id_spd\").val(\"$data->id\");
							    $(\"#nomor_spd\").val(\"$data->nomor_spd\");
								$(\"#koring_jenis_pd\").val(\"$data->koring\");
								$(\"#lama\").val(\"$data->lama\");
								$(\"#Kwitansi_uang_harian\").trigger(\"keyup\");
								$(\".format-money\").trigger(\"keyup\");
				"))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
		),
));

	
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer
?>


<?php //Dialog Penerima
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogPenerima',
    'options'=>array(
        'title'=>'Pilih Penerima',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	$penerima = new Pegawai;
	$this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'penerima-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$penerima->search(),
		'filter'=>$penerima,
		'columns'=>array(
			'nip',
			'nama',
			array(
				'class'=>'CDataColumn',
				'name'=>'id_golongan',
				'header'=>'Golongan',
				'type'=>'raw',
				'value'=>'$data->getRelationField("golongan","nama")',
				'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
			),
			'jabatan',
			array(
				'class'=>'CDataColumn',
				'name'=>'tgl_lahir',
				'header'=>'Tanggal Lahir',
				'type'=>'raw',
				'value'=>'Bantu::tgl($data->tgl_lahir)',
				//'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_lahir)',
			),
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#dialogPenerima\").dialog(\"close\");
							    $(\"#Kwitansi_penerima\").val(\"$data->id\");
								$(\"#nama_penerima\").val(\"$data->nama\");",
					"class"=>"btn-primary"
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
					
				),
			),
		),
));
	
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer
?>

<?php //Dialog Bendahara
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogBendahara',
    'options'=>array(
        'title'=>'Pilih Bendahara',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	$bendahara = new Pejabat;
	$this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'bendahara-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$bendahara->search(),
		'filter'=>$bendahara,
		'columns'=>array(
			'nip',
			'nama',
			'jabatan',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#dialogBendahara\").dialog(\"close\");
							    $(\"#Kwitansi_bendahara\").val(\"$data->id\");
								$(\"#nama_bendahara\").val(\"$data->nama\");",
					"class"=>"btn-primary"
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
					
				),
			),
		),
));
	
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer
?>

<?php //Dialog PPTK
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogPptk',
    'options'=>array(
        'title'=>'Pilih PPTK',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	$pptk = new Pejabat;
	$this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'pptk-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$pptk->search(),
		'filter'=>$pptk,
		'columns'=>array(
			'nip',
			'nama',
			'jabatan',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#dialogPptk\").dialog(\"close\");
							    $(\"#Kwitansi_pptk\").val(\"$data->id\");
								$(\"#nama_pptk\").val(\"$data->nama\");",
					"class"=>"btn-primary"
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
					
				),
			),
		),
));
	
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer
?>

<?php //Dialog PA
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogPa',
    'options'=>array(
        'title'=>'Pilih PA/KPA',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	$pa = new Pejabat;
	$this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'pa-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$pa->search(),
		'filter'=>$pa,
		'columns'=>array(
			'nip',
			'nama',
			'jabatan',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#dialogPa\").dialog(\"close\");
							    $(\"#Kwitansi_pa\").val(\"$data->id\");
								$(\"#nama_pa\").val(\"$data->nama\");",
					"class"=>"btn-primary"
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
					
				),
			),
		),
));
	
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer
?>


<?php //Dialog Akomodasi
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'akomodasiDialog',
    'options'=>array(
        'title'=>'Pilih Akomodasi',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	
	$this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'akomodasi-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$akomodasi->search(),
		'filter'=>$akomodasi,
		'columns'=>array(
			'jarak',
			array(
				'class'=>'CDataColumn',
				'name'=>'id_golongan',
				'header'=>'Golongan',
				'type'=>'raw',
				'value'=>'$data->getRelationField("golongan","nama")',
				'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
			),
			'biaya',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#akomodasiDialog\").dialog(\"close\");
							    $(\"#Kwitansi_akomodasi\").val(\"$data->biaya\");
								$(\".format-money\").trigger(\"keyup\");
				"))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
		),
	));
	
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer
?>

<?php //Refresentatif Dialog
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'refresentatifDialog',
    'options'=>array(
        'title'=>'Pilih Refresentatif',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	
	$this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'refresentatif-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$refresentatif->search(),
		'filter'=>$refresentatif,
		'columns'=>array(
			'jarak',
			'biaya',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#refresentatifDialog\").dialog(\"close\");
							    $(\"#Kwitansi_refresentatif\").val(\"$data->biaya\");
                                $(\".format-money\").trigger(\"keyup\");								
				"))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
		),
	));
	
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer
?>

<?php //Uang Harian
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogUangHarian',
    'options'=>array(
        'title'=>'Pilih Uang Harian',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	
	$this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'uang-harian-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$uangHarian->search(),
		'filter'=>$uangHarian,
		'columns'=>array(
			'jarak',
			array(
				'class'=>'CDataColumn',
				'name'=>'id_golongan',
				'header'=>'Golongan',
				'type'=>'raw',
				'value'=>'$data->golongan->nama',
				'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
			),
			'biaya',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#dialogUangHarian\").dialog(\"close\");
							    $(\"#Kwitansi_uang_harian\").val(\"$data->biaya\");
								$(\"#Kwitansi_uang_harian\").trigger(\"keyup\");
								$(\".format-money\").trigger(\"keyup\");
				"))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
		),
	));
	
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer
?>
