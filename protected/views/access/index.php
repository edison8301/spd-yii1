<?php
$this->breadcrumbs=array(
	'Accesses',
);

$this->menu=array(
array('label'=>'Create Access','url'=>array('create')),
array('label'=>'Manage Access','url'=>array('admin')),
);
?>

<h1>Accesses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
