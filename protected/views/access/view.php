<?php
$this->breadcrumbs=array(
	'Accesses'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Access','url'=>array('index')),
array('label'=>'Create Access','url'=>array('create')),
array('label'=>'Update Access','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Access','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Access','url'=>array('admin')),
);
?>

<h1>View Access #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbButton',array(
			'label' => 'Tambah Access',
			'icon'=>'plus white',
			'type' => 'primary',
			'url'=>array('access/create'),
		)
	);
?>


<?php $this->widget('bootstrap.widgets.TbButton',array(
			'label' => 'Kelola Access',
			'icon'=>'plus white',
			'type' => 'primary',
			'url'=>array('access/admin'),
		)
	);
?>

<div>&nbsp;</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nama_controller',
		'nama_action',
),
)); ?>
