<?php
$this->breadcrumbs=array(
	'Refresentatifs'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Refresentatif','url'=>array('index')),
array('label'=>'Manage Refresentatif','url'=>array('admin')),
);
?>

<h1>Create Refresentatif</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>