<?php
$this->breadcrumbs=array(
	'Refresentatifs'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Refresentatif','url'=>array('index')),
array('label'=>'Create Refresentatif','url'=>array('create')),
array('label'=>'Update Refresentatif','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Refresentatif','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Refresentatif','url'=>array('admin')),
);
?>

<h1>Detail Refresentatif</h1>

<?php
            $this->widget('bootstrap.widgets.TbButtonGroup', array(
            //'size'=>'small',
            'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'buttons'=>array(
            array('label'=>'Menu', 'items'=>array(
					array('label'=>'Tambah Refresentatif', 'icon'=>'plus', 'url'=>array('refresentatif/create')),
                    array('label'=>'Sunting Refresentatif', 'icon'=>'plus', 'url'=>array('refresentatif/update','id'=>$model->id)),
                )),
            ),
            ));
?>
<div> &nbsp; </div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'jarak',
		'biaya',
),
)); ?>
