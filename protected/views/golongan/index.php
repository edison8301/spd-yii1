<?php
$this->breadcrumbs=array(
	'Golongan',
);

$this->menu=array(
array('label'=>'Create Golongan','url'=>array('create')),
array('label'=>'Manage Golongan','url'=>array('admin')),
);
?>

<h1>Golongan</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
