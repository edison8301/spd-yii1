<?php
$this->breadcrumbs=array(
	'Golongan'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Golongan','url'=>array('index')),
array('label'=>'Manage Golongan','url'=>array('admin')),
);
?>

<h1>Tambah Golongan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>