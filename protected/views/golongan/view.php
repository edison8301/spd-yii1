<?php
$this->breadcrumbs=array(
	'Golongan'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Golongan','url'=>array('index')),
array('label'=>'Create Golongan','url'=>array('create')),
array('label'=>'Update Golongan','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Golongan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Golongan','url'=>array('admin')),
);
?>

<h1>Detail Golongan</h1>

<?php
            $this->widget('bootstrap.widgets.TbButtonGroup', array(
            //'size'=>'small',
            'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'buttons'=>array(
            array('label'=>'Menu', 'items'=>array(
					array('label'=>'Tambah Golongan', 'icon'=>'plus', 'url'=>array('golongan/create')),
                    array('label'=>'Sunting Golongan', 'icon'=>'plus', 'url'=>array('golongan/update','id'=>$model->id)),
                )),
            ),
            ));
?>
<div> &nbsp; </div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nama',
),
)); ?>
