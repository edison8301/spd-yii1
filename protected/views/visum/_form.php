<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'visum-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nomor',array('class'=>'span3','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'nip',array('class'=>'span4','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'jabatan',array('class'=>'span4','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'instansi',array('class'=>'span5','maxlength'=>255)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Simpan',
			'icon'=>'ok white',
		)); ?>
</div>

<?php $this->endWidget(); ?>
