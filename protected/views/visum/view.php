<?php
$this->breadcrumbs=array(
	'Visums'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Visum','url'=>array('index')),
array('label'=>'Create Visum','url'=>array('create')),
array('label'=>'Update Visum','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Visum','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Visum','url'=>array('admin')),
);
?>

<h1>View Visum #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nomor',
		'nip',
		'nama',
		'jabatan',
		'instansi',
),
)); ?>
