<?php
$this->breadcrumbs=array(
	'Visums',
);

$this->menu=array(
array('label'=>'Create Visum','url'=>array('create')),
array('label'=>'Manage Visum','url'=>array('admin')),
);
?>

<h1>Visums</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
