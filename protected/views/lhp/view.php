<?php
$this->breadcrumbs=array(
	'Lhps'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Lhp','url'=>array('index')),
array('label'=>'Create Lhp','url'=>array('create')),
array('label'=>'Update Lhp','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Lhp','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Lhp','url'=>array('admin')),
);
?>

<h1>Detail LHP</h1>

<?php $this->widget('bootstrap.widgets.TbButton',array(
		'label' => 'LHP kop SETDA',
		'icon'=>'download-alt white',
		'type' => 'primary',
		'url'=>array('lhp/cetakPdf','id'=>$model->id,'kop'=>'setda'),
		'htmlOptions'=>array('target'=>'_blank')
)); ?> 

<?php $this->widget('bootstrap.widgets.TbButton',array(
		'label' => 'LHP kop SETDA Non Pengikut',
		'icon'=>'download-alt white',
		'type' => 'primary',
		'url'=>array('lhp/cetakPdf','id'=>$model->id,'kop'=>'setda','nonPengikut'=>1),
		'htmlOptions'=>array('target'=>'_blank')
)); ?> 

<?php $this->widget('bootstrap.widgets.TbButton',array(
		'label' => 'LHP kop DPKD',
		'icon'=>'download-alt white',
		'type' => 'primary',
		'url'=>array('lhp/cetakPdf','id'=>$model->id,'kop'=>'dpkd'),
		'htmlOptions'=>array('target'=>'_blank')
)); ?>

<?php $this->widget('bootstrap.widgets.TbButton',array(
		'label' => 'LHP kop DPKD Non Pengikut',
		'icon'=>'download-alt white',
		'type' => 'primary',
		'url'=>array('lhp/cetakPdf','id'=>$model->id,'kop'=>'dpkd','nonPengikut'=>1),
		'htmlOptions'=>array('target'=>'_blank')
)); ?>

<div>&nbsp;</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'nomor',
			array(
				'label'=>'No. SPD',
				'value'=>$model->getRelationField('spd','nomor_spd')
			),
			'kepada',
			'dari',
			'tanggal',
			'hal',
			'kesimpulan',
		),
)); ?>

<?php $this->widget('bootstrap.widgets.TbButton',array(
		'label' => 'Input LHP',
		'icon'=>'plus white',
		'type' => 'primary',
		'url'=>array('lhp/create'),
)); ?>

<?php $this->widget('bootstrap.widgets.TbButton',array(
		'label' => 'Sunting LHP',
		'icon'=>'pencil white',
		'type' => 'primary',
		'url'=>array('lhp/update','id'=>$model->id),
)); ?>

