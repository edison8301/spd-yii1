<table border="<?php print $border; ?>" class="table">
<tr>
	<th>NOMOR LHP</th>
	<th>TANGGAL</th>
	<th>KEPADA</th>
	<th>DARI</th>
	<th>NOMOR SPD</th>
	<th>HAL</th>
	<th>KESIMPULAN</th>
</tr>
<?php if(isset($_GET['tampil'])) { ?>
<?php
		$criteria = new CDbCriteria;
		$criteria->order = 'tanggal ASC';
		
		if(!empty($_GET['tanggal_awal']) AND !empty($_GET['tanggal_akhir']))
		{
			$criteria->condition = 'tanggal >= :tanggal_awal AND tanggal <= :tanggal_akhir';
			$criteria->params = array(':tanggal_awal'=>$_GET['tanggal_awal'],':tanggal_akhir'=>$_GET['tanggal_akhir']);
		}
?>

<?php $i=1; foreach(LHP::model()->findAll($criteria) as $data) { ?>
<tr>
	<td><?php print $data->nomor; ?></td>
	<td><?php print $data->tanggal; ?></td>
	<td><?php print $data->kepada; ?></td>
	<td><?php print $data->dari; ?></td>
	<td><?php print $data->getRelationField("spd","nomor_spd"); ?></td>
	<td><?php print $data->hal; ?></td>
	<td><?php print $data->kesimpulan; ?></td>
</tr>
<?php $i++; } ?>
<?php } ?>
</table>