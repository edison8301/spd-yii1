<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'lhp-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nomor',array('class'=>'span3','maxlength'=>255)); ?>

	<?php echo $form->dropDownListRow($model,'kepada',array('Wali Kota Serang'=>'Wali Kota Serang', 
															'Wakil Wali Kota Serang'=>'Wakil Wali Kota Serang', 
															'Sekertaris Daerah'=>'Sekertaris Daerah', 
															'Plt. Sekertaris Daerah'=>'Plt. Sekertaris Daerah', 
															'Kepala DPKD Kota Serang'=>'Kepala DPKD Kota Serang', 
															'Sekertaris DPKD Kota Serang'=>'Sekertaris DPKD Kota Serang',
															'Kepala Bidang Anggaran DPKD Kota Serang'=>'Kepala Bidang Anggaran DPKD Kota Serang',
															'Kepala Bidang Perbendaharaan DPKD Kota Serang'=>'Kepala Bidang Perbendaharaan DPKD Kota Serang',
															'Kepala Bidang Akuntansi DPKD Kota Serang'=>'Kepala Bidang Akuntansi DPKD Kota Serang',
															'Kepala Bidang Pendapatan DPKD Kota Serang'=>'Kepala Bidang Pendapatan DPKD Kota Serang',
															'Kasubag Umum dan Kepegawian DPKD Kota Serang'=>'Kasubag Umum dan Kepegawian DPKD Kota Serang',
															'Kasubag Evaluasi dan Pelaporan DPKD Kota Serang'=>'Kasubag Evaluasi dan Pelaporan DPKD Kota Serang',
															'Kasubag Keuangan DPKD Kota Serang'=>'Kasubag Keuangan DPKD Kota Serang',
															'Kasi Belanja Tidak Langsung DPKD Kota Serang'=>'Kasi Belanja Tidak Langsung DPKD Kota Serang',
															'Kasi Kas Daerah DPKD Kota Serang'=>'Kasi Kas Daerah DPKD Kota Serang',
															'Kasi Belanja Langsung DPKD Kota Serang'=>'Kasi Belanja Langsung DPKD Kota Serang',
															'Kasi Pelaporan DPKD Kota Serang'=>'Kasi Pelaporan DPKD Kota Serang',
															'Kasi Penerimaan DPKD Kota Serang'=>'Kasi Penerimaan DPKD Kota Serang',
															'Kasi Pengeluaran DPKD Kota Serang'=>'Kasi Pengeluaran DPKD Kota Serang',
															'Kasi Penyusunan Anggaran'=>'Kasi Penyusunan Anggaran',
															'Kasi Evaluasi dan Dokumentasi Anggaran DPKD Kota Serang'=>'Kasi Evaluasi dan Dokumentasi Anggaran DPKD Kota Serang',
															'Kasi Kebijakan dan Perumusan Anggaran DPKD Kota Serang'=>'Kasi Kebijakan dan Perumusan Anggaran DPKD Kota Serang',
															'Kasi Penetapan dan Penagihan DPKD Kota Serang'=>'Kasi Penetapan dan Penagihan DPKD Kota Serang',
															'Kasi Pendapatan dan Sumber-sumber Lain DPKD Kota Serang'=>'Kasi Pendapatan dan Sumber-sumber Lain DPKD Kota Serang',
															'Kasi Pendapatan dan Pendaftaran DPKD Kota Serang'=>'Kasi Pendapatan dan Pendaftaran DPKD Kota Serang',
															'Kepala UPT PBB-P2 Kec.Serang'=>'Kepala UPT PBB-P2 Kec.Serang',
															'Kepala UPT PBB-P2 Kec.Cipocok Jaya'=>'Kepala UPT PBB-P2 Kec.Cipocok Jaya',
															'Kasubag TU UPTD PBB-P2 Kec.Serang'=>'Kasubag TU UPTD PBB-P2 Kec.Serang',
															'Kasubag TU UPTD PBB-P2 Kec.Cipocok Jaya'=>'Kasubag TU UPTD PBB-P2 Kec.Cipocok Jaya',
															'Pelaksana'=>'Pelaksana'),array('empty'=>'-- Pilih Kepada --')); ?>

	<div class="row-fluid">
		<div class="span3">
			<?php echo $form->dropDownListRow($model,'dari',array('Wali Kota Serang'=>'Wali Kota Serang', 
															'Wakil Wali Kota Serang'=>'Wakil Wali Kota Serang', 
															'Sekertaris Daerah'=>'Sekertaris Daerah', 
															'Plt. Sekertaris Daerah'=>'Plt. Sekertaris Daerah', 
															'Kepala DPKD Kota Serang'=>'Kepala DPKD Kota Serang', 
															'Sekertaris DPKD Kota Serang'=>'Sekertaris DPKD Kota Serang',
															'Kepala Bidang Anggaran DPKD Kota Serang'=>'Kepala Bidang Anggaran DPKD Kota Serang',
															'Kepala Bidang Perbendaharaan DPKD Kota Serang'=>'Kepala Bidang Perbendaharaan DPKD Kota Serang',
															'Kepala Bidang Akuntansi DPKD Kota Serang'=>'Kepala Bidang Akuntansi DPKD Kota Serang',
															'Kepala Bidang Pendapatan DPKD Kota Serang'=>'Kepala Bidang Pendapatan DPKD Kota Serang',
															'Kasubag Umum dan Kepegawian DPKD Kota Serang'=>'Kasubag Umum dan Kepegawian DPKD Kota Serang',
															'Kasubag Evaluasi dan Pelaporan DPKD Kota Serang'=>'Kasubag Evaluasi dan Pelaporan DPKD Kota Serang',
															'Kasubag Keuangan DPKD Kota Serang'=>'Kasubag Keuangan DPKD Kota Serang',
															'Kasi Belanja Tidak Langsung DPKD Kota Serang'=>'Kasi Belanja Tidak Langsung DPKD Kota Serang',
															'Kasi Kas Daerah DPKD Kota Serang'=>'Kasi Kas Daerah DPKD Kota Serang',
															'Kasi Belanja Langsung DPKD Kota Serang'=>'Kasi Belanja Langsung DPKD Kota Serang',
															'Kasi Pelaporan DPKD Kota Serang'=>'Kasi Pelaporan DPKD Kota Serang',
															'Kasi Penerimaan DPKD Kota Serang'=>'Kasi Penerimaan DPKD Kota Serang',
															'Kasi Pengeluaran DPKD Kota Serang'=>'Kasi Pengeluaran DPKD Kota Serang',
															'Kasi Penyusunan Anggaran'=>'Kasi Penyusunan Anggaran',
															'Kasi Evaluasi dan Dokumentasi Anggaran DPKD Kota Serang'=>'Kasi Evaluasi dan Dokumentasi Anggaran DPKD Kota Serang',
															'Kasi Kebijakan dan Perumusan Anggaran DPKD Kota Serang'=>'Kasi Kebijakan dan Perumusan Anggaran DPKD Kota Serang',
															'Kasi Penetapan dan Penagihan DPKD Kota Serang'=>'Kasi Penetapan dan Penagihan DPKD Kota Serang',
															'Kasi Pendapatan dan Sumber-sumber Lain DPKD Kota Serang'=>'Kasi Pendapatan dan Sumber-sumber Lain DPKD Kota Serang',
															'Kasi Pendapatan dan Pendaftaran DPKD Kota Serang'=>'Kasi Pendapatan dan Pendaftaran DPKD Kota Serang',
															'Kepala UPT PBB-P2 Kec.Serang'=>'Kepala UPT PBB-P2 Kec.Serang',
															'Kepala UPT PBB-P2 Kec.Cipocok Jaya'=>'Kepala UPT PBB-P2 Kec.Cipocok Jaya',
															'Kasubag TU UPTD PBB-P2 Kec.Serang'=>'Kasubag TU UPTD PBB-P2 Kec.Serang',
															'Kasubag TU UPTD PBB-P2 Kec.Cipocok Jaya'=>'Kasubag TU UPTD PBB-P2 Kec.Cipocok Jaya',
															'Pelaksana'=>'Pelaksana'),array('empty'=>'-- Pilih Dari --')); ?>

			<?php echo $form->textFieldRow($model,'hal',array('class'=>'span12','maxlength'=>255)); ?>
		</div>
		<div class="span5">
			<div class="control-group ">
				<label class="control-label required" for="Lhp_tanggal">
					Tanggal
				</label>
				<div class="controls">
					<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
						'name' => 'Lhp[tanggal]',
						'id'=>'Lhp_tanggal',
						'language' => 'id',
						'model' => $model,
						'value'=>$model->tanggal,
						// additional javascript options for the date picker plugin
						'options'=>array(
						    'showAnim'=>'fold',
						    'showOn'=>'button',
						    'buttonImage'=>Yii::app()->baseUrl.'/img/calendar.png',
						    'dateFormat'=>'yy-mm-dd',
						    'changeMonth' => 'false',
						    'showButtonPanel' => 'false',
						    'changeYear'=>'false',
						    'constrainInput' => 'false',
						),
						'htmlOptions'=>array(
						    'style'=>'height:20px;width:150px; margin-bottom:0px',
						    'readonly'=>true,
						),
					));?>
				</div>
			</div>

			<?php print $form->hiddenField($model,'id_spd'); ?>
	
	
			<?php print CHtml::label('Nomor SPD',''); ?>
			<?php print CHtml::textField('nomor_spd',$model->getRelationField("spd","nomor_spd") != '' ? $model->getRelationField("spd","nomor_spd") : '',array('readonly'=>'readonly','class'=>'span5','placeholder'=>'Pilih SPD')); ?>
			<?php $this->widget('bootstrap.widgets.TbButton', array(
					'label'=>'[..]',
					'type'=>'danger', 
					'htmlOptions'=>array(
						'onclick'=>'$("#dialogSpd").dialog("open"); return false;',
						'style'=>'margin-bottom:10px',
					),
			)); ?>
		</div>
	</div>
	
	<?php echo $form->textAreaRow($model,'kesimpulan',array('class'=>'span5','maxlength'=>255)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
	)); ?>
	
	<?php if(!$model->isNewRecord) { ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'link',
			'type'=>'primary',
			'icon'=>'print white',
			'label'=>'Cetak',
			'url'=>array('lhp/view','id'=>$model->id)
	)); ?>
	<?php } ?>
</div>

<?php $this->endWidget(); ?>


<?php //Dialog SPD
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogSpd',
    'options'=>array(
        'title'=>'Pilih SPD',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	$spd = new Spd;
	$this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'spd-grid',
		'type'=> 'striped bordered',
		'dataProvider'=>$spd->dataSpd(),
		'filter'=>$spd,
		'columns'=>array(
			array(
				'class'=>'CDataColumn',
				'type'=>'raw',
				'header'=>'Cetak',
				'value'=>'CHtml::link("<center><i class=icon-print ></i></center>",array("spd/viewSpd","id"=>"$data->id"))',
			),
			'nomor_spd',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_spd',
				'header'=>'Tanggal SPD',
				'type'=>'raw',
				'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tanggal_spd)',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pejabat',
				'header'=>'Penandatangan',
				'type'=>'raw',
				'value'=>'$data->getRelationField("pejabat","nama")',
				'filter'=>CHtml::listData(Pejabat::model()->findAll(),'id','nama')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pegawai',
				'header'=>'Pegawai',
				'type'=>'raw',
				'value'=>'$data->getRelationField("pegawai","nama")',
				'filter'=>CHtml::listData(Pegawai::model()->findAll(),'id','nama')
			),
			'maksud',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#dialogSpd\").dialog(\"close\");
								$(\"#Kwitansi_lama\").val(\"$data->lama\");
								$(\"#Kwitansi_id_spd\").val(\"$data->id\");
							    $(\"#nomor_spd\").val(\"$data->nomor_spd\");
								$(\"#Lhp_id_spd\").val(\"$data->id\");
				"))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
		),
));

	
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer
?>
