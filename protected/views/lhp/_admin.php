<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'lhp-grid',
		'type'=> 'striped bordered',
		'dataProvider'=>$lhp->search(),
		'filter'=>$lhp,
		'columns'=>array(
			array(
				'class'=>'CDataColumn',
				'type'=>'raw',
				'header'=>'Cetak',
				'value'=>'CHtml::link("<center><i class=icon-print ></i></center>",array("lhp/view","id"=>"$data->id"))',
			),
			'nomor',
			array(
				'class'=>'CDataColumn',
				'name'=>'id_spd',
				'header'=>'No. SPD',
				'type'=>'raw',
				'value'=>'$data->getRelationField("spd","nomor_spd")',
				'filter'=>CHtml::listData(Spd::model()->findAll(),'id','nomor')
			),
			'kepada',
			'dari',	
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal',
				'header'=>'Tanggal',
				'type'=>'raw',
				'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tanggal)',
			),
			'hal',
			'kesimpulan',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}{delete}'
			),
		),
)); ?>