<?php
$this->breadcrumbs=array(
	'Lhps'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Lhp','url'=>array('index')),
array('label'=>'Create Lhp','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('lhp-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Cetak Laporan Hasil Perjalanan Dinas</h1>

<div>Silahkan klik simbol printer <i class="icon-print"></i> untuk memilih data yang akan dicetak</div>

<?php $this->renderPartial('_admin',array('lhp'=>$model)); ?>