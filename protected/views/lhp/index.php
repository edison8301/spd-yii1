<?php
$this->breadcrumbs=array(
	'Lhps',
);

$this->menu=array(
array('label'=>'Create Lhp','url'=>array('create')),
array('label'=>'Manage Lhp','url'=>array('admin')),
);
?>

<h1>Lhps</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
