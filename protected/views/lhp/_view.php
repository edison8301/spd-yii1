<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor')); ?>:</b>
	<?php echo CHtml::encode($data->nomor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kepada')); ?>:</b>
	<?php echo CHtml::encode($data->kepada); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dari')); ?>:</b>
	<?php echo CHtml::encode($data->dari); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hal')); ?>:</b>
	<?php echo CHtml::encode($data->hal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_spd')); ?>:</b>
	<?php echo CHtml::encode($data->id_spd); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('kesimpulan')); ?>:</b>
	<?php echo CHtml::encode($data->kesimpulan); ?>
	<br />

	*/ ?>

</div>