<?php
$this->breadcrumbs=array(
	'Perjalanans'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Perjalanan','url'=>array('index')),
	array('label'=>'Create Perjalanan','url'=>array('create')),
	array('label'=>'View Perjalanan','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Perjalanan','url'=>array('admin')),
	);
	?>

	<h1>Update Perjalanan <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>