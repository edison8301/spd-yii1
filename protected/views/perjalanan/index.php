<?php
$this->breadcrumbs=array(
	'Perjalanans',
);

$this->menu=array(
array('label'=>'Create Perjalanan','url'=>array('create')),
array('label'=>'Manage Perjalanan','url'=>array('admin')),
);
?>
<?php /*<h1>Perjalanans</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
));*/ ?>
<h1>Kalender Perjalanan: <?php print date('F Y',strtotime(Bantu::getMonth())); ?></h1>

<div>&nbsp;</div>

<div style="overflow:auto">

<div style="margin-bottom:10px;">

<?php $this->widget('bootstrap.widgets.TbButton',array(
		'buttonType'=>'link',
		'type'=>'primary',
		'icon'=>'chevron-left white',
		'url'=>array('perjalanan/index','month'=>Bantu::getPrevMonth())
)); ?>

<?php $this->widget('bootstrap.widgets.TbButton',array(
		'buttonType'=>'link',
		'type'=>'primary',
		'icon'=>'chevron-right white',
		'url'=>array('perjalanan/index','month'=>Bantu::getNextMonth())
)); ?>

</div>


<table class="table table-bordered table-striped">
	<thead>	
		<tr>
			<?php $jumlahHari = date('t',strtotime(Bantu::getMonth())); ?>
			<th rowspan="2" style="background:#317EAC;color:#FFFFFF;font-weight:bold;vertical-align:middle;" class="lebarKolomNama" style="width:20%">Pegawai</th>
			<th colspan="<?php print $jumlahHari; ?>" style="background:#317EAC;color:#FFFFFF;font-weight:bold;vertical-align:middle;text-align:center"><?php print date('F Y',strtotime(Bantu::getMonth())); ?></th>
		</tr>
		<tr>
			<?php for($i=1;$i<=$jumlahHari;$i++) { ?>
			<?php
				$hari = Bantu::getHariSingkat(date('Y-m-d',strtotime(date('Y-m-j',strtotime(Bantu::getMonth().'-'.$i)))));
			?>
			<?php if($hari == "Sab"|| $hari == "Min") { ?>
			<th style="background:#C10000;color:#FFFFFF;font-weight:bold;text-align:center">
				<?php echo $i; ?><br><?php print $hari; ?>
			</th>
			<?php } else { ?>
			<th style="background:#317EAC;color:#FFFFFF;font-weight:bold;text-align:center">
				<?php echo $i; ?><br><?php print $hari; ?>
			</th>			
			<?php } ?>
			<?php } ?>
		</tr>
	</thead>
	<tbody>
		<?php
			$criteria = new CDbCriteria;
			$criteria->order = 'nama ASC';
		?>
		<?php foreach (Pegawai::model()->findAll($criteria) as $pegawai) { ?>
		<tr>
			<td style="background:#317EAC;color:#FFFFFF;font-weight:bold" class="lebarKolomNama"><?php echo $pegawai->nama; ?></td>
			
			<?php $tanggal = Bantu::getMonth().'-01'; ?>
			
			<?php for($i=1;$i<=$jumlahHari;$i++) {  ?>
				<?php $perjalanan = Perjalanan::model()->findByAttributes(array('id_pegawai'=>$pegawai->id,'tanggal'=>$tanggal,'aktif'=>1)); ?>
				<?php if($perjalanan !== null) { ?>
					<td style="background:#000000;text-align:center"><?php print CHtml::link('DL',array('spd/viewSpt','id'=>$perjalanan->id_spd),array('style'=>'color:#fff')); ?></td>
				<?php } else { ?>
					<td >&nbsp;</td>
				<?php } ?>
				<?php $tanggal = date('Y-m-d',strtotime(date("Y-m-d", strtotime($tanggal)) . " +1 day")); ?>
			<?php } ?>
		</tr>
		<?php } ?>
	</tbody>
</table>
</div>

 


 








<?php /*foreach (Pegawai::model()->findAll() as $pegawai) {?>
				<tr><td><?php echo $pegawai->nama; ?></td>	
			<?php
				$tanggal= date("t");
				$j=1;
				for ($i=1;$i<=$tanggal;$i++) { 
					foreach (Perjalanan::model()->findAllbyAttributes(array('id_pegawai'=>$pegawai->id)) as $perjalanan){
					 $A=substr($perjalanan->tanggal, 8,2);
					 if ($A==$j) ?>
						<td <?php { ?> style="background:red;" <?php } ?> ></td>
					<?php } ?>		
			<?php $j++; }?></tr><?php } */?>