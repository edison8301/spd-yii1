<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List User','url'=>array('index')),
array('label'=>'Manage User','url'=>array('admin')),
);
?>

<h1>Entry User</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<h2>Data User</h2>

<?php print $this->renderPartial('_admin',array('user'=>$user)); ?>

