<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List User','url'=>array('index')),
	array('label'=>'Create User','url'=>array('create')),
	array('label'=>'View User','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage User','url'=>array('admin')),
	);
	?>

	<h1>Sunting User <?php echo $model->username; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'user-grid',
'type'=>'striped bordered',
'dataProvider'=>$user->search(),
'filter'=>$user,
'columns'=>array(
		'username',
		'password',
		array(
			'class'=>'CDataColumn',
			'name'=>'role_id',
			'header'=>'Role',
			'type'=>'raw',
			'value'=>'$data->role->nama',
			'filter'=>CHtml::listData(Role::model()->findAll(),'id','nama')
		),
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
'template'=>'{update} {delete}'
),
),
)); ?>