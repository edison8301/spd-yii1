<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'user-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$user->search(),
		'filter'=>$user,
		'columns'=>array(
			'username',
			array(
				'class'=>'CDataColumn',
				'name'=>'role_id',
				'header'=>'Role',
				'type'=>'raw',
				'value'=>'$data->getRelationField("role","nama")',
				'filter'=>CHtml::listData(Role::model()->findAll(),'id','nama')
			),
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update} {delete}'
			),
		),
)); ?>