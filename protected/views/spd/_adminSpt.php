
<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'spd-grid',
		'type'=> 'striped bordered',
		'dataProvider'=>$spd->search(),
		'filter'=>$spd,
		'columns'=>array(
			array(
				'class'=>'CDataColumn',
				'type'=>'raw',
				'header'=>'Cetak',
				'value'=>'CHtml::link("<center><i class=icon-print ></i></center>",array("spd/viewSpt","id"=>"$data->id"))',
			),
			'nomor_spt',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_spt',
				'header'=>'Tanggal SPT',
				'type'=>'raw',
				'value'=>'Bantu::tgl($data->tanggal_spt)',
				//'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tanggal_spt)',
			),

			array(
				'class'=>'CDataColumn',
				'name'=>'id_pejabat',
				'header'=>'Penandatangan',
				'type'=>'raw',
				'value'=>'$data->getRelationField("pejabat","nama")',
				'filter'=>CHtml::listData(Pejabat::model()->findAll(),'id','nama')
			),
			
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pegawai',
				'header'=>'Pegawai',
				'type'=>'raw',
				'value'=>'$data->getRelationField("pegawai","nama")',
				'filter'=>CHtml::listData(Pegawai::model()->findAll(),'id','nama')
			),
			'tgl_pergi',
			'tgl_kembali',
			'tujuan',
			'maksud',
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'updateButtonUrl'=>'array("spd/updateSpt","id"=>$data->id)',
				'template'=>'{update}{delete}',
				'viewButtonUrl'=>'array("spd/viewSpt","id"=>$data->id)'
			),
			
		),
)); ?>