<?php
/*   public function actionExportDataSpd($id=null)
    {
	    $pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'Legal', true, 'UTF-8');
		// create new PDF document
		$pdf = new TCPDF('P', PDF_UNIT, 'F4', true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Surat Perjalanan Dinas');
		$pdf->SetTitle('Surat Perjalanan Dinas');
		$pdf->SetSubject('Surat Perjalanan Dinas');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// add a page
		$pdf->AddPage();
		$pdf->SetFont('times', '', 11);
		
		
		
		$html= */?>
		<p align="right"><b>Lampiran VI<br>Permenkeu RI No. 113/PMK 05/2012</b></p>
		<table border="1" style="padding:5px;line-height:95%" width="100%">
			
		<?php 	
		$i=1;
			if($id!=null) {
			foreach(Spd::model()->findAllbyPk($id) as $data)
			{
				//$pejabat=Pejabat::model()->findbyPk($data->id_pejabat);
				//$pegawai=Pegawai::model()->findbyPk($data->id_pegawai);
				//$golongan=Golongan::model()->findbyPk($pegawai->id_golongan);
			?>
			   
			 	<tr>
			 		<td width="50%" rowspan="2">&nbsp;</td>
			 		<td width="50%" >
			 			<table>
				 			<tr>
								<td style="width:5%">I.</td>
				 				<td style="width:40%">Berangkat dari<br>(Tempat Kedudukan)</td>
				 				<td>: <?= $data->tempat_berangkat ?></td>
				 			</tr>
				 			<tr>
								<td style="width:5%">&nbsp;</td>
				 				<td style="width:40%">Ke</td>
				 				<td>: <?= $data->tujuan ?></td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td style="width:40%"></td>
				 				<td></td>
				 			</tr>
				 			<tr>
								<td style="width:5%">&nbsp;</td>
				 				<td style="width:40%">Pada Tanggal</td>
				 				<td>: <?= date('d F Y', strtotime($data->tgl_pergi)); ?></td>
				 			</tr>
			 			</table>
			 		</td>
				</tr>
				<tr>
					<td width="50%" >
			 			<table>
				 			<tr>
				 				<td align="center" colspan="2"> <?= $data->getRelationField("pejabat","jabatan"); ?></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>				 							 							 			
				 			<tr>
				 				<td align="center" colspan="2"><p style="text-decoration:underline;"><b><?= $data->getRelationField("pejabat","nama"); ?></b></p></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2">NIP. <?= $data->getRelationNip("pejabat") ?></td>
				 			</tr>
			 			</table>
			 		</td>
			 	</tr>
				<tr>
					<td width="50%" style="line-height:95%">
						<table>
				 			<tr>
								<td style="width:5%">II.</td>
				 				<td>Tiba di</td>
				 				<td>: <?= $data->tujuan; ?></td>
				 			</tr>
				 			<tr>
								<td style="width:5%"></td>
				 				<td>Pada Tanggal</td>
				 				<td>: <?= date('d F Y', strtotime($data->tgl_pergi)) ?></td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
				 			<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
				 			<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
			 			</table>
					</td>
					<td width="50%" style="line-height:95%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>: <?= $data->tujuan ?></td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>: <?= $data->tempat_berangkat ?></td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>: <?= date('d F Y', strtotime($data->tgl_kembali)) ?></td>
				 			</tr>
				 			<tr>
								<td></td>
								<td></td>
							</tr>
				 			<tr>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
							</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
								<td style="width:6%">III.</td>
				 				<td>Tiba di</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
								<td style="width:6%"></td>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>														
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td colspan="3" style="width:100%" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td colspan="2" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
								<td style="width:6%">IV.</td>
				 				<td>Tiba di</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
								<td style="width:6%"></td>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
								<td style="width:6%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:6%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:6%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:6%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:6%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td colspan="3" style="width:100%" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
								<td style="width:6%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td colspan="2" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
								<td style="width:5%">V.</td>
				 				<td>Tiba di</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
								<td style="width:5%"></td>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td colspan="3" style="width:100%" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td colspan="2" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table >
				 			<tr>
								<td style="width:6%">VI.</td>
				 				<td>Tiba di</td>
				 				<td>: <?= $data->tempat_berangkat ?></td>
				 			</tr>
				 			<tr>
								<td style="width:6%"></td>
				 				<td>Pada Tanggal</td>
				 				<td>: <?= date('d F Y', strtotime($data->tgl_kembali)) ?></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="3" style="width:100%">An. Pejabat yang berwenang/<br>Pejabat lainnya yang di tunjuk</td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="3"></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="3"><p style="text-decoration:underline;"><b><?= $data->getRelationField("pejabat","nama") ?></b></p></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="3">NIP. <?=$data->getRelationNip("pejabat") ?> </td>
				 			</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td><p>Telah diperiksa dengan keterangan bahwa perjalanan
				 						tersebut atas perintahnya dan semata-mata untuk
				 						kepentingan jabatan dan waktu yang sesingkat-singkatnya.</p>
				 				</td>
				 			</tr>
				 			<tr>
				 				<td align="center">An. Pejabat yang berwenang/<br>Pejabat lainnya yang di tunjuk</td>
				 			</tr>
				 			<tr>
				 				<td align="center"></td>
				 			</tr>
				 			<tr>
				 				<td align="center"></td>
				 			</tr>
							<tr>
				 				<td align="center"></td>
				 			</tr>

							<tr>
				 				<td align="center"></td>
				 			</tr>
							<tr>
				 				<td align="center"></td>
				 			</tr>
				 			<tr>
				 				// <td align="center"><p><b><u><?= $data->getRelationField("pejabat","nama"); ?></u></b><br>NIP. <?=$data->getRelationNip("pejabat") ?></p></td>
				 			</tr>

			 			</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						Catatan lain lain:
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table>
							<tr>
								<td width="5%">
									<b>VII.</b>
								</td>
								<td width="95%">
								<b>PERHATIAN</b><br>
								Pejabat yang berwenang menerbitkan SPPD, pegawai yang melakukan perjalanan dinas, para pejabat yang mengesahkan tanggal
								berangkat/tiba, serta bendaharawan bertanggung jawab berdasarkan peraturan-peraturan keuangan Negara apabila negara menderita rugi
								akibat kesalahan, kelalaian dan kealpaannya.
								</td>
							</tr>
						</table>
						
					</td>
				</tr>
				 <?php ;
				$i++;
			}
		} else {
			 ?>
			 	<tr>
			 		<td width="50%" rowspan="2">&nbsp;</td>
			 		<td width="50%" >
			 			<table width="100%">
				 			<tr>
								<td style="width:5%">I.</td>
				 				<td style="width:40%">Berangkat dari<br>(Tempat Kedudukan)</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
								<td style="width:5%">&nbsp;</td>
				 				<td style="width:40%">Ke</td>
				 				<td>:</td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td style="width:40%"></td>
				 				<td></td>
				 			</tr>
				 			<tr>
								<td style="width:5%">&nbsp;</td>
				 				<td style="width:40%">Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
			 			</table>
			 		</td>
				</tr>
				<tr>
					<td width="50%" >
			 			<table border="1" width="100%">
				 			<tr>
				 				<td align="center" colspan="2"></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
							<tr>
				 				<td align="center" colspan="2"><div>&nbsp;</div></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2"></td>
				 			</tr>
				 			<tr>
				 				<td align="center" colspan="2">NIP.</td>
				 			</tr>
			 			</table>
			 		</td>
			 	</tr>
				<tr>
					<td width="50%" style="line-height:95%">
						<table>
				 			<tr>
								<td style="width:5%">II.</td>
				 				<td>Tiba di</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
								<td style="width:5%"></td>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
								<td style="width:5%"></td>
				 				<td></td>
				 				<td></td>
				 			</tr>
				 			<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
				 			<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td style="width:5%"></td>
								<td></td>
								<td></td>
							</tr>
			 			</table>
					</td>
					<td width="50%" style="line-height:95%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
								<td></td>
								<td></td>
							</tr>
				 			<tr>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
							</tr>
			 			</table>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<table>
				 			<tr>
								<td style="width:6%">III.</td>
				 				<td>Tiba di</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
								<td style="width:6%"></td>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td colspan="3" style="width:100%" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
			 			</table>
					</td>
					<td width="50%">
						<table>
				 			<tr>
				 				<td>Berangkat dari</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Ke</td>
				 				<td>:</td>
				 			</tr>
				 			<tr>
				 				<td>Pada Tanggal</td>
				 				<td>:</td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
							<tr>
								<td colspan="2" align="center"><b>_________________________________</b></td>
							</tr>
							<tr>
				 				<td></td>
				 				<td></td>
				 			</tr>
			 			</table>
					</td>
				</tr>
					<tr>
						<td width="50%">
							<table>
					 			<tr>
									<td style="width:6%">IV.</td>
					 				<td>Tiba di</td>
					 				<td>:</td>
					 			</tr>
					 			<tr>
									<td style="width:6%"></td>
					 				<td>Pada Tanggal</td>
					 				<td>:</td>
					 			</tr>
								<tr>
									<td style="width:6%"></td>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
									<td style="width:6%"></td>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
									<td style="width:6%"></td>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
									<td style="width:6%"></td>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
									<td style="width:6%"></td>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
									<td style="width:6%"></td>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
									<td colspan="3" style="width:100%" align="center"><b>_________________________________</b></td>
								</tr>
								<tr>
									<td style="width:6%"></td>
					 				<td></td>
					 				<td></td>
					 			</tr>
				 			</table>
						</td>
						<td width="50%">
							<table>
					 			<tr>
					 				<td>Berangkat dari</td>
					 				<td>:</td>
					 			</tr>
					 			<tr>
					 				<td>Ke</td>
					 				<td>:</td>
					 			</tr>
					 			<tr>
					 				<td>Pada Tanggal</td>
					 				<td>:</td>
					 			</tr>
								<tr>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
									<td colspan="2" align="center"><b>_________________________________</b></td>
								</tr>
								<tr>
					 				<td></td>
					 				<td></td>
					 			</tr>
				 			</table>
						</td>
					</tr>
					<tr>
						<td width="50%">
							<table>
					 			<tr>
									<td style="width:5%">V.</td>
					 				<td>Tiba di</td>
					 				<td>:</td>
					 			</tr>
					 			<tr>
									<td style="width:5%"></td>
					 				<td>Pada Tanggal</td>
					 				<td>:</td>
					 			</tr>
								<tr>
									<td style="width:5%"></td>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
									<td style="width:5%"></td>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
									<td style="width:5%"></td>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
									<td style="width:5%"></td>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
									<td style="width:5%"></td>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
									<td style="width:5%"></td>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
									<td colspan="3" style="width:100%" align="center"><b>_________________________________</b></td>
								</tr>
								<tr>
									<td style="width:5%"></td>
					 				<td></td>
					 				<td></td>
					 			</tr>
				 			</table>
						</td>
						<td width="50%">
							<table>
					 			<tr>
					 				<td>Berangkat dari</td>
					 				<td>:</td>
					 			</tr>
					 			<tr>
					 				<td>Ke</td>
					 				<td>:</td>
					 			</tr>
					 			<tr>
					 				<td>Pada Tanggal</td>
					 				<td>:</td>
					 			</tr>
								<tr>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
					 				<td></td>
					 				<td></td>
					 			</tr>
								<tr>
									<td colspan="2" align="center"><b>_________________________________</b></td>
								</tr>
								<tr>
					 				<td></td>
					 				<td></td>
					 			</tr>
				 			</table>
						</td>
					</tr>
					<tr>
						<td width="50%">
							<table >
					 			<tr>
									<td style="width:6%">VI.</td>
					 				<td>Tiba di</td>
					 				<td>:</td>
					 			</tr>
					 			<tr>
									<td style="width:6%"></td>
					 				<td>Pada Tanggal</td>
					 				<td>:</td>
					 			</tr>
								<tr>
					 				<td align="center" colspan="3"></td>
					 			</tr>
					 			<tr>
					 				<td align="center" colspan="3" style="width:100%">An. Pejabat yang berwenang/<br>Pejabat lainnya yang di tunjuk</td>
					 			</tr>
					 			<tr>
					 				<td align="center" colspan="3"></td>
					 			</tr>
					 			<tr>
					 				<td align="center" colspan="3"></td>
					 			</tr>
								<tr>
					 				<td align="center" colspan="3"></td>
					 			</tr>
								
								<tr>
					 				<td align="center" colspan="3"></td>
					 			</tr>
					 			<tr>
					 				<td align="center" colspan="3"></td>
					 			</tr>
					 			<tr>
					 				<td align="center" colspan="3">NIP.</td>
					 			</tr>
				 			</table>
						</td>
						<td width="50%">
							<table>
					 			<tr>
					 				<td><p>Telah diperiksa dengan keterangan bahwa perjalanan
					 						tersebut atas perintahnya dan semata-mata untuk
					 						kepentingan jabatan dan waktu yang sesingkat-singkatnya.</p>
					 				</td>
					 			</tr>
					 			<tr>
					 				<td align="center">An. Pejabat yang berwenang/<br>Pejabat lainnya yang di tunjuk</td>
					 			</tr>
					 			
								<tr>
					 				<td align="center"></td>
					 			</tr>
								
								<tr>
					 				<td align="center"></td>
					 			</tr>
								<tr>
					 				<td align="center"></td>
					 			</tr>
								<tr>
					 				<td align="center"></td>
					 			</tr>
					 			<tr>
					 				<td align="center"></td>
					 			</tr>

				 			</table>
						</td>
					</tr>
				<tr>
					<td colspan="2">
						Catatan lain lain:
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table>
							<tr>
								<td width="5%">
									<b>VII.</b>
								</td>
								<td width="95%">
								<b>PERHATIAN</b><br>
								Pejabat yang berwenang menerbitkan SPPD, pegawai yang melakukan perjalanan dinas, para pejabat yang mengesahkan tanggal
								berangkat/tiba, serta bendaharawan bertanggung jawab berdasarkan peraturan-peraturan keuangan Negara apabila negara menderita rugi
								akibat kesalahan, kelalaian dan kealpaannya.
								</td>
							</tr>
						</table>
						
					</td>
				</tr>

				<?php
				
		} ?>
		</table>