<?php
$this->breadcrumbs=array(
	'Spds'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Spd','url'=>array('index')),
array('label'=>'Create Spd','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('spd-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Daftar Surat Perjalanan Dinas</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'spd-grid',
'type'=> 'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'nomor_spt',
		array(
			'class'=>'CDataColumn',
			'name'=>'tanggal',
			'header'=>'Tanggal',
			'type'=>'raw',
			'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tanggal)',
		),
		array(
			'class'=>'CDataColumn',
			'name'=>'id_pejabat',
			'header'=>'Penandatangan',
			'type'=>'raw',
			'value'=>'$data->pejabat->nama',
			'filter'=>CHtml::listData(Pejabat::model()->findAll(),'id','nama')
		),
		array(
			'class'=>'CDataColumn',
			'name'=>'id_pegawai',
			'header'=>'Pegawai',
			'type'=>'raw',
			'value'=>'$data->pegawai->nama',
			'filter'=>CHtml::listData(Pegawai::model()->findAll(),'id','nama')
		),
		'maksud',
		array(
			'class'=>'CDataColumn',
			'type'=>'raw',
			'header'=>'Cetak',
			'value'=>'CHtml::link("<center><i class=icon-print ></i></center>",array("spd/view2","id"=>"$data->id"))',
		),
		/*
		'tujuan',
		'tgl_pergi',
		'tgl_kembali',
		'lama',
		'kendaraan',
		'tempat_berangkat',
		'instansi',
		'biaya',
		'koring',
		
		array(
		'class'=>'bootstrap.widgets.TbButtonColumn',
		'template'=>'',
		'buttons' => array( 
						'inputspd'=>array(
							'label'=>'<i class="icon-download"></i>',
							'url'=>'Yii::app()->createUrl("spd/inputspd", array("id"=>$data->id))',
						),
					),
		),*/
	),
	
)); ?>
