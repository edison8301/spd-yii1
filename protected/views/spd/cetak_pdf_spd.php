<?php

		$pejabat_berwenang = $model->getRelationField("pejabat","jabatan");
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $pejabat_berwenang = "&nbsp;";
		
		$nama = $model->getRelationField("pegawai","nama").'<br>'.$model->getRelationNip("pegawai");
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $nama = "&nbsp;";
		
		$pangkat = $model->getRelationRelationField("pegawai","golongan","nama");
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $pangkat = "&nbsp;";
		
		$jabatan = $model->getRelationField("pegawai","jabatan");
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $jabatan = "&nbsp;";
		
		$biaya = '-';
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $biaya = "&nbsp;";
		
		$maksud = $model->maksud;
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $maksud = "&nbsp;";
		
		$kendaraan = $model->kendaraan;
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $kendaraan = "&nbsp;";
		
		$tempat_berangkat = $model->tempat_berangkat;
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $tempat_berangkat = "&nbsp;";
		
		$tempat_tujuan = $model->tujuan;
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $tempat_tujuan = "&nbsp;";
		
		$lama = $model->lama.' Hari';
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $lama = "&nbsp;";
		
		$tgl_pergi = Bantu::tanggal(($model->tgl_pergi));
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $tgl_pergi = "&nbsp;";
		
		$tgl_kembali = Bantu::tanggal(($model->tgl_kembali));
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $tgl_kembali = "&nbsp;";

		$nomor_spd = $model->nomor_spd;
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $model->nomor_spd = "&nbsp;";
	

		?>


		
	
			<div class="table" style="margin-top: 200px !important;">
			 	<div style="text-align:center; margin-top: 220px;">
					<b><u>SURAT PERJALANAN DINAS</u></b><br>
					Nomor : <?= $nomor_spd ?>
				</div>
				<br>
				<table width="80%" style="border-collapse: collapse; padding: 30px" cellpadding="5px" border="1">	
				<tr>
					<td width="6" align="center">1</td>
					<td width="" >Pejabat berwenang yang memberi perintah</td>
					<td width="200px" colspan="2"><?= $pejabat_berwenang ?></td>
				</tr>
				<tr>
					<td align="center">2</td>
					<td >Nama/NIP pegawai yang diperintahkan</td>
					<td colspan="2"><?= $nama ?></td>
				</tr>
				<tr>
					<td align="center">3</td>
					<td >a. Pangkat dan Golongan Ruang Gaji</td>
					<td colspan="2"><?= $pangkat ?></td>
				</tr>
				<tr>
					<td align="center"></td>
					<td >b. Jabatan / Instansi</td>
					<td colspan="2"><?= $jabatan ?></td>
				</tr>
				<tr>
					<td align="center"></td>
					<td >c. Tingkat biaya perjalanan dinas</td>
					<td colspan="2"><?= $biaya ?></td>
				</tr>
				<tr>
					<td align="center">4</td>
					<td >Maksud perjalanan dinas</td>
					<td colspan="2"><?= $maksud ?></td>
				</tr>
				<tr>
					<td align="center">5</td>
					<td >Alat angkutan yang dipergunakan</td>
					<td colspan="2"><?= $kendaraan ?></td>
				</tr>
				<tr>
					<td align="center">6</td>
					<td >a. Tempat berangkat</td>
					<td colspan="2"><?= $tempat_berangkat ?></td>
				</tr>
				<tr>
					<td align="center"></td>
					<td >b. Tempat tujuan</td>
					<td colspan="2"><?= $tempat_tujuan ?></td>
				</tr>
				<tr>
					<td align="center">7</td>
					<td >a. Lamanya perjalanan dinas</td>
					<td colspan="2"><?= $lama ?></td>
				</tr>
				<tr>
					<td align="center"></td>
					<td >b. Tanggal berangkat</td>
					<td colspan="2"><?= $tgl_pergi ?></td>
				</tr>
				<tr>
					<td align="center"></td>
					<td >c. Tanggal harus kembali / tiba di tempat baru*)</td>
					<td colspan="2"><?= $tgl_kembali ?></td>
				</tr>
				<tr>
					<td align="center">8</td>
					<td ><b>Pengikut &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama:</b></td>
					<td> <b>Tgl. Lahir</b></td>
					<td> <b>Jabatan</b></td>
				</tr>

		<?php
	
		$namaPengikut = '';
		$tglLahirPengikut = '';
		$jabatanPengikut = '';
		
		$i = 1;
		
		foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$id,'aktif'=>1)) as $pengikut)
		{
			$nama = $pengikut->getRelationField("pegawai","nama");
			if(isset($_GET['blanko']) AND $_GET['blanko']==1) $nama = "&nbsp;";
			
			$tglLahir = date('d/m/Y', strtotime($pengikut->getRelationField("pegawai","tgl_lahir")));
			if(isset($_GET['blanko']) AND $_GET['blanko']==1) $tglLahir = "&nbsp;";
			
			$jabatan = $pengikut->getRelationField("pegawai","jabatan");
			if(isset($_GET['blanko']) AND $_GET['blanko']==1) $jabatan = "&nbsp;";
			
			
			$namaPengikut 		.= $nama.'<br>';
			$tglLahirPengikut 	.= $tglLahir.'<br>';
			$jabatanPengikut 	.= $jabatan.'<br>';
			$i++;
		}
		
		for($j=$i;$j<=6;$j++)
		{
			$namaPengikut .= '<br>&nbsp;';
			$tglLahirPengikut .= '<br>&nbsp;';
			$jabatanPengikut .= '<br>&nbsp;';
		}
			?>
				<tr>
					<td width="6%" align="center"></td>
					<td><?= $namaPengikut ?></td>
					<td><?= $tglLahirPengikut ?></td>
					<td><?= $jabatanPengikut ?></td>
				</tr>
			<?php
		
		$instansi = $model->instansi;
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $instansi = "&nbsp;";
		
		$koring = $model->koring;
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $koring = "&nbsp;";
		
		?>
				<tr>
					<td width="6%" align="center">9</td>
					<td width="100%" >
						Pembebanan Anggaran<br>
						a. Instansi<br>
						b. Mata Anggaran
					</td>
					<td colspan="2" >
						<br><br>
						<?= $instansi ?><br>
						<?= $koring ?>
					</td>
				</tr>
				<tr>
					<td width="6%" align="center">10</td>
					<td width="47%" >Keterangan Lain-lain</td>
					<td colspan="2" ></td>
				</tr>

		</table>

		<?php
		
		$dikeluarkan = $model->tempat_berangkat;
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $dikeluarkan = "&nbsp;";
		
		$tgl_spd = $model->tanggal_spt; 
		if($model->tanggal_spd!=null) $tgl_spd = $model->tanggal_spd;
		$tgl_spd = Bantu::tanggal($tgl_spd);

		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $tgl_spd = "&nbsp;";
		
		$pejabat = $model->getRelationField("pejabat","jabatan");
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $pejabat = "&nbsp;";
		
		$namaPejabat = $model->getRelationField("pejabat","nama");
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $namaPejabat = "&nbsp;";
		
		$nip = $model->getRelationNip("pejabat");
		if(isset($_GET['blanko']) AND $_GET['blanko']==1) $nip = "&nbsp;";
	
		
		?>
				<br>
				<table>
					<tr>
						<td width="60%">&nbsp;</td>
						<td width="35%" style="text-align:center">
							Di keluarkan di : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?= $dikeluarkan ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
							Pada Tanggal : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $tgl_spd ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
							<br><?= $pejabat ?><br>
							<br><br><br><br>
							<b><u><?= $namaPejabat ?></u></b><br>
							NIP. <?= $nip ?>
						</td>
					</tr>
				</table>
	</div>
		
		
 