<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'spd-form',
	'enableAjaxValidation'=>false,
)); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$(".format-money").on("keyup", function(){
	    var _this = $(this);
	    var value = _this.val().replace(/\.| /g,"");
	    _this.val(accounting.formatMoney(value, "", 0, ".", ","))
	})
});
</script>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>


<?php echo $form->errorSummary($model); ?>

<div class="row-fluid">
	<div class="span5">
		<?php echo $form->textFieldRow($model,'nomor_spt',array('class'=>'span6','readonly'=>'readonly')); ?>
		<?php $this->widget('bootstrap.widgets.TbButton',array(
				'buttonType'=>'link',
				'type'=>'primary',
				'icon'=>'pencil white',
				'url'=>array('spd/updateSpt','id'=>$model->id),
				'htmlOptions'=>array('style'=>'margin-bottom:10px')
		)); ?>
		
		
		<?php //echo $form->textFieldRow($model,'tanggal',array('class'=>'span5')); ?>
		<div class="control-group ">
			<label class="control-label required" for="Spd_tanggal">
				Tanggal SPT</span>
			</label>
			<div class="controls">
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'name' => 'Spd[tanggal_spd]',
					'id'=>'Spd_tanggal_spt',
					'language' => 'id',
					'model' => $model,
					'value'=>$model->tanggal_spt,
					// additional javascript options for the date picker plugin
					'options'=>array(
					    'showAnim'=>'fold',
					    //'showOn'=>'button',
					    //'buttonImage'=>Yii::app()->baseUrl.'/img/calendar.png',
					    'dateFormat'=>'yy-mm-dd',
					    'changeMonth' => 'false',
					    'showButtonPanel' => 'false',
					    'changeYear'=>'false',
					    'constrainInput' => 'false',
					),
					'htmlOptions'=>array(
					    'style'=>'height:20px;width:150px; margin-bottom:10px',
					    'readonly'=>true,
					),
				));?>
				<?php $this->widget('bootstrap.widgets.TbButton',array(
				'buttonType'=>'link',
				'type'=>'primary',
				'icon'=>'pencil white',
				'url'=>array('spd/updateSpt','id'=>$model->id),
				'htmlOptions'=>array('style'=>'margin-bottom:10px')
		)); ?>
			</div>
		</div>

		<?php echo $form->textFieldRow($model,'tujuan',array('class'=>'span6','maxlength'=>255,'readonly'=>'readonly')); ?>
		<?php $this->widget('bootstrap.widgets.TbButton',array(
				'buttonType'=>'link',
				'type'=>'primary',
				'icon'=>'pencil white',
				'url'=>array('spd/updateSpt','id'=>$model->id),
				'htmlOptions'=>array('style'=>'margin-bottom:10px')
		)); ?>
		
		<?php //echo $form->textFieldRow($model,'tgl_pergi',array('class'=>'span5')); ?>
		<div class="control-group ">
			<label class="control-label required" for="Spd_tgl_pergi">
				Tanggal Pergi</span>
			</label>
			<div class="controls">
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'name' => 'Spd[tgl_pergi]',
					'id'=>'Spd_tgl_pergi',
					'language' => 'id',
					'model' => $model,
					'value'=>$model->tgl_pergi,
					// additional javascript options for the date picker plugin
					'options'=>array(
					    'showAnim'=>'fold',
					    //'showOn'=>'button',
					    //'buttonImage'=>Yii::app()->baseUrl.'/img/calendar.png',
					    'dateFormat'=>'yy-mm-dd',
					    'changeMonth' => 'false',
					    'showButtonPanel' => 'false',
					    'changeYear'=>'false',
					    'constrainInput' => 'false',
					),
					'htmlOptions'=>array(
					    'style'=>'height:20px;width:150px; margin-bottom:10px',
					    'readonly'=>true,
					),
				));?>
				<?php $this->widget('bootstrap.widgets.TbButton',array(
						'buttonType'=>'link',
						'type'=>'primary',
						'icon'=>'pencil white',
						'url'=>array('spd/updateSpt','id'=>$model->id),
						'htmlOptions'=>array('style'=>'margin-bottom:10px')
				)); ?>
			</div>
		</div>

		<?php // echo $form->textFieldRow($model,'tgl_kembali',array('class'=>'span5')); ?>
		<div class="control-group ">
			<label class="control-label required" for="Spd_tgl_kembali">
				Tanggal Kembali</span>
			</label>
			<div class="controls">
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'name' => 'Spd[tgl_kembali]',
					'id'=>'Spd_tgl_kembali',
					'language' => 'id',
					'model' => $model,
					'value'=>$model->tgl_kembali,
					// additional javascript options for the date picker plugin
					'options'=>array(
					    'showAnim'=>'fold',
					    //'showOn'=>'button',
					    //'buttonImage'=>Yii::app()->baseUrl.'/img/calendar.png',
					    'dateFormat'=>'yy-mm-dd',
					    'changeMonth' => 'false',
					    'showButtonPanel' => 'false',
					    'changeYear'=>'false',
					    'constrainInput' => 'false',
					),
					'htmlOptions'=>array(
					    'style'=>'height:20px;width:150px; margin-bottom:10px',
					    'readonly'=>true,
					),
				));?>
				<?php $this->widget('bootstrap.widgets.TbButton',array(
						'buttonType'=>'link',
						'type'=>'primary',
						'icon'=>'pencil white',
						'url'=>array('spd/updateSpt','id'=>$model->id),
						'htmlOptions'=>array('style'=>'margin-bottom:10px')
				)); ?>
			</div>
		</div>
		<?php echo $form->textareaRow($model,'maksud',array('class'=>'span8','maxlength'=>255,'rows'=>3,'readonly'=>'readonly')); ?>
		<?php $this->widget('bootstrap.widgets.TbButton',array(
				'buttonType'=>'link',
				'type'=>'primary',
				'icon'=>'pencil white',
				'url'=>array('spd/updateSpt','id'=>$model->id),
				'htmlOptions'=>array('style'=>'margin-bottom:10px')
		)); ?>
		
		<?php //echo $form->dropDownListRow($model,'id_pegawai',CHtml::listData(Pegawai::model()->findAll(array('order'=>'nama ASC')),'id','nama' , 'nip')); ?>
		<?php print CHtml::label('Pegawai',''); ?>
		<?php print CHtml::textField('pegawai',$model->getRelationField("pegawai","nama"),array('readonly'=>'readonly')); //echo $form->dropDownListRow($model,'id_pejabat',CHtml::listData(Pejabat::model()->findAll(array('order'=>'nama ASC')),'id','nama', 'nip')); ?>
		<?php $this->widget('bootstrap.widgets.TbButton',array(
				'buttonType'=>'link',
				'type'=>'primary',
				'icon'=>'pencil white',
				'url'=>array('spd/updateSpt','id'=>$model->id),
				'htmlOptions'=>array('style'=>'margin-bottom:10px')
		)); ?>
		
		<?php print CHtml::label('Penandatangan Dokumen',''); ?>
		<?php print CHtml::textField('pejabat',$model->getRelationField("pejabat","nama"),array('readonly'=>'readonly')); //echo $form->dropDownListRow($model,'id_pejabat',CHtml::listData(Pejabat::model()->findAll(array('order'=>'nama ASC')),'id','nama', 'nip')); ?>
		<?php $this->widget('bootstrap.widgets.TbButton',array(
				'buttonType'=>'link',
				'type'=>'primary',
				'icon'=>'pencil white',
				'url'=>array('spd/updateSpt','id'=>$model->id),
				'htmlOptions'=>array('style'=>'margin-bottom:10px')
		)); ?>
		
	</div>
	<div class="span6">
		<?php echo $form->textFieldRow($model,'nomor_spd',array('class'=>'span6','maxlength'=>255,'value'=>$model->nomor_spt)); ?>

		<?php echo $form->textFieldRow($model,'lama',array('class'=>'span4','maxlength'=>255,'append'=>'Hari')); ?>

		<?php echo $form->textFieldRow($model,'kendaraan',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'tempat_berangkat',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'instansi',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'biaya',array('class'=>'span10 format-money','maxlength'=>255,'id'=>'biaya','prepend'=>'Rp')); ?>

		<?php echo $form->dropDownListRow($model,'koring',array('5.2.15.01'=>'5.2.15.01','5.2.15.02'=>'5.2.15.02')); ?>
	</div>
</div>


<div class="row-fluid">
	<div class="span5">
		</div>
	<div class="span6">
		</div>
</div>	

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
	)); ?>
	<?php if(!$model->isNewRecord) { ?>
	<?php $this->widget('bootstrap.widgets.TbButton',array(
				'buttonType'=>'link',
				'type'=>'primary',
				'icon'=>'print white',
				'label'=>'Cetak SPD',
				'url'=>array('spd/viewSpd','id'=>$model->id),
		)); ?>
	<?php } ?>
</div>
<?php $this->endWidget(); ?>
