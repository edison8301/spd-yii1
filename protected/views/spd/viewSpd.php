<?php
$this->breadcrumbs=array(
	'Spds'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Spd','url'=>array('index')),
array('label'=>'Create Spd','url'=>array('create')),
array('label'=>'Update Spd','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Spd','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Spd','url'=>array('admin')),
);
?>

<h1>Surat Perjalanan Dinas</h1>
<div class="row-fluid">
	<div class="span3">
		<?php
			$this->widget(
				'bootstrap.widgets.TbButton',
				array(
				'label' => 'SPD kop SETDA (Lembar 1)',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('spd/cetakPdfSpd','id'=>$model->id,'kop'=>'setda'),
				'htmlOptions'=>array('target'=>'_blank')
				)
			);
			echo "<p></p>";
			$this->widget(
				'bootstrap.widgets.TbButton',
				array(
				'label' => 'SPD kop DPKD (Lembar 1)',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('spd/cetakPdfSpd','id'=>$model->id,'kop'=>'dpkd'),
				'htmlOptions'=>array('target'=>'_blank')
				)
			);
			echo "<p></p>";
			$this->widget(
				'bootstrap.widgets.TbButton',
				array(
				'label' => 'SPD (Lembar 2)',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('export/exportDataSpd','id'=>$model->id),
				'htmlOptions'=>array('target'=>'_blank')
				)
			);
			
		?>

	</div>
	<div class="span3">
		<?php $this->widget('bootstrap.widgets.TbButton',array(
					'label' => 'Blanko SPD kop SETDA (Lembar 1)',
					'icon'=>'download-alt white',
					'type' => 'primary',
					'url'=>array('spd/cetakPdfSpd','id'=>$model->id,'kop'=>'setda','blanko'=>1),
					'htmlOptions'=>array('target'=>'_blank'),
				)
			);
			echo "<p></p>";
			$this->widget(
				'bootstrap.widgets.TbButton',
				array(
					'label' => 'Blanko SPD kop DPKD (Lembar 1)',
					'icon'=>'download-alt white',
					'type' => 'primary',
					'url'=>array('spd/cetakPdfSpd','id'=>$model->id,'kop'=>'dpkd','blanko'=>1),
					'htmlOptions'=>array('target'=>'_blank'),
				)
			);
			echo "<p></p>";
			$this->widget(
				'bootstrap.widgets.TbButton',
				array(
					'label' => 'Blanko SPD (Lembar 2)',
					'icon'=>'download-alt white',
					'type' => 'primary',
					'url'=>array('export/exportDataSpd'),
					'htmlOptions'=>array('target'=>'_blank'),
				)
			);
		?>
	</div>
	<div class="span3">
		<?php
			$this->widget(
				'bootstrap.widgets.TbButton',
				array(
				'label' => 'SPD Setting Otomatis (Lembar 1)',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('export/exportSpdOto1','id'=>$model->id),
				'htmlOptions'=>array('target'=>'_blank')
				)
			);
			echo "<p></p>";
			$this->widget(
				'bootstrap.widgets.TbButton',
				array(
				'label' => 'SPD Setting Otomatis (Lembar 2)',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('export/exportSpdOto2','id'=>$model->id),
				'htmlOptions'=>array('target'=>'_blank')
				)
			);
		?>
	</div>
	<div class="span3">
		<?php /*
			$this->widget(
				'bootstrap.widgets.TbButton',
				array(
				'label' => 'Rekap Nomor SPD',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('export/ExportSpd'),
				'htmlOptions'=>array('target'=>'_blank')
				)
			);
			echo "<p></p>";
			$this->widget(
				'bootstrap.widgets.TbButton',
				array(
				'label' => 'Rekap SPD Detail',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('export/ExportSpdDetail','id'=>$model->id),
				'htmlOptions'=>array('target'=>'_blank')
				)
			);
		*/ ?>
	</div>
</div>

<div>&nbsp;</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'bordered striped',
		'attributes'=>array(
			'nomor_spd',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal',
				'header'=>'Tanggal SPD',
				'type'=>'raw',
				'value'=>Yii::app()->dateFormatter->format("dd-MM-yyyy",$model->tanggal_spd),
			),
			array(
				'label'=>'Pejabat',
				'type'=>'raw',
				'value'=>$model->getRelationField("pejabat","nama")
			),
			array(
				'label'=>'Pegawai',
				'type'=>'raw',
				'value'=>$model->getRelationField("pegawai","nama")
			),
			'maksud',
			'tujuan',
			array(
				'class'=>'CDataColumn',
				'name'=>'tgl_pergi',
				'header'=>'Tangal Pergi',
				'type'=>'raw',
				'value'=>Yii::app()->dateFormatter->format("dd-MM-yyyy",$model->tgl_pergi),
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'tgl_kembali',
				'header'=>'Tanggal Kembali',
				'type'=>'raw',
				'value'=>Yii::app()->dateFormatter->format("dd-MM-yyyy",$model->tgl_kembali),
			),
			array(
				'label'=>'Lama',
				'type'=>'raw',
				'value'=>''.CHtml::encode($model->lama).' hari'
			),
			'kendaraan',
			'tempat_berangkat',
			'instansi',
			'biaya',
			'koring',
		),
)); ?>

<?php $this->widget('bootstrap.widgets.TbButton',array(
		'label' => 'Sunting SPD',
		'icon'=>'pencil white',
		'type' => 'primary',
		'url'=>array('spd/updateSpd','id'=>$model->id),
)); ?>

<?php $this->widget('bootstrap.widgets.TbButton',array(
		'label' => 'Input SPD',
		'icon'=>'plus white',
		'type' => 'primary',
		'url'=>array('spd/adminSpd'),
)); ?>
