<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'nomor',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'tanggal',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'id_pejabat',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'id_pegawai',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'maksud',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'tujuan',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'tgl_pergi',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'tgl_kembali',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'lama',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'kendaraan',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'tempat_berangkat',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'instansi',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'biaya',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'koring',array('class'=>'span5','maxlength'=>255)); ?>

		<?php echo $form->textFieldRow($model,'pengikut',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
