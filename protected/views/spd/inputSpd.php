<?php
$this->breadcrumbs=array(
	'Spds'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Spd','url'=>array('index')),
array('label'=>'Create Spd','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('spd-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Entry Surat Perjalanan Dinas</h1>

<div>Silahkan pilih SPT yang akan dibuatkan SPD dari data di bawah ini</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'spd-grid',
		'type'=> 'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'class'=>'CDataColumn',
				'type'=>'raw',
				'header'=>'Pilih',
				'value'=>'"<div style=\"text-align:center\">".CHtml::link("<center><i class=\"icon-plus icon-white\"></i></center>",array("spd/updateSpd","id"=>"$data->id"),array("class"=>"btn btn-primary btn-mini"))."</div>"',
			),
			'nomor_spt',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_spt',
				'header'=>'Tanggal SPT',
				'type'=>'raw',
				'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tanggal_spd)',
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pejabat',
				'header'=>'Penandatangan',
				'type'=>'raw',
				'value'=>'$data->getRelationField("pejabat","nama")',
				'filter'=>CHtml::listData(Pejabat::model()->findAll(),'id','nama')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'id_pegawai',
				'header'=>'Pegawai',
				'type'=>'raw',
				'value'=>'$data->getRelationField("pegawai","nama")',
				'filter'=>CHtml::listData(Pegawai::model()->findAll(),'id','nama')
			),
			'maksud',
		),
)); ?>

<div class="form-actions">&nbsp;</div>

<h2>Data Surat Perjalanan Dinas</h2>

<?php $this->renderPartial('_adminSpd',array('model'=>$model)); ?>
