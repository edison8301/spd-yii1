<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor')); ?>:</b>
	<?php echo CHtml::encode($data->nomor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pejabat')); ?>:</b>
	<?php echo CHtml::encode($data->pejabat->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pegawai')); ?>:</b>
	<?php echo CHtml::encode($data->id_pegawai); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('maksud')); ?>:</b>
	<?php echo CHtml::encode($data->maksud); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tujuan')); ?>:</b>
	<?php echo CHtml::encode($data->tujuan); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_pergi')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_pergi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_kembali')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_kembali); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lama')); ?>:</b>
	<?php echo CHtml::encode($data->lama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kendaraan')); ?>:</b>
	<?php echo CHtml::encode($data->kendaraan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tempat_berangkat')); ?>:</b>
	<?php echo CHtml::encode($data->tempat_berangkat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('instansi')); ?>:</b>
	<?php echo CHtml::encode($data->instansi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biaya')); ?>:</b>
	<?php echo CHtml::encode($data->biaya); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('koring')); ?>:</b>
	<?php echo CHtml::encode($data->koring); ?>
	<br />

	*/ ?>

</div>