<?php
$this->breadcrumbs=array(
	'Spds'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Spd','url'=>array('index')),
array('label'=>'Manage Spd','url'=>array('admin')),
);
?>

<h1>Update Surat Perintah Tugas</h1>

<?php echo $this->renderPartial('_formSpt', array('model'=>$model,'model2'=>$model2)); ?>

<?php print $this->renderPartial('_adminSpt',array('spd'=>$spd)); ?>