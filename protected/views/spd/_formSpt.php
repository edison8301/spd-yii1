<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'spd-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>
<?php echo $form->errorSummary($model2); ?>


<div class="row-fluid">
	<div class="span5">
		<?php echo $form->textFieldRow($model,'nomor_spt',array('class'=>'span5','maxlength'=>255)); ?>

		<?php //echo $form->textFieldRow($model,'tanggal',array('class'=>'span5')); ?>
		<div class="control-group ">
			<label class="control-label required" for="Spd_tanggal">
				Tanggal SPT</span>
			</label>
			<div class="controls">
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'name' => 'Spd[tanggal_spt]',
					'id'=>'Spd_tanggal_spt',
					'language' => 'id',
					'model' => $model,
					'value'=>$model->tanggal_spt,
					// additional javascript options for the date picker plugin
					'options'=>array(
					    'showAnim'=>'fold',
					    'showOn'=>'button',
					    'buttonImage'=>Yii::app()->baseUrl.'/img/calendar.png',
					    'dateFormat'=>'yy-mm-dd',
					    'changeMonth' => 'false',
					    'showButtonPanel' => 'false',
					    'changeYear'=>'false',
					    'constrainInput' => 'false',
					),
					'htmlOptions'=>array(
					    'style'=>'height:20px;width:150px; margin-bottom:0px',
					    'readonly'=>true,
					),
				));?>
			</div>
		</div>
		
		<?php echo $form->hiddenField($model,'id_pegawai'); ?>
		
		<?php print CHtml::label('Nama Pegawai',''); ?>
		<?php print CHtml::textField('nama_pegawai',$model->id_pegawai != null ? $model->getRelationField("pegawai","nama") : '',array('readonly'=>'readonly','placeholder'=>'Pilih Pegawai')); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'[..]',
            'type'=>'danger', 
            'htmlOptions'=>array(
				'onClick'=>'$("#dialogPegawai").dialog("open"); return false;',
				'style'=>'margin-bottom:10px',
            ),
		)); ?>
		
		<?php echo $form->textFieldRow($model,'tujuan',array('class'=>'span6','maxlength'=>255)); ?>

		<?php //echo $form->textFieldRow($model,'tgl_pergi',array('class'=>'span5')); ?>
		<div class="control-group ">
			<label class="control-label required" for="Spd_tgl_pergi">
				Tanggal Pergi</span>
			</label>
			<div class="controls">
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'name' => 'Spd[tgl_pergi]',
					'id'=>'Spd_tgl_pergi',
					'language' => 'id',
					'model' => $model,
					'value'=>$model->tgl_pergi,
					// additional javascript options for the date picker plugin
					'options'=>array(
					    'showAnim'=>'fold',
					    'showOn'=>'button',
					    'buttonImage'=>Yii::app()->baseUrl.'/img/calendar.png',
					    'dateFormat'=>'yy-mm-dd',
					    'changeMonth' => 'false',
					    'showButtonPanel' => 'false',
					    'changeYear'=>'false',
					    'constrainInput' => 'false',
					),
					'htmlOptions'=>array(
					    'style'=>'height:20px;width:150px; margin-bottom:0px',
					    'readonly'=>true,
					),
				));?>
			</div>
		</div>

		<?php // echo $form->textFieldRow($model,'tgl_kembali',array('class'=>'span5')); ?>
		<div class="control-group ">
			<label class="control-label required" for="Spd_tgl_kembali">
				Tanggal Kembali</span>
			</label>
			<div class="controls">
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'name' => 'Spd[tgl_kembali]',
					'id'=>'Spd_tgl_kembali',
					'language' => 'id',
					'model' => $model,
					'value'=>$model->tgl_kembali,
					// additional javascript options for the date picker plugin
					'options'=>array(
					    'showAnim'=>'fold',
					    'showOn'=>'button',
					    'buttonImage'=>Yii::app()->baseUrl.'/img/calendar.png',
					    'dateFormat'=>'yy-mm-dd',
					    'changeMonth' => 'false',
					    'showButtonPanel' => 'false',
					    'changeYear'=>'false',
					    'constrainInput' => 'false',
					),
					'htmlOptions'=>array(
					    'style'=>'height:20px;width:150px; margin-bottom:0px',
					    'readonly'=>true,
					),
				));?>
			</div>
		</div>

	</div>
	<div class="span6">
		<?php echo $form->textareaRow($model,'maksud',array('class'=>'span8','maxlength'=>255,'rows'=>3)); ?>	
		
		<?php echo $form->hiddenField($model,'id_pejabat'); ?>
		
		<?php print CHtml::label('Penandatangan Dokumen',''); ?>
		<?php print CHtml::textField('nama_pejabat',$model->id_pejabat != null ? $model->getRelationField("pejabat","nama") : '',array('readonly'=>'readonly','placeholder'=>'Pilih Penandatangan')); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'[..]',
            'type'=>'danger', 
            'htmlOptions'=>array(
				'onClick'=>'$("#dialogPejabat").dialog("open"); return false;',
				'style'=>'margin-bottom:10px',
            ),
		)); ?>
		
		<hr>
		
		<?php $i=1;foreach($model->getDataPengikut() as $data) { ?>
		<?php print CHtml::hiddenField('Pengikut['.$i.']',$data->id_pegawai); ?>
		Pengikut <?php print $i; ?> <?php print CHtml::textField('nama_pengikut['.$i.']',$data->getRelationField("pegawai","nama"),array('readonly'=>'readonly','placeholder'=>'Pilih Pegawai')); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'[..]',
            'type'=>'danger', 
            'htmlOptions'=>array(
				'onClick'=>'$("#dialogPengikut_'.$i.'").dialog("open"); return false;',
				'style'=>'margin-bottom:10px',
            ),
		)); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'X',
            'type'=>'danger', 
            'htmlOptions'=>array(
				'onClick'=>'$("#Pengikut_'.$i.'").val("");$("#nama_pengikut_'.$i.'").val(""); return false;',
				'style'=>'margin-bottom:10px',
            ),
		)); ?><br>
		
		<?php $i++; } ?>
		
		<?php for($j=$i;$j<=6;$j++) { ?>
		<?php print CHtml::hiddenField('Pengikut['.$j.']',''); ?>
		Pengikut <?php print $j; ?> <?php print CHtml::textField('nama_pengikut['.$j.']','',array('readonly'=>'readonly','placeholder'=>'Pilih Pegawai')); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'[..]',
            'type'=>'danger', 
            'htmlOptions'=>array(
				'onClick'=>'$("#dialogPengikut_'.$j.'").dialog("open"); return false;',
				'style'=>'margin-bottom:10px',
            ),
		)); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'label'=>'X',
            'type'=>'danger', 
            'htmlOptions'=>array(
				'onClick'=>'$("#Pengikut_'.$j.'").val("");$("#nama_pengikut_'.$j.'").val(""); return false;',
				'style'=>'margin-bottom:10px',
            ),
		)); ?>
		<br>
		<?php } ?>
		
	</div>
</div>

<div class="row-fluid">
	<div class="span5">
			</div>
	
</div>	


<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
	)); ?>
	<?php if(!$model->isNewRecord) { ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'link',
			'type'=>'primary',
			'icon'=>'print white',
			'label'=>'Cetak',
			'url'=>array('spd/view','id'=>$model->id)
	)); ?>
	<?php } ?>
</div>
<?php $this->endWidget(); ?>





<?php //Dialog Pegawai
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogPegawai',
    'options'=>array(
        'title'=>'Pilih Pegawai',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	$pegawai = new Pegawai;
	$this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'pegawai-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$pegawai->search(),
		'filter'=>$pegawai,
		'columns'=>array(
			'nip',
			'nama',
			array(
				'class'=>'CDataColumn',
				'name'=>'id_golongan',
				'header'=>'Golongan',
				'type'=>'raw',
				'value'=>'$data->golongan->nama',
				'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
			),
			'jabatan',
			array(
				'class'=>'CDataColumn',
				'name'=>'tgl_lahir',
				'header'=>'Tanggal Lahir',
				'type'=>'raw',
				'value'=>'Bantu::tgl($data->tgl_lahir)',
				//'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_lahir)',
				
			),
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#dialogPegawai\").dialog(\"close\");
							    $(\"#Spd_id_pegawai\").val(\"$data->id\");
								$(\"#nama_pegawai\").val(\"$data->nama\");",
					"class"=>"btn-primary"
				))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
					
				),
			),
		),
));
	
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer
?>

<?php //Dialog Pejabat
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogPejabat',
    'options'=>array(
        'title'=>'Pilih Penandatangan Dokumen',
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	$pejabat = new Pejabat;
	
	$this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'pejabat-grid',
		'type'=> 'striped bordered',
		'dataProvider'=>$pejabat->search(),
		'filter'=>$pejabat,
		'columns'=>array(
			'nip',
			'nama',
			'jabatan',
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#dialogPejabat\").dialog(\"close\");
							    $(\"#Spd_id_pejabat\").val(\"$data->id\");
								$(\"#nama_pejabat\").val(\"$data->nama\");
                                								
				"))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
		),
	));
	
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer
?>

<?php //Dialog Pengikut

for($i=1;$i<=6;$i++) { 

$pengikut[$i] = new Pegawai;

$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogPengikut_'.$i,
    'options'=>array(
        'title'=>'Pilih Pengikut '.$i,
        'autoOpen'=>false,
		'minWidth'=>700,
		'modal'=>true,
    ),
));
	
	$this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'pegawai-grid-'.$i,
		'type'=>'striped bordered',
		'dataProvider'=>$pengikut[$i]->search(),
		'filter'=>$pengikut[$i],
		'columns'=>array(
			'nip',
			'nama',
			array(
				'class'=>'CDataColumn',
				'name'=>'id_golongan',
				'header'=>'Golongan',
				'type'=>'raw',
				'value'=>'$data->golongan->nama',
				'filter'=>CHtml::listData(Golongan::model()->findAll(),'id','nama')
			),
			'jabatan',
			array(
				'class'=>'CDataColumn',
				'name'=>'tgl_lahir',
				'header'=>'Tanggal Lahir',
				'type'=>'raw',
				'value'=>'Bantu::tgl($data->tgl_lahir)',
				//'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_lahir)',
			),
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Button("+",array(
					"onClick" => "$(\"#dialogPengikut_'.$i.'\").dialog(\"close\");
							    $(\"#Pengikut_'.$i.'\").val(\"$data->id\");
								$(\"#nama_pengikut_'.$i.'\").val(\"$data->nama\");
                                								
				"))',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
		),
));
	
$this->endWidget('zii.widgets.jui.CJuiDialog'); //=== end CJuiDialog for mark insurer

}
?>