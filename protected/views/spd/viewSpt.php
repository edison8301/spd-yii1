<?php
$this->breadcrumbs=array(
	'Spds'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Spd','url'=>array('index')),
array('label'=>'Create Spd','url'=>array('create')),
array('label'=>'Update Spd','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Spd','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Spd','url'=>array('admin')),
);
?>

<h1>Cetak Surat Perintah Tugas</h1>

<div class="row-fluid">
	<div class="span3">
		
		<?php $this->widget('bootstrap.widgets.TbButton',array(
				'label' => 'SPT kop SETDA Tanpa Pengikut',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('spd/cetakPdfSpt','id'=>$model->id,'tanpaPengikut'=>1,'kop'=>'setda'),
				'htmlOptions'=>array('target'=>'_blank','style'=>'margin-bottom:5px;')
		)); ?>
		<?php $this->widget('bootstrap.widgets.TbButton',array(
				'label' => 'SPT kop SETDA',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('spd/cetakPdfSpt','id'=>$model->id,'kop'=>'setda'),
				'htmlOptions'=>array('target'=>'_blank')
		)); ?>
	</div>
	<div class="span3">
		<?php $this->widget('bootstrap.widgets.TbButton',array(
				'label' => 'SPT kop DPKD Tanpa Pengikut',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('spd/cetakPdfSpt','id'=>$model->id,'tanpaPengikut'=>1,'kop'=>'dpkd'),
				'htmlOptions'=>array('target'=>'_blank','style'=>'margin-bottom:5px')
		)); ?>
		<?php $this->widget('bootstrap.widgets.TbButton',array(
				'label' => 'SPT kop DPKD',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('spd/cetakPdfSpt','id'=>$model->id,'kop'=>'dpkd'),
				'htmlOptions'=>array('target'=>'_blank')
		)); ?>
	</div>
	<div class="span3">
		<?php /*
			$this->widget(
				'bootstrap.widgets.TbButton',
				array(
				'label' => 'Rekap Nomor SPT',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('export/ExportSpt'),
				'htmlOptions'=>array('target'=>'_blank')
				)
			);
			echo "<p></p>";
			$this->widget(
				'bootstrap.widgets.TbButton',
				array(
				'label' => 'Rekap SPT Detail',
				'icon'=>'download-alt white',
				'type' => 'primary',
				'url'=>array('export/ExportSptDetail','id'=>$model->id),
				'htmlOptions'=>array('target'=>'_blank')
				)
			);
		*/ ?>
	</div>
</div>

<div>&nbsp;</div>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'nomor_spt',
			array(
				'class'=>'CDataColumn',
				'name'=>'tanggal_spt',
				'header'=>'Tanggal SPT',
				'type'=>'raw',
				'value'=>Yii::app()->dateFormatter->format("dd-MM-yyyy",$model->tanggal_spt),
			),
			array(
				'label'=>'Pejabat',
				'type'=>'raw',
				'value'=>$model->getRelationField("pejabat","nama")
			),
			array(
				'label'=>'Pegawai',
				'type'=>'raw',
				'value'=>$model->getRelationField("pegawai","nama")
			),
			'tujuan',	
			array(
				'class'=>'CDataColumn',
				'name'=>'tgl_pergi',
				'header'=>'Tangal Pergi',
				'type'=>'raw',
				'value'=>Yii::app()->dateFormatter->format("dd-MM-yyyy",$model->tgl_pergi),
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'tgl_kembali',
				'header'=>'Tanggal Kembali',
				'type'=>'raw',
				'value'=>Yii::app()->dateFormatter->format("dd-MM-yyyy",$model->tgl_kembali),
			),
			'maksud',
		),
)); ?>

<?php $this->widget('bootstrap.widgets.TbButton',array(
		'label' => 'Input SPD',
		'icon'=>'plus white',
		'type' => 'primary',
		'url'=>array('spd/updateSpd','id'=>$model->id),
)); ?>

<?php $this->widget('bootstrap.widgets.TbButton',array(
		'label' => 'Sunting SPT',
		'icon'=>'pencil white',
		'type' => 'primary',
		'url'=>array('spd/updateSpt','id'=>$model->id),
)); ?>
