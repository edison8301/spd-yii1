<?php
$this->breadcrumbs=array(
	'Spds'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Spd','url'=>array('index')),
array('label'=>'Create Spd','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('spd-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Rekap per SPD</h1>


<?php echo CHtml::beginForm('index.php?r=spd/rekap','get',array('name' => 'filter')); ?>
	<div class="control-group ">
			<label class="control-label required" for="Spd_tanggal">
				Filter Data</span>
			</label>
			<div class="controls">
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'name' => 'tgl_awal',
					'id' => 'tgl_awal',
					'language' => 'id',
					// additional javascript options for the date picker plugin
					'options'=>array(
					    'showAnim'=>'fold',
					    'showOn'=>'button',
					    'buttonImage'=>Yii::app()->baseUrl.'/img/calendar.png',
					    'dateFormat'=>'yy-mm-dd',
					    'changeMonth' => 'false',
					    'showButtonPanel' => 'false',
					    'changeYear'=>'false',
					    'constrainInput' => 'false',
					),
					'htmlOptions'=>array(
					    'style'=>'height:20px;width:150px; margin-bottom:0px',
					    'readonly'=>true,
					),
				));?>
			</div>
			s/d
			<div class="controls">
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'name' => 'tgl_akhir',
					'id' => 'tgl_akhir',
					'language' => 'id',
					// additional javascript options for the date picker plugin
					'options'=>array(
					    'showAnim'=>'fold',
					    'showOn'=>'button',
					    'buttonImage'=>Yii::app()->baseUrl.'/img/calendar.png',
					    'dateFormat'=>'yy-mm-dd',
					    'changeMonth' => 'false',
					    'showButtonPanel' => 'false',
					    'changeYear'=>'false',
					    'constrainInput' => 'false',
					),
					'htmlOptions'=>array(
					    'style'=>'height:20px;width:150px; margin-bottom:0px',
					    'readonly'=>true,
					),
				));?>
			</div>
		</div>
		<button class="btn btn-primary" type="submit"><i class="icon-search icon-white"/></i> Tampilkan Data</button>
<?php echo CHtml::endForm(); ?>


<div>&nbsp;</div>

<?php if(isset($_GET['tgl_awal']) & isset($_GET['tgl_akhir'])) { ?>

<table class="items table table-striped table-bordered">
<thead>
	<tr>
		<th>No</th>
		<th>No. Lamp. SPD</th>
		<th>Tanggal</th>
		<th>Nama Pegawai</th>
		<th>Tujuan</th>
		<th>Jumlah Biaya</th>
	</tr>
</thead>
<tbody>

<?php
	$criteria = new CDbCriteria;
	$criteria->params = array(':tgl_awal'=>$_GET['tgl_awal'], ':tgl_akhir'=>$_GET['tgl_akhir']);
	$criteria->condition = 'tanggal >= :tgl_awal AND tanggal <= :tgl_akhir AND nomor IS NOT NULL';
?>			
<?php $i=1; $grandTotal = 0; foreach (Spd::model()->findAll($criteria) as $data) { ?>

<?php $kwitansi = kwitansi::model()->findByAttributes(array('id_spd'=>$data->id)); ?>
	<tr>
		<td><?php echo $i; ?></td>
		<td><?php echo $data->nomor; ?></td>
		<td><?php echo Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_pergi); ?> s/d <?php echo Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->tgl_kembali); ?></td>
		<td><?php echo $data->pegawai->nama; ?></td>
		<td><?php echo $data->tujuan; ?></td>
		<td>Rp <?php echo number_format($kwitansi->total,0,",","."); ?></td>
	</tr>
<?php $i++; $grandTotal = $grandTotal + $kwitansi->total; }?>
	<tr>
		<td colspan="4">&nbsp;</td>
		<td  style="font-weight:bold">TOTAL</td>
		<td style="font-weight:bold">Rp <?php print number_format($grandTotal,0,",","."); ?></td>
	</tr>
</tbody>
</table>

<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','type'=>'primary','label'=>'Cetak','icon'=>'print white','url'=>array('export/RekapSpdFilter','tgl_awal'=>$_GET['tgl_awal'],'tgl_akhir'=>$_GET['tgl_akhir']))); ?>

<?php } ?>