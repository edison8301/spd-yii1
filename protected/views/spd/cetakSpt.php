<?php


$this->menu=array(
array('label'=>'List Spd','url'=>array('index')),
array('label'=>'Create Spd','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('spd-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Cetak Surat Perintah Tugas (SPT)</h1>

<div>Silahkan klik simbol printer <i class="icon-print"></i> untuk memilih data yang akan dicetak</div>

<?php print $this->renderPartial('_adminSpt',array('spd'=>$spd)); ?>
