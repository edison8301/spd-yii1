<?php
$this->breadcrumbs=array(
	'Spds'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Input Surat Perjalanan Dinas',
);

	$this->menu=array(
	array('label'=>'List Spd','url'=>array('index')),
	array('label'=>'Create Spd','url'=>array('create')),
	array('label'=>'View Spd','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Spd','url'=>array('admin')),
	);
	?>

<h1>Input Surat Perjalanan Dinas</h1>

<?php echo $this->renderPartial('_formSpd',array('model'=>$model)); ?>

<h2>Data Surat Perjalanan Dinas</h2>

<?php $this->renderPartial('_adminSpd',array('model'=>$model)); ?>