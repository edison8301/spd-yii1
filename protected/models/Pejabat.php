<?php

/**
 * This is the model class for table "pejabat".
 *
 * The followings are the available columns in table 'pejabat':
 * @property integer $id
 * @property string $nip
 * @property string $nama
 * @property string $jabatan
 *
 * The followings are the available model relations:
 * @property Kwitansi[] $kwitansis
 * @property Kwitansi[] $kwitansis1
 * @property Rill[] $rills
 * @property Spd[] $spds
 */
class Pejabat extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pejabat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nip, nama, jabatan', 'length', 'max'=>255),
			array('nip,nama,jabatan','required','message'=>'{attribute} tidak boleh kosong'),
			array('nip','karakterNip'),
			array('nip','unik'),
			array('nip','numerical','integerOnly'=>'true','message'=>'NIP harus dalam angka, jangan gunakan spasi'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nip, nama, jabatan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kwitansis' => array(self::HAS_MANY, 'Kwitansi', 'bendahara'),
			'kwitansis1' => array(self::HAS_MANY, 'Kwitansi', 'ppk'),
			'rills' => array(self::HAS_MANY, 'Rill', 'ppk'),
			'spds' => array(self::HAS_MANY, 'Spd', 'id_pejabat'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nip' => 'NIP',
			'nama' => 'Nama',
			'jabatan' => 'Jabatan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nip',$this->nip,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('jabatan',$this->jabatan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pejabat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function karakterNip()
	{
		if(strlen($this->nip)!=18) $this->addError('nip','Jumlah karakter NIP yang anda masukkan salah');
	}
	
	public function unik()
	{
		$model = Pejabat::model()->findByAttributes(array('nip'=>$this->nip));
		if($model!==null)
		{
			if($model->id != $this->id)
				$this->addError('nip','NIP sudah digunakan oleh pejabat yang lain');
			
		}
	}
	
	public function getNip()
	{
		return substr($this->nip,0,8).' '.substr($this->nip,8,6).' '.substr($this->nip,14,1).' '.substr($this->nip,15,3);
	}
	
	public function getFoto($htmlOptions = array())
	{
		if($this->foto != '')
			return CHtml::image(Yii::app()->request->baseUrl.'/uploads/pejabat/'.$this->foto,'',$htmlOptions);
		else
			return CHtml::image(Yii::app()->request->baseUrl.'/img/no-profile.jpg','',$htmlOptions);
	}
	
	public function beforeDelete()
	{
		$path = Yii::app()->basePath.'/../uploads/pejabat/';
		
		$oldFoto = $this->foto;
		
		if(file_exists($path.$oldFoto) and $oldFoto != '')
			unlink($path.$oldFoto);
		
		return true;
	}
}
