<?php

/**
 * This is the model class for table "spd".
 *
 * The followings are the available columns in table 'spd':
 * @property integer $id
 * @property string $nomor
 * @property string $tanggal
 * @property integer $id_pejabat
 * @property integer $id_pegawai
 * @property string $maksud
 * @property string $tujuan
 * @property string $tgl_pergi
 * @property string $tgl_kembali
 * @property string $lama
 * @property string $kendaraan
 * @property string $tempat_berangkat
 * @property string $instansi
 * @property string $biaya
 * @property string $koring
 * @property integer $pengikut
 *
 * The followings are the available model relations:
 * @property Kwitansi[] $kwitansis
 * @property Lhp[] $lhps
 * @property Pengikut[] $pengikuts
 * @property Rill[] $rills
 * @property Pegawai $idPegawai
 * @property Pejabat $idPejabat
 */
class Spd extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'spd';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pejabat, id_pegawai, pengikut', 'numerical', 'integerOnly'=>true),
			array('nomor_spt, id_pejabat, id_pegawai, tanggal_spt,tujuan, tgl_pergi, tgl_kembali','required','message'=>'{attribute} tidak boleh kosong'),
			array('nomor_spd, lama, kendaraan, tanggal_spd tempat_berangkat, instansi, biaya, koring','required','on'=>'inputspd'),
			array('nomor_spt, nomor_spd, maksud, tujuan, lama, kendaraan, tempat_berangkat, instansi, biaya, koring', 'length', 'max'=>255),
			array('tanggal, tgl_pergi, tgl_kembali', 'safe'),
			array('nomor_spd','nomorSpdUnik','on'=>'inputSpd'),
			array('nomor_spd','nomorSpdUnik','on'=>'updateSpd'),
			array('nomor_spt','nomorSptUnik','on'=>'inputSpt'),
			array('nomor_spt','nomorSptUnik','on'=>'updateSpt'),
			array('id_pegawai','perjalananTidakDobel'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id,nomor_spt, nomor_spd, tanggal, id_pejabat, id_pegawai, maksud, tujuan, tgl_pergi, tgl_kembali, lama, kendaraan, tempat_berangkat, instansi, biaya, koring, pengikut', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kwitansi' => array(self::HAS_MANY, 'Kwitansi', 'id_spd'),
			'lhp' => array(self::HAS_MANY, 'Lhp', 'id_spd'),
			'pengikut' => array(self::HAS_MANY, 'Pengikut', 'id_spd'),
			'rill' => array(self::HAS_MANY, 'Rill', 'id_spd'),
			'pegawai' => array(self::BELONGS_TO, 'Pegawai', 'id_pegawai'),
			'pejabat' => array(self::BELONGS_TO, 'Pejabat', 'id_pejabat'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nomor_spt'=>'Nomor SPT',
			'nomor_spd' => 'Nomor SPD',
			'tanggal_spt' => 'Tanggal SPT',
			'tanggal_spd' => 'Tanggal SPD',
			'id_pejabat' => 'Pejabat',
			'id_pegawai' => 'Pegawai',
			'maksud' => 'Maksud',
			'tujuan' => 'Tujuan',
			'tgl_pergi' => 'Tgl Pergi',
			'tgl_kembali' => 'Tgl Kembali',
			'lama' => 'Lama',
			'kendaraan' => 'Kendaraan',
			'tempat_berangkat' => 'Tempat Berangkat',
			'instansi' => 'Instansi',
			'biaya' => 'Biaya',
			'koring' => 'Koring',
			'pengikut' => 'Pengikut',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		
		if(isset($_GET['Spd']))
			$this->attributes = $_GET['Spd'];
		
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('nomor_spt',$this->nomor_spt,true);
		$criteria->compare('nomor_spd',$this->nomor_spd,true);
		$criteria->compare('tanggal_spt',$this->tanggal_spt,true);
		$criteria->compare('tanggal_spd',$this->tanggal_spd,true);
		$criteria->compare('id_pejabat',$this->id_pejabat);
		$criteria->compare('id_pegawai',$this->id_pegawai);
		$criteria->compare('maksud',$this->maksud,true);
		$criteria->compare('tujuan',$this->tujuan,true);
		$criteria->compare('tgl_pergi',$this->tgl_pergi,true);
		$criteria->compare('tgl_kembali',$this->tgl_kembali,true);
		$criteria->compare('lama',$this->lama,true);
		$criteria->compare('kendaraan',$this->kendaraan,true);
		$criteria->compare('tempat_berangkat',$this->tempat_berangkat,true);
		$criteria->compare('instansi',$this->instansi,true);
		$criteria->compare('biaya',$this->biaya,true);
		$criteria->compare('koring',$this->koring,true);
		$criteria->compare('pengikut',$this->pengikut);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
                    'defaultOrder'=>'id DESC',
                ),
		));
	}
	
	public function pilihSpt()
	{
		//return new CActiveDataProvider('Spd');
		
		return new CActiveDataProvider("Spd", array(
			'criteria'=>array('condition'=>'nomor_spd = ""'),
			'sort'=>array('defaultOrder'=>'id DESC'),
		));
		
	
	}
	
	public function dataSpd()
	{
		return new CActiveDataProvider("Spd", array(
			'criteria'=>array(
				'condition'=>'nomor_spd <> ""',
			),
			'sort'=>array(
                    'defaultOrder'=>'id DESC',
            ),
		));
	
	}

	public function getDataSpd()
	{
		$model = Spd::model()->findAllByAttributes(array('nomor_spd'=>'Cek 11'));
		
		if($model!==null)
			return $model;
		else
			return false;
	}

	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Spd the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getDataPengikut()
	{
		$model = Pengikut::model()->findAllByAttributes(array('id_spd'=>$this->id,'aktif'=>'1'));
		
		if($model!==null)
			return $model;
		else
			return false;
	}
	
	public function getListPengikut()
	{
		$model = Pegawai::model()->findAll(array('order'=>'nama ASC'));
		
		$list = array();
		
		$list[0] = '-- Pilih Pegawai --';
		
		foreach($model as $data)
		{
			$list[$data->id] = $data->nama;
		}
		
		return $list;
	}
	
	public function beforeDelete()
	{
		foreach(Pengikut::model()->findAllByAttributes(array('id_spd'=>$this->id)) as $data)
		{
			$data->delete();
		}
		
		foreach(Perjalanan::model()->findAllByAttributes(array('id_spd'=>$this->id)) as $data)
		{
			$data->delete();
		}
		
		return true;
	}
	
	public function getNamaPegawai()
	{
	
	}
	
	public function nomorSpdUnik()
	{
		$model = Spd::model()->findByAttributes(array('nomor_spd'=>$this->nomor_spd));
		if($model!==null)
		{
			if($model->id != $this->id)
				$this->addError('nomor_spd','Ganti nomor SPD, sudah ada SPD yang menggunakan nomor tersebut');
			
		}
	}
	
	public function nomorSptUnik()
	{
		$model = Spd::model()->findByAttributes(array('nomor_spt'=>$this->nomor_spt));
		if($model!==null)
		{
			if($model->id != $this->id)
				$this->addError('nomor_spt','Ganti nomor SPT, sudah ada SPT yang menggunakan nomor tersebut');
			
		}
	}
	
	public function perjalananTidakDobel()
	{
		$pegawai = $this->pegawai;
		
		$tanggal = $this->tgl_pergi;
		
		$error = 0;
		
		while($tanggal <= $this->tgl_kembali)
		{
			$perjalanan = Perjalanan::model()->findByAttributes(array('id_pegawai'=>$pegawai->id,'tanggal'=>$tanggal,'aktif'=>1));
		
			if($perjalanan !== null AND $perjalanan->id_spd != $this->id) $error = 1;
			
			$tanggal = date('Y-m-d',strtotime(date("Y-m-d", strtotime($tanggal)) . " +1 day"));
		}
		
		if($error) $this->addError('id_pegawai','Pegawai sedang dalam perjalanan dinas antara '.$this->tgl_pergi.' dan '.$this->tgl_kembali);
	
	}
	
	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}
	
	public function getRelationRelationField($relation1,$relation2,$field)
	{
		if(!empty($this->$relation1->$relation2->$field))
			return $this->$relation1->$relation2->$field;
		else
			return null;
	}
	
	public function getPejabatNip()
	{
		if(!empty($this->pejabat))
			return $this->pejabat->getNip();
		else
			return null;
	}
	
	public function getRelationNip($relation)
	{
		if(!empty($this->$relation))
			return $this->$relation->getNip();
		else
			return null;
	}
}
