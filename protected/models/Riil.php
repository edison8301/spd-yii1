<?php

/**
 * This is the model class for table "rill".
 *
 * The followings are the available columns in table 'rill':
 * @property integer $id
 * @property integer $id_spd
 * @property string $bbm
 * @property string $tol
 * @property string $jumlah
 * @property string $tanggal
 * @property integer $ppk
 *
 * The followings are the available model relations:
 * @property Pejabat $ppk0
 * @property Spd $idSpd
 */
class Riil extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Rill the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'riil';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_spd, ppk', 'numerical', 'integerOnly'=>true),
			array('bbm, tol, jumlah', 'length', 'max'=>255),
			array('tanggal', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_spd, bbm, tol, jumlah, tanggal, ppk', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'spd' => array(self::BELONGS_TO, 'Spd', 'id_spd'),
			'ppkRelation' => array(self::BELONGS_TO, 'Pejabat', 'ppk'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_spd' => 'Id Spd',
			'bbm' => 'BBM',
			'tol' => 'Tol',
			'jumlah' => 'Jumlah',
			'tanggal' => 'Tanggal Pembuatan',
			'ppk' => 'PPK',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_spd',$this->id_spd);
		$criteria->compare('bbm',$this->bbm,true);
		$criteria->compare('tol',$this->tol,true);
		$criteria->compare('jumlah',$this->jumlah,true);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('ppk',$this->ppk);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
                    'defaultOrder'=>'id DESC',
                ),
		));
	}
	
	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}
}