<?php

/**
 * This is the model class for table "lhp".
 *
 * The followings are the available columns in table 'lhp':
 * @property integer $id
 * @property string $nomor
 * @property string $kepada
 * @property string $dari
 * @property string $tanggal
 * @property string $hal
 * @property integer $id_spd
 * @property string $kesimpulan
 *
 * The followings are the available model relations:
 * @property Spd $idSpd
 */
class Lhp extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Lhp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lhp';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nomor, kepada, dari, hal, kesimpulan','required','message'=>'{attribute} tidak boleh kosong'),
			array('id_spd', 'numerical', 'integerOnly'=>true),
			array('nomor, kepada, dari, hal, kesimpulan', 'length', 'max'=>255),
			array('tanggal', 'safe'),
			array('nomor','nomorUnik'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nomor, kepada, dari, tanggal, hal, id_spd, kesimpulan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'spd' => array(self::BELONGS_TO, 'Spd', 'id_spd'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nomor' => 'Nomor',
			'kepada' => 'Kepada',
			'dari' => 'Dari',
			'tanggal' => 'Tanggal',
			'hal' => 'Hal',
			'id_spd' => 'Nomor Spd',
			'kesimpulan' => 'Kesimpulan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nomor',$this->nomor,true);
		$criteria->compare('kepada',$this->kepada,true);
		$criteria->compare('dari',$this->dari,true);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('hal',$this->hal,true);
		$criteria->compare('id_spd',$this->id_spd);
		$criteria->compare('kesimpulan',$this->kesimpulan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
                    'defaultOrder'=>'id DESC',
                ),
		));
	}
	
	public function nomorUnik()
	{
		$model = Lhp::model()->findByAttributes(array('nomor'=>$this->nomor));
		if($model!==null)
		{
			if($model->id != $this->id)
				$this->addError('nomor','Ganti nomor LHP, sudah ada LHP yang menggunakan nomor tersebut');
			
		}
	}
	
	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}
	
	public function getRelationRelationField($relation1,$relation2,$field)
	{
		if(!empty($this->$relation1->$relation2->$field))
			return $this->$relation1->$relation2->$field;
		else
			return null;
	}
	
	public function getRelationRelationNip($relation1,$relation2)
	{
		if(!empty($this->$relation1->$relation2))
			return $this->$relation1->$relation2->getNip();
		else
			return null;
	}
}