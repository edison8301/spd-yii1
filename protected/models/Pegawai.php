<?php

/**
 * This is the model class for table "pegawai".
 *
 * The followings are the available columns in table 'pegawai':
 * @property integer $id
 * @property string $nip
 * @property string $nama
 * @property integer $id_golongan
 * @property string $jabatan
 * @property string $tgl_lahir
 *
 * The followings are the available model relations:
 * @property Golongan $idGolongan
 * @property Pengikut[] $pengikuts
 * @property Spd[] $spds
 */
class Pegawai extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pegawai';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_golongan', 'numerical', 'integerOnly'=>true),
			array('nip, nama, password, tgl_lahir, jabatan, id_golongan','required','message'=>'{attribute} tidak boleh kosong'),
			array('nip','karakterNip'),
			array('nip','unik'),
			array('foto','file','allowEmpty'=>true,'types'=>'png,jpg,gif','maxSize'=>150*1024,'tooLarge'=>'File terlalu BESAR. Ukuran file maksimal adalah 150 KB'),
			array('nip, nama, jabatan, tgl_lahir, password', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nip, nama, id_golongan, jabatan, tgl_lahir', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'golongan' => array(self::BELONGS_TO, 'Golongan', 'id_golongan'),
			'pengikut' => array(self::HAS_MANY, 'Pengikut', 'id_pegawai'),
			'spd' => array(self::HAS_MANY, 'Spd', 'id_pegawai'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nip' => 'NIP',
			'nama' => 'Nama',
			'id_golongan' => 'Golongan',
			'jabatan' => 'Jabatan',
			'tgl_lahir' => 'Tgl Lahir',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		
		if(isset($_GET['Pegawai']))
			$this->attributes = $_GET['Pegawai'];
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nip',$this->nip,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('id_golongan',$this->id_golongan);
		$criteria->compare('jabatan',$this->jabatan,true);
		$criteria->compare('tgl_lahir',$this->tgl_lahir,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pegawai the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function beforeDelete()
	{
		foreach(Pengikut::model()->findAllByAttributes(array('id_pegawai'=>$this->id)) as $data)
		{
			$data->delete();
		}
		
		$oldFoto = $this->foto;
		
		$path = Yii::app()->basePath.'/../uploads/pegawai/';
		if(file_exists($path.$oldFoto) and $oldFoto != '')
			unlink($path.$oldFoto);
		
		return true;
	}
	
	public function karakterNip()
	{
		if(strlen($this->nip)!=18) $this->addError('nip','Jumlah karakter NIP yang anda masukkan salah');
	}
	
	public function unik()
	{
		$model = Pegawai::model()->findByAttributes(array('nip'=>$this->nip));
		if($model!==null)
		{
			if($model->id != $this->id)
				$this->addError('nip','NIP sudah digunakan oleh pegawai yang lain');
			
		}
	}
	
	public function getNip()
	{
		if($this->nip!='')
			return substr($this->nip,0,8).' '.substr($this->nip,8,6).' '.substr($this->nip,14,1).' '.substr($this->nip,15,3);
		else
			return null;
	}
	
	public function cekPerjalanan($tanggal)
	{
		$model = Perjalanan::model()->findByAttributes(array('id_pegawai'=>$this->id,'tanggal'=>$tanggal,'aktif'=>1));
		
		if($model!==null)
			return 1;
		else
			return 0;
	}
	
	public function getFoto($htmlOptions = array())
	{
		if($this->foto != '')
			return CHtml::image(Yii::app()->request->baseUrl.'/uploads/pegawai/'.$this->foto,'',$htmlOptions);
		else
			return CHtml::image(Yii::app()->request->baseUrl.'/img/no-profile.jpg','',$htmlOptions);
	}
	
	public function getJumlahPerjalananByBulanByTahun($bulan,$tahun)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'id_pegawai=:id_pegawai AND tanggal >= :tanggal_awal AND tanggal <= :tanggal_akhir AND aktif = 1';
		$criteria->params = array(':id_pegawai'=>$this->id,':tanggal_awal'=>$tahun.'-'.$bulan.'-01',':tanggal_akhir'=>$tahun.'-'.$bulan.'-31');
		
		return Perjalanan::model()->count($criteria);
	}
	
	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}	
	
	public function getTglLahir()
	{
		return Bantu::tanggal($this->tgl_lahir);
	}
	
	
}
