<?php

/**
 * This is the model class for table "akomodasi".
 *
 * The followings are the available columns in table 'akomodasi':
 * @property integer $id
 * @property string $jarak
 * @property integer $id_golongan
 * @property string $biaya
 *
 * The followings are the available model relations:
 * @property Golongan $idGolongan
 * @property Kwitansi[] $kwitansis
 */
class Akomodasi extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Akomodasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'akomodasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_golongan', 'numerical', 'integerOnly'=>true),
			array('jarak, biaya', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, jarak, id_golongan, biaya', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'golongan' => array(self::BELONGS_TO, 'Golongan', 'id_golongan'),
			'kwitansi' => array(self::HAS_MANY, 'Kwitansi', 'id_akomodasi'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'jarak' => 'Jarak',
			'id_golongan' => 'Golongan',
			'biaya' => 'Biaya',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		
		if(isset($_GET['Akomodasi']))
			$this->attributes = $_GET['Akomodasi'];
		
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('jarak',$this->jarak,true);
		$criteria->compare('id_golongan',$this->id_golongan);
		$criteria->compare('biaya',$this->biaya,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}
}