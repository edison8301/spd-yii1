window.onload=function() {
  	var container = document.getElementById("tabContainer");
		var tabcon = document.getElementById("tabscontent");
    var navitem = document.getElementById("tabHeader_1");
    navitem.parentNode.setAttribute("data-current",1);

     var pages = tabcon.getElementsByTagName("div");
      for (var i = 0; i < pages.length; i++) {
      var number = i+1;
      if($("#tabHeader_"+number).attr("class") != "tabActiveHeader"){
        pages.item(i).style.display="none";
      }else{
        navitem.parentNode.setAttribute("data-current",number);
      }
		};

    //this adds click event to tabs
    var tabs = container.getElementsByTagName("li");
    for (var i = 0; i < tabs.length; i++) {
      tabs[i].onclick=displayPage;
    }
}

// on click of one of tabs
function displayPage() {
  var current = this.parentNode.getAttribute("data-current");
  //remove class of activetabheader and hide old contents
  document.getElementById("tabHeader_" + current).removeAttribute("class");
  document.getElementById("tabpage_" + current).style.display="none";

  var ident = this.id.split("_")[1];
  //add class of activetabheader to new active tab and show contents
  this.setAttribute("class","tabActiveHeader");
  document.getElementById("tabpage_" + ident).style.display="block";
  this.parentNode.setAttribute("data-current",ident);
}